﻿using log4net;
using Microsoft.Xna.Framework;
using ShevaEngine.Assets;
using ShevaEngine.Entities.Components;
using ShevaEngine.Graphics;
using ShevaEngine.Input;
using ShevaEngine.Renderers;
using ShevaEngine.Scenes;
using LogManager = ShevaEngine.Logging.LogManager;

namespace ShevaEngine
{
    /// <summary>
    /// Sheva game.
    /// </summary>
    public class ShevaGame : Game
    {
        public AssetsManager AssetsManager { get; }
        public GraphicsSystem GraphicsSystem { get; }
        public InputManager Input { get; } 
        public ILog Log { get; }        
        public Scene Scene { get; private set; }
        public RenderersManager Renderers { get; private set; }
        private readonly LogManager _logManager;
        

        /// <summary>
        /// Constructor.
        /// </summary>
        public ShevaGame()
            : base()
        {
            _logManager = new LogManager();
            Log = log4net.LogManager.GetLogger(GetType());

            Log.Info("Starting Sheva game engine");
            
            AssetsManager = new AssetsManager(this);
            GraphicsSystem = new GraphicsSystem(this);            
            Input = new InputManager();
            Scene = null;
        }

        /// <summary>
        /// Initialize.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            
            Renderers = new RenderersManager(this);
            Window.Title = "Sheva Engine";
            IsMouseVisible = true;            
        }

        /// <summary>
        /// Load content.
        /// </summary>
        protected override void LoadContent()
        {
            base.LoadContent();
            
            

            LoadScene(@"Assets\Scenes\TestScene.scene");
        }

        /// <summary>
        /// Load scene.
        /// </summary>        
        public bool LoadScene(string path)
        {
            Scene = AssetsManager.Get<Scene>(path);

            Scene?.Start(this);

            return true;
        }

        /// <summary>
        /// Update.
        /// </summary>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            Input?.Update(gameTime);

            Scene?.Update(gameTime);
        }

        /// <summary>
        /// Draw.
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            lock (Scene)
                if (Scene != null)
                {
                    foreach (CameraComponent cameraComponent in Scene.CameraComponents)
                        if (cameraComponent.IsActive)
                            RenderCamera(cameraComponent);
                }
        }

        /// <summary>
        /// Render camera.
        /// </summary>        
        private void RenderCamera(CameraComponent cameraComponent)
        {
            Renderers[cameraComponent.RenderingPath].Render(cameraComponent, Scene);
        }        
    }
}

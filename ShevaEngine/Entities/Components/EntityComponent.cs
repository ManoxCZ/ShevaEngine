﻿using log4net;
using ShevaEngine.Assets;
using ShevaEngine.Input;
using ShevaEngine.Scenes;
using System.Runtime.Serialization;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Entity component.
    /// </summary>
    [DataContract]
    public abstract class EntityComponent : Asset, IEntityComponent
    {
        public Entity Entity { get; private set; }
        protected ILog Log { get; }
        public InputManager Input => Entity.Input;
        public Scene Scene => Entity.Scene;


        /// <summary>
        /// Constructor.
        /// </summary>
        protected EntityComponent()
        {
            Log = LogManager.GetLogger(GetType());
        }

        /// <summary>
        /// Initialize method.
        /// </summary>
        internal void Initialize(Entity entity)
        {
            Entity = entity;
        }

        /// <summary>
        /// Awake.
        /// </summary>
        public virtual void Awake()
        {            
            Log.Debug("Awake");
        }

        /// <summary>
        /// Get component.
        /// </summary>
        public T GetComponent<T>() where T : IEntityComponent
        {
            return Entity.GetComponent<T>();
        }
    }
}

﻿using System.Runtime.Serialization;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Activable component.
    /// </summary>
    public class ActivableComponent : EntityComponent
    {
        [DataMember]
        public bool IsActive { get; set; } = true;
    }
}

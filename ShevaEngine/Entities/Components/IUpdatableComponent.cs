﻿using Microsoft.Xna.Framework;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// IUpdatable component.
    /// </summary>
    public interface IUpdatableComponent : IEntityComponent
    {
        /// <summary>
        /// Update method.
        /// </summary>        
        void Update(GameTime gameTime);
    }
}

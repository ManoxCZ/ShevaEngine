﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Transform component.
    /// </summary>
    [DataContract]
    public class TransformComponent : EntityComponent
    {
        [IgnoreDataMember]
        private TransformComponent _parent;
        [DataMember]
        public TransformComponent Parent
        {
            get => _parent;
            set
            {
                if (_parent != value)
                {
                    _parent = value;

                    UpdateMatrices();
                }
            }
        }
        [DataMember]
        public List<TransformComponent> Children { get; set; }
        [IgnoreDataMember]
        private Vector3 _localPosition;
        [DataMember]
        public Vector3 LocalPosition
        {
            get => _localPosition;
            set
            {
                if (_localPosition != value)
                {
                    _localPosition = value;

                    UpdateMatrices();
                }
            }
        }
        [IgnoreDataMember]
        private Quaternion _localRotation;
        [DataMember]
        public Quaternion LocalRotation
        {
            get => _localRotation;
            set
            {
                if (_localRotation != value)
                {
                    _localRotation = value;

                    UpdateMatrices();
                }
            }
        }
        [IgnoreDataMember]
        private Vector3 _localScale;
        [DataMember]
        public Vector3 LocalScale
        {
            get => _localScale;
            set
            {
                if (_localScale != value)
                {
                    _localScale = value;

                    UpdateMatrices();
                }
            }
        }
        //[IgnoreDataMember]
        //public Vector3 Position { get; set; }
        //[IgnoreDataMember]
        //public Quaternion Rotation { get; set; }
        //[IgnoreDataMember]
        //public Vector3 Scale { get; set; }
        [IgnoreDataMember]
        internal Matrix WorldMatrix { get; private set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        public TransformComponent()
        {
            Children = new List<TransformComponent>();
            WorldMatrix = Matrix.Identity;
        }

        /// <summary>
        /// Update matrices.
        /// </summary>
        private void UpdateMatrices()
        {
            WorldMatrix = Matrix.Identity;

            if (Parent != null)
                WorldMatrix = Parent.WorldMatrix;

            WorldMatrix *=
                Matrix.CreateTranslation(LocalPosition) *
                Matrix.CreateFromQuaternion(LocalRotation) *
                Matrix.CreateScale(LocalScale);

            foreach (TransformComponent child in Children)
                child.UpdateMatrices();            
        }
    }
}

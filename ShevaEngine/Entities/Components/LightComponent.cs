﻿using Microsoft.Xna.Framework;
using ShevaEngine.Renderers.Shadows;
using System.Runtime.Serialization;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Shadow map type.
    /// </summary>
    public enum ShadowMapType
    {
        None,
        DualParaboloid,
    }

    /// <summary>
    /// Light component.
    /// </summary>
    public class LightComponent : ActivableComponent
    {
        [DataMember]
        public Color Color { get; set; }
        [DataMember]
        public float Intensity { get; set; }
        [DataMember]
        public float Radius { get; set; }
        [DataMember]
        public ShadowMapType ShadowMapType { get; set; }
        public ShadowMap ShadowMap { get; set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        public LightComponent()
        {
            Color = Color.White;
            Intensity = 1;
            Radius = 10;
            ShadowMapType = ShadowMapType.None;
        }
    }    
}

﻿namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Script component.
    /// </summary>
    public abstract class ScriptComponent : ActivableComponent, IOnGUIComponent
    {
        /// <summary>
        /// On Gui method.
        /// </summary>
        public virtual void OnGui()
        {
            
        }
    }
}

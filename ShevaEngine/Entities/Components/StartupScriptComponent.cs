﻿namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Startup script component.
    /// </summary>
    public class StartupScriptComponent : ScriptComponent
    {
        /// <summary>
        /// Start.
        /// </summary>
        public virtual void Start()
        {

        }
    }
}

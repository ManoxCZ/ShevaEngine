﻿using ShevaEngine.Renderers.RenderingPipelines;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// I Renderable component.
    /// </summary>
    public interface IRenderableComponent : IEntityComponent
    {
        /// <summary>
        /// Render method.
        /// </summary>
        void Render(IRenderingPipeline pipeline, CameraComponent camera);
    }
}

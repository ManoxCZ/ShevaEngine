﻿namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// IEntityComponent interface.
    /// </summary>
    public interface IEntityComponent
    {
        /// <summary>
        /// Entity.
        /// </summary>
        Entity Entity { get; }
    }
}

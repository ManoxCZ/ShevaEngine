﻿using Microsoft.Xna.Framework;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Sync script component.
    /// </summary>
    public class SyncScriptComponent : StartupScriptComponent, IUpdatableComponent
    {

        /// <summary>
        /// Update.
        /// </summary>        
        public virtual void Update(GameTime gameTime)
        {

        }
    }
}

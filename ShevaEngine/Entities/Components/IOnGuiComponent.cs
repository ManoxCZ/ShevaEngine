﻿namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// On Gui interface.
    /// </summary>
    public interface IOnGUIComponent : IEntityComponent
    {
        /// <summary>
        /// On Gui method.
        /// </summary>
        void OnGui();
    }
}

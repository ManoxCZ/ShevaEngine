﻿using ShevaEngine.Graphics.Meshes;
using ShevaEngine.Renderers.RenderingPipelines;
using System.Runtime.Serialization;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Mesh component.
    /// </summary>
    [DataContract]
    public class MeshComponent : EntityComponent, IRenderableComponent
    {
        [DataMember]
        public Mesh Mesh { get; set; }


        /// <summary>
        /// Render method.
        /// </summary>
        public void Render(IRenderingPipeline pipeline, CameraComponent camera)
        {
            
        }
    }
}

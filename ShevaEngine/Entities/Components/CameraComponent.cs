﻿using Microsoft.Xna.Framework;
using System.Runtime.Serialization;

namespace ShevaEngine.Entities.Components
{
    /// <summary>
    /// Camera component.
    /// </summary>
    public class CameraComponent : ActivableComponent
    {
        [IgnoreDataMember]
        private TransformComponent _transform;
        [IgnoreDataMember]
        private float _fieldOfView;
        [DataMember]
        public float FieldOfView
        {
            get => _fieldOfView;
            set
            {
                if (_fieldOfView != value)
                {
                    _fieldOfView = value;

                    UpdateProjectionMatrix();
                }
            }
        }
        [DataMember]
        public Color ClearColor { get; set; }
        [DataMember]
        public string RenderingPath { get; set; }
        [IgnoreDataMember]
        private float _nearPlane;
        [DataMember]
        public float NearPlane
        {
            get => _nearPlane;
            set
            {
                if (_nearPlane != value)
                {
                    _nearPlane = value;

                    UpdateProjectionMatrix();
                }
            }
        }
        [IgnoreDataMember]
        private float _farPlane;
        [DataMember]
        public float FarPlane
        {
            get => _farPlane;
            set
            {
                if (_farPlane != value)
                {
                    _farPlane = value;

                    UpdateProjectionMatrix();
                }
            }
        }
        [IgnoreDataMember]
        private Rectangle _viewport;
        [DataMember]
        public Rectangle Viewport
        {
            get => _viewport;
            set
            {
                if (_viewport != value)
                {
                    _viewport = value;

                    UpdateProjectionMatrix();
                }
            }
        }
        internal Matrix ViewMatrix => _transform.WorldMatrix;
        internal Matrix ProjectionMatrix { get; private set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        public CameraComponent()
        {
            ClearColor = Color.AliceBlue;            
            ProjectionMatrix = Matrix.Identity;
        }

        /// <summary>
        /// Awake method.
        /// </summary>
        public override void Awake()
        {
            _transform = GetComponent<TransformComponent>();
        }

        /// <summary>
        /// Update projection matrix.
        /// </summary>
        private void UpdateProjectionMatrix()
        {
            if (FieldOfView == 0 || Viewport.Width == 0 || Viewport.Height == 0 || NearPlane == 0 || FarPlane == 0)
                return;

            ProjectionMatrix = Matrix.CreatePerspectiveFieldOfView(FieldOfView, Viewport.Width / (float)Viewport.Height, NearPlane, FarPlane);
        }
    }
}

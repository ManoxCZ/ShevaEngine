﻿using ShevaEngine.Assets;
using ShevaEngine.Entities.Components;
using ShevaEngine.Graphics;
using ShevaEngine.Input;
using ShevaEngine.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace ShevaEngine.Entities
{
    /// <summary>
    /// Entity.
    /// </summary>
    [DataContract]
    public class Entity : Asset
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        private List<EntityComponent> Components { get; }
        internal ShevaGame Game { get; private set; }
        public AssetsManager AssetsManager => Game?.AssetsManager;
        public GraphicsSystem GraphicsSystem => Game?.GraphicsSystem;
        public InputManager Input => Game?.Input;
        public Scene Scene => Game?.Scene;


        /// <summary>
        /// Constructor.
        /// </summary>
        public Entity()
        {
            Name = "Entity";

            Components = new List<EntityComponent>();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Entity(string name)
        {
            Id = Guid.NewGuid();

            Name = name;

            Components = new List<EntityComponent>();
        }

        /// <summary>
        /// Start.
        /// </summary>
        internal void Start(ShevaGame game)
        {
            Game = game;

            foreach (EntityComponent component in Components)
            {
                component.Initialize(this);

                component.Awake();
            }
        }

        /// <summary>
        /// Add component.
        /// </summary>
        public T AddComponent<T>() where T : EntityComponent, new()
        {
            return (T)AddComponent(new T());
        }

        /// <summary>
        /// Add component.
        /// </summary>
        public IEntityComponent AddComponent(EntityComponent component)
        {
            component.Initialize(this);

            component.Awake();

            Components.Add(component);

            return component;
        }

        /// <summary>
        /// Get component.
        /// </summary>
        public T GetComponent<T>() where T : IEntityComponent
        {
            return Components.OfType<T>().FirstOrDefault();
        }

        /// <summary>
        /// Get components.
        /// </summary>
        public IEnumerable<T> GetComponents<T>() where T : IEntityComponent
        {
            return Components.OfType<T>();
        }             
    }
}

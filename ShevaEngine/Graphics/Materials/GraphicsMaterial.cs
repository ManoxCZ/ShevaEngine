﻿using Microsoft.Xna.Framework;
using ShevaEngine.Assets;
using System;
using Texture2D = ShevaEngine.Graphics.Textures.Texture2D;

namespace ShevaEngine.Graphics.Materials
{
    /// <summary>
    /// Material.
    /// </summary>
    public class GraphicsMaterial : Asset
    {
        public bool IsTransparent = false;        
        
        public Vector3 DiffuseColor = Color.Gray.ToVector3();
        private float _roughness = 0.5f;
        public float Roughness
        {
            get => _roughness;
            set => _roughness = Math.Max(value, 0.001f);
        }
        public float Metallic;
        public float EmissiveStrength;

        public Texture2D AlbedoMap { get; set; }
        public Texture2D RoughnessMap { get; set; }
        public Texture2D MetallicMap { get; set; }
        public Texture2D NormalMap { get; set; }
        public Texture2D DisplacementMap { get; set; }
        public Texture2D Mask { get; set; }        
        public bool RenderCClockwise = false;        
        public GraphicsMaterialTypes Type { get; set; } = GraphicsMaterialTypes.Basic;

        public bool HasDiffuse => AlbedoMap != null;
        public bool HasRoughnessMap => RoughnessMap != null;
        public bool HasMask => Mask != null;
        public bool HasNormalMap => NormalMap != null;
        public bool HasMetallic => MetallicMap != null;
        public bool HasDisplacement => DisplacementMap != null;       
    }
}

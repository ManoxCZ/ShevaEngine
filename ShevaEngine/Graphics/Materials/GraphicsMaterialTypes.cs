﻿namespace ShevaEngine.Graphics.Materials
{
    /// <summary>
    /// Graphics material types.
    /// </summary>
    public enum  GraphicsMaterialTypes
    {
        Basic = 0,
        Emissive = 3,
        Hologram = 1,
        ProjectHologram = 2,
        SubsurfaceScattering = 4,
        ForwardShaded = 5,
    }
}

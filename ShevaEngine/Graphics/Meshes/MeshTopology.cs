﻿namespace ShevaEngine.Graphics.Meshes
{
    /// <summary>
    /// Mesh topology.
    /// </summary>
    public enum MeshTopology
    {
        Triangles,
        Quads,
        Lines,
        LineStrip,
        Points
    }
}

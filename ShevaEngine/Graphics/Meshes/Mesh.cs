﻿using Microsoft.Xna.Framework;
using ShevaEngine.Assets;
using ShevaEngine.Graphics.DrawCalls;
using System;
using System.Runtime.Serialization;

namespace ShevaEngine.Graphics.Meshes
{
    /// <summary>
    /// Mesh asset.
    /// </summary>    
    public class Mesh : Asset
    {        
        [DataMember]        
        public Vector3[] Vertices { get; set; }
        [DataMember]
        public Vector3[] Normals { get; set; }
        [DataMember]
        public Vector4[] Tangents { get; set; }
        [DataMember]
        public Vector2[] TextureCoords1 { get; set; }
        [DataMember]
        public Vector2[] TextureCoords2 { get; set; }
        [DataMember]
        public Vector2[] TextureCoords3 { get; set; }
        [DataMember]
        public Vector2[] TextureCoords4 { get; set; }
        [DataMember]
        public int[][] Indices { get; set; }        
        public UInt16 DrawCallId { get; private set; } = DrawCallKey.None;


        /// <summary>
        /// Initialize.
        /// </summary>        
        public override void Initialize(ShevaGame game)
        {
            base.Initialize(game);

            DrawCallId = game.GraphicsSystem.DrawCallManager.GetDrawCallForMesh(this);
        }
    }
}

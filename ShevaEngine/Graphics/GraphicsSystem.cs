﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Graphics.DrawCalls;
using ShevaEngine.Graphics.Shaders;
using ShevaEngine.Graphics.Textures;
using ShevaEngine.Scenes;

namespace ShevaEngine.Graphics
{
    /// <summary>
    /// Graphics system.
    /// </summary>
    public class GraphicsSystem
    {
        private readonly GraphicsDeviceManager _graphicsDeviceManager;
        public GraphicsDevice GraphicsDevice => _graphicsDeviceManager.GraphicsDevice;
        public DrawCallManager DrawCallManager { get; }
        public ShadersManager ShadersManager { get; }
        public TextureManager TextureManager { get; }
        private readonly GraphicsCompositor _graphicsCompositor;

        /// <summary>
        /// Constructor.
        /// </summary>
        public GraphicsSystem(Game game)
        {
            _graphicsDeviceManager = new GraphicsDeviceManager(game);            
            _graphicsDeviceManager.GraphicsProfile = GraphicsProfile.HiDef;

            DrawCallManager = new DrawCallManager(this);
            ShadersManager = new ShadersManager(this);
            TextureManager = new TextureManager(this);
            _graphicsCompositor = new GraphicsCompositor();
        }

        /// <summary>
        /// Render scene.
        /// </summary>
        public void RenderScene(Scene scene)
        {
            _graphicsCompositor.RenderScene(scene);
        }        
    }
}

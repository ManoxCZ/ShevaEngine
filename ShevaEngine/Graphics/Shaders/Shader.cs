﻿using ShevaEngine.Assets;
using System.Runtime.Serialization;

namespace ShevaEngine.Graphics.Shaders
{
    /// <summary>
    /// Shader asset.
    /// </summary>
    public class Shader : Asset
    {        
        [DataMember]
        public byte[] ByteCode { get; set; }
    }
}

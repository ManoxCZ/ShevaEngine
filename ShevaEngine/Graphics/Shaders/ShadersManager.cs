﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace ShevaEngine.Graphics.Shaders
{
    /// <summary>
    /// Shaders manager.
    /// </summary>
    public class ShadersManager
    {
        private readonly GraphicsSystem _system;
        private readonly SortedDictionary<Guid, Effect> _shaders;

        public Effect this[Shader shader]
        {
            get
            {
                if (!_shaders.ContainsKey(shader.Id))
                {
                    Effect newEffect = new Effect(_system.GraphicsDevice, shader.ByteCode);

                    _shaders.Add(shader.Id, newEffect);
                }

                if (_shaders.ContainsKey(shader.Id))
                    return _shaders[shader.Id];

                return null;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ShadersManager(GraphicsSystem system)
        {
            _system = system;
            _shaders = new SortedDictionary<Guid, Effect>();
        }
    }
}

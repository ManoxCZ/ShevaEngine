﻿using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Assets;

namespace ShevaEngine.Graphics.Textures
{
    /// <summary>
    /// Texture 2D.
    /// </summary>
    public class Texture2D : Asset
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public byte[][] Data { get; set; }
        public int LevelCount { get; set; }
        public SurfaceFormat Format { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Texture2D()
        {
         
        }
    }
}

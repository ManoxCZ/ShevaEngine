﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Assets;
using Texture2D = ShevaEngine.Graphics.Textures.Texture2D;

namespace ShevaEngine.Graphics.Textures
{
    /// <summary>
    /// Texture manager.
    /// </summary>
    public class TextureManager
    {
        private readonly GraphicsSystem _graphicsSystem;
        private readonly SortedDictionary<Asset, Texture> _textures;

        /// <summary>
        /// Constructor.
        /// </summary>
        public TextureManager(GraphicsSystem graphicsSystem)
        {
            _graphicsSystem = graphicsSystem;

            _textures = new SortedDictionary<Asset, Texture>();
        }

        /// <summary>
        /// Get texture.
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        public Texture GetTexture(Asset asset)
        {
            if (!_textures.ContainsKey(asset))
            {
                _textures.Add(asset, CreateTextureFromAsset(asset as Texture2D));
            }

            return _textures[asset];
        }

        /// <summary>
        /// Create texture from asset. 
        /// </summary>
        private Texture CreateTextureFromAsset(Texture2D asset)
        {
            Texture output = new Microsoft.Xna.Framework.Graphics.Texture2D(_graphicsSystem.GraphicsDevice, asset.Width, asset.Height);

            return output;
        }
    }
}

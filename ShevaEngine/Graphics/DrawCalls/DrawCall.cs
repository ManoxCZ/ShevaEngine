﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Assets;
using System;
using System.Collections.Generic;
using ShevaEngine.Graphics.Meshes;

namespace ShevaEngine.Graphics.DrawCalls
{
    /// <summary>
    /// Draw call.
    /// </summary>
    public class DrawCall
    {        
        /// <summary>The index buffer for this mesh part.</summary>
        public IndexBuffer IndexBuffer;
        /// <summary>The vertex buffer for this mesh part.</summary>
        public VertexBuffer VertexBuffer;        
        /// <summary>The number of vertices used during a draw call.</summary>
        private readonly int _numVertices;
        /// <summary>Parts.</summary>
        private DrawCallPart[] _parts;


        /// <summary>
        /// Constructor.
        /// </summary>
        public DrawCall(GraphicsDevice device, Mesh mesh)
        {
            if (!IsValid(mesh, out _numVertices))
                return;

            CreateVBO(device, mesh);
            CreateIBO(device, mesh);
        }

        /// <summary>
        /// Is valid ?
        /// </summary>
        private bool IsValid(Mesh mesh, out int verticesCount)
        {
            verticesCount = 0;

            if (mesh.Vertices.Length > 0)
            {
                if (verticesCount == 0)
                    verticesCount = mesh.Vertices.Length;
                else if (verticesCount != mesh.Vertices.Length)
                    return false;
            }

            if (mesh.Normals.Length > 0)
            {
                if (verticesCount == 0)
                    verticesCount = mesh.Normals.Length;
                else if (verticesCount != mesh.Normals.Length)
                    return false;
            }

            if (mesh.Tangents.Length > 0)
            {
                if (verticesCount == 0)
                    verticesCount = mesh.Tangents.Length;
                else if (verticesCount != mesh.Tangents.Length)
                    return false;
            }

            if (mesh.TextureCoords1.Length > 0)
            {
                if (verticesCount == 0)
                    verticesCount = mesh.TextureCoords1.Length;
                else if (verticesCount != mesh.TextureCoords1.Length)
                    return false;
            }

            if (mesh.TextureCoords2.Length > 0)
            {
                if (verticesCount == 0)
                    verticesCount = mesh.TextureCoords2.Length;
                else if (verticesCount != mesh.TextureCoords2.Length)
                    return false;
            }

            if (mesh.TextureCoords3.Length > 0)
            {
                if (verticesCount == 0)
                    verticesCount = mesh.TextureCoords3.Length;
                else if (verticesCount != mesh.TextureCoords3.Length)
                    return false;
            }

            if (mesh.TextureCoords4.Length > 0)
            {
                if (verticesCount == 0)
                    verticesCount = mesh.TextureCoords4.Length;
                else if (verticesCount != mesh.TextureCoords4.Length)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Create VBO.
        /// </summary>
        private void CreateVBO(GraphicsDevice device, Mesh mesh)
        {
            VertexDeclaration vertexDeclaration = GetVertexDeclaration(mesh);

            VertexBuffer = new VertexBuffer(device, vertexDeclaration, _numVertices, BufferUsage.WriteOnly);

            byte[] data = GetVerticesData(mesh, vertexDeclaration);

            VertexBuffer.SetData(data, 0, data.Length);
        }

        /// <summary>
        /// Get vertex declaration.
        /// </summary>
        private VertexDeclaration GetVertexDeclaration(Mesh mesh)
        {
            List<VertexElement> elements = new List<VertexElement>();

            int offset = 0;

            if (mesh.Vertices.Length > 0)
            {
                elements.Add(new VertexElement(offset, VertexElementFormat.Vector3, VertexElementUsage.Position, 0));

                offset += 3 * sizeof(float);
            }

            if (mesh.Normals.Length > 0)
            {
                elements.Add(new VertexElement(offset, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0));

                offset += 3 * sizeof(float);
            }

            if (mesh.Tangents.Length > 0)
            {
                elements.Add(new VertexElement(offset, VertexElementFormat.Vector3, VertexElementUsage.Tangent, 0));

                offset += 4 * sizeof(float);
            }

            if (mesh.TextureCoords1.Length > 0)
            {
                elements.Add(new VertexElement(offset, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0));

                offset += 2 * sizeof(float);
            }

            if (mesh.TextureCoords2.Length > 0)
            {
                elements.Add(new VertexElement(offset, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 1));

                offset += 2 * sizeof(float);
            }

            if (mesh.TextureCoords3.Length > 0)
            {
                elements.Add(new VertexElement(offset, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 2));

                offset += 2 * sizeof(float);
            }

            if (mesh.TextureCoords4.Length > 0)
            {
                elements.Add(new VertexElement(offset, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 3));

                offset += 2 * sizeof(float);
            }

            return new VertexDeclaration(elements.ToArray());
        }

        /// <summary>
        /// Get vertices data.
        /// </summary>
        private byte[] GetVerticesData(Mesh mesh, VertexDeclaration vertexDeclaration)
        {
            byte[] buff = new byte[vertexDeclaration.VertexStride * _numVertices];

            for (int i = 0; i < _numVertices; i++)
            {
                int offset = i * vertexDeclaration.VertexStride;

                if (mesh.Vertices.Length > 0)
                {
                    Vector3 data = mesh.Vertices[i];

                    Buffer.BlockCopy(BitConverter.GetBytes(data.X), 0, buff, offset, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Y), 0, buff, offset + sizeof(float), sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Z), 0, buff, offset + 2 * sizeof(float), sizeof(float));

                    offset += 3 * sizeof(float);
                }

                if (mesh.Normals.Length > 0)
                {
                    Vector3 data = mesh.Normals[i];

                    Buffer.BlockCopy(BitConverter.GetBytes(data.X), 0, buff, offset, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Y), 0, buff, offset + sizeof(float), sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Z), 0, buff, offset + 2 * sizeof(float), sizeof(float));

                    offset += 3 * sizeof(float);
                }

                if (mesh.Tangents.Length > 0)
                {
                    Vector4 data = mesh.Tangents[i];

                    Buffer.BlockCopy(BitConverter.GetBytes(data.X), 0, buff, offset, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Y), 0, buff, offset + sizeof(float), sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Z), 0, buff, offset + 2 * sizeof(float), sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.W), 0, buff, offset + 3 * sizeof(float), sizeof(float));

                    offset += 4 * sizeof(float);
                }

                if (mesh.TextureCoords1.Length > 0)
                {
                    Vector2 data = mesh.TextureCoords1[i];

                    Buffer.BlockCopy(BitConverter.GetBytes(data.X), 0, buff, offset, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Y), 0, buff, offset + sizeof(float), sizeof(float));

                    offset += 2 * sizeof(float);
                }

                if (mesh.TextureCoords2.Length > 0)
                {
                    Vector2 data = mesh.TextureCoords2[i];

                    Buffer.BlockCopy(BitConverter.GetBytes(data.X), 0, buff, offset, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Y), 0, buff, offset + sizeof(float), sizeof(float));

                    offset += 2 * sizeof(float);
                }

                if (mesh.TextureCoords3.Length > 0)
                {
                    Vector2 data = mesh.TextureCoords3[i];

                    Buffer.BlockCopy(BitConverter.GetBytes(data.X), 0, buff, offset, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Y), 0, buff, offset + sizeof(float), sizeof(float));

                    offset += 2 * sizeof(float);
                }

                if (mesh.TextureCoords4.Length > 0)
                {
                    Vector2 data = mesh.TextureCoords4[i];

                    Buffer.BlockCopy(BitConverter.GetBytes(data.X), 0, buff, offset, sizeof(float));
                    Buffer.BlockCopy(BitConverter.GetBytes(data.Y), 0, buff, offset + sizeof(float), sizeof(float));

                    offset += 2 * sizeof(float);
                }
            }

            return buff;
        }

        /// <summary>
        /// Create IBO.
        /// </summary>
        private void CreateIBO(GraphicsDevice device, Mesh mesh)
        {
            _parts = new DrawCallPart[mesh.Indices.Length];

            List<UInt16> indices = new List<UInt16>();

            for (int i = 0; i < mesh.Indices.Length; i++)
            {
                int[] partIndices = mesh.Indices[i];

                DrawCallPart newPart = new DrawCallPart();
                newPart.PrimitiveCount = partIndices.Length / 3;
                newPart.PrimitiveType = PrimitiveType.TriangleList;
                newPart.BaseVertex = 0;
                newPart.StartIndex = indices.Count;

                foreach (int partIndex in partIndices)
                    indices.Add((UInt16)partIndex);
                
                _parts[i] = newPart;
            }

            IndexBuffer = new IndexBuffer(device, IndexElementSize.SixteenBits, indices.Count, BufferUsage.WriteOnly);

            IndexBuffer.SetData(indices.ToArray());
        }

        /// <summary>
        /// Render.
        /// </summary>
        public void Render(GraphicsDevice device)
        {
            foreach (DrawCallPart drawCallPart in _parts)
                device.DrawIndexedPrimitives(drawCallPart.PrimitiveType, drawCallPart.BaseVertex, drawCallPart.StartIndex, drawCallPart.PrimitiveCount);
        }
    }
}

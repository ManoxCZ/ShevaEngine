﻿using ShevaEngine.Graphics.Meshes;
using System;
using System.Collections.Generic;

namespace ShevaEngine.Graphics.DrawCalls
{
    /// <summary>
    /// Draw call manager.
    /// </summary>
    public class DrawCallManager
    {
        private readonly GraphicsSystem _graphicsSystem;
        private readonly SortedDictionary<Guid, UInt16> _meshToDrawCallId;
        private readonly SortedDictionary<UInt16, DrawCall> _drawCalls;
        public DrawCall this[UInt16 id]
        {
            get
            {
                if (_drawCalls.ContainsKey(id))
                {
                    return _drawCalls[id];
                }

                return null;
            }
        }
        private UInt16 _counter;


        /// <summary>
        /// Constructor.
        /// </summary>
        internal DrawCallManager(GraphicsSystem graphicsSystem)
        {
            _graphicsSystem = graphicsSystem;

            _meshToDrawCallId = new SortedDictionary<Guid, ushort>();
            _drawCalls = new SortedDictionary<UInt16, DrawCall>();
        }

        /// <summary>
        /// Get draw call id for mesh.
        /// </summary>        
        public UInt16 GetDrawCallForMesh(Mesh mesh)
        {
            if (!_meshToDrawCallId.ContainsKey(mesh.Id))
            {
                UInt16 id = _counter++;

                _drawCalls.Add(id, new DrawCall(_graphicsSystem.GraphicsDevice, mesh));
                _meshToDrawCallId.Add(mesh.Id, id);
            }

            return _meshToDrawCallId[mesh.Id];
        }
    }
}

﻿using Microsoft.Xna.Framework.Graphics;

namespace ShevaEngine.Graphics.DrawCalls
{
    /// <summary>
    /// Draw call part.
    /// </summary>
    public class DrawCallPart
    {
        /// <summary>Typ primitiva.</summary>
        public PrimitiveType PrimitiveType;
        /// <summary>The number of primitives to render.</summary>
        public int PrimitiveCount;
        /// <summary>The location in the index array at which to start reading vertices.</summary>
        public int StartIndex;
        /// <summary>Base vertex.</summary>
        public int BaseVertex;
    }
}

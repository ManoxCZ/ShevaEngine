﻿using System;

namespace ShevaEngine.Graphics.DrawCalls
{
    /// <summary>
    /// Draw call key.
    /// </summary>
    public struct DrawCallKey
    {
        /// <summary>
        /// None DrawCallKey.
        /// </summary>
        public static readonly DrawCallKey NoneDrawCallKey = new DrawCallKey()
        {
            LayerId = DrawCallKey.None,            
            MaterialId = DrawCallKey.None,
            MaterialParamsId = DrawCallKey.None,
            DrawCallId = DrawCallKey.None
        };

        public const UInt16 None = UInt16.MaxValue;

        public UInt16 LayerId;
        public UInt16 MaterialId;
        public UInt16 MaterialParamsId;        
        public UInt16 DrawCallId;


        /// <summary>
        /// Konstruktor.
        /// </summary>
        public DrawCallKey(UInt16 layerId, UInt16 materialId, UInt16 materialParamsId, UInt16 drawCallId)
        {
            LayerId = layerId;
            MaterialId = materialId;
            MaterialParamsId = materialParamsId;
            DrawCallId = drawCallId;
        }        
    }
}

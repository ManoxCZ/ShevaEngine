﻿namespace ShevaEngine.Graphics.Effects
{
    /// <summary>
    /// Effect constants.
    /// </summary>
    public class EffectConstants
    {
        public const string WorldMatrix = "WorldMatrix";
        public const string ViewMatrix = "ViewMatrix";
        public const string ProjMatrix = "ProjMatrix";
        public const string Position = "Position";
    }
}

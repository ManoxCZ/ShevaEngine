using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// Contains information about an animation.
    /// </summary>    
    public class AnimationInfo
    {
        /// <summary>Gets the name of the animation.</summary>
        public string Name { get; set; }
        /// <summary>Gets the total duration of this animation in ticks.</summary>
        public long Duration { get; set; }        
        /// <summary>Gets a collection of channels that represent the bone animation
        /// tracks for this animation.</summary>
        public AnimationChannelCollection AnimationChannels { get; set; }
        
        /// <summary>Gets a collection of bones that have tracks in this animation.</summary>
        [ContentSerializerIgnore]
        public List<string> AffectedBones => AnimationChannels.AffectedBones;


        /// <summary>
        /// Constructor.
        /// </summary>
        public AnimationInfo()
        {

        }

        /// <summary>
        /// Internal because it should only be created by the AnimationReader.
        /// </summary>
        public AnimationInfo(string animationName, long duration, AnimationChannelCollection anims)
        {
            Name = animationName;
            Duration = duration;
            AnimationChannels = anims;

            foreach (BoneKeyframeCollection channel in anims.Values)
                if (Duration < channel.Duration)
                    Duration = channel.Duration;
        }

        /// <summary>
        /// Returns true if the animation contains any tracks that affect the given
        /// bone.
        /// </summary>
        /// <param name="boneName">The bone to test for track information.</param>
        /// <returns>True if the animation contains any tracks that affect the given
        /// bone.</returns>
        public bool AffectsBone(string boneName)
        {
            return AnimationChannels.AffectsBone(boneName);
        }
    }    
}

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// Represents the current pose of a model bone.
    /// </summary>
    public class BonePose
    {
        /// <summary>Used when no animation is set.</summary> 
        public Matrix DefaultMatrix;

        /// <summary>Buffers for interpolation when blending</summary>
        private static Matrix _returnMatrix, _blendMatrix, _currentMatrixBuffer;

        public int BoneIndex { get; }
        public string BoneName { get; }
        public BonePose Parent { get; }
        private IAnimationController _currentAnimation;
        private IAnimationController _currentBlendAnimation;

        /// <summary>THe amount to interpolate between the current animation and the current blend animation.</summary>
        public float BlendFactor = 0;

        public BonePoseCollection Children { get; }

        /// <summary>True if the current animation contains a track for this bone.</summary> 
        private bool _doesAnimContainChannel;

        /// <summary>True if the current blend animation contains a track for this bone.</summary>
        private bool _doesBlendContainChannel;

        /// <summary>
        /// Gets or sets the current animation that affects this bone.  If null,
        /// then DefaultTransform will be used for this bone's transform.
        /// </summary>
        public IAnimationController CurrentController
        {
            get => _currentAnimation;
            set
            {
                // Don't do anything if the animation hasn't changed
                if (_currentAnimation != value)
                {
                    if (value != null)
                    {
                        if (_currentAnimation != null)
                            _currentAnimation.AnimationTracksChanged -= Current_AnimationTracksChanged;

                        if (BoneName != null)
                        {
                            // Update info on whether or not the current anim
                            // contains a track for this bone
                            _doesAnimContainChannel = value.ContainsAnimationTrack(this);
                            value.AnimationTracksChanged += Current_AnimationTracksChanged;
                        }
                    }
                    else // A null animation; use defaulttransform
                        _doesAnimContainChannel = false;

                    _currentAnimation = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the blend animation that affects this bone.  If the value
        /// is null, then no blending will occur.
        /// </summary>
        public IAnimationController CurrentBlendController
        {
            get => _currentBlendAnimation;
            set
            {
                // Don't do anything if the animation hasn't changed
                if (_currentBlendAnimation != value)
                {

                    if (value != null)
                    {
                        if (_currentBlendAnimation != null)
                            _currentBlendAnimation.AnimationTracksChanged -= Blend_AnimationTracksChanged;

                        if (BoneName != null)
                        {
                            // Update info on whether or not the current anim
                            // contains a track for this bone
                            _doesBlendContainChannel =
                                value.ContainsAnimationTrack(this);
                            value.AnimationTracksChanged += Blend_AnimationTracksChanged;
                        }
                    }
                    else
                        _doesBlendContainChannel = false;
                    _currentBlendAnimation = value;
                }
            }
        }

        /// <summary>
        /// Internal creation.
        /// </summary>
        internal BonePose(ModelBone bone, ModelBoneCollection bones, BonePose[] anims)
        {
            // Set the values according to the bone
            BoneIndex = bone.Index;
            BoneName = bone.Name;
            DefaultMatrix = bone.Transform;

            if (bone.Parent != null)
                Parent = anims[bone.Parent.Index];

            anims[BoneIndex] = this;

            // Recurse on children
            List<BonePose> childList = new List<BonePose>();

            foreach (ModelBone child in bone.Children)
            {
                BonePose newChild = new BonePose(
                    bones[child.Index],
                    bones,
                    anims);

                childList.Add(newChild);
            }

            Children = new BonePoseCollection(childList);
        }

        /// <summary>
        /// Finds the hierarchy for which this bone is the root.
        /// </summary>
        private void FindHierarchy(List<BonePose> poses)
        {
            poses.Add(this);

            foreach (BonePose child in Children)
                child.FindHierarchy(poses);
        }

        /// <summary>
        /// Finds a collection of bones that represents the tree of BonePoses with
        /// the current BonePose as the root.
        /// </summary>
        public BonePoseCollection GetHierarchy()
        {
            List<BonePose> poses = new List<BonePose>();

            FindHierarchy(poses);

            return new BonePoseCollection(poses);
        }

        /// <summary>
        /// Current animation tracks changed.
        /// </summary>
        private void Current_AnimationTracksChanged(object sender, EventArgs e)
        {
            _doesAnimContainChannel = _currentAnimation.ContainsAnimationTrack(this);
        }

        /// <summary>
        /// Blend animation tracks changed.
        /// </summary>
        private void Blend_AnimationTracksChanged(object sender, EventArgs e)
        {
            _doesBlendContainChannel = _currentBlendAnimation.ContainsAnimationTrack(this);
        }

        /// <summary>
        /// Calculates the current transform, based on the animations, for the bone
        /// represented by the BonePose object.
        /// </summary>
        public Matrix GetCurrentTransform()
        {
            // If the bone is not currently affected by an animation
            if (_currentAnimation == null || !_doesAnimContainChannel)
            {
                // If the bone is affected by a blend animation,
                // blend the defaultTransform with the blend animation
                if (_currentBlendAnimation != null && _doesBlendContainChannel)
                {
                    _blendMatrix = _currentBlendAnimation.GetCurrentBoneTransform(this);

                    Util.SlerpMatrix(ref DefaultMatrix, ref _blendMatrix, BlendFactor, out _returnMatrix);
                }
                // else return the default transform
                else
                    return DefaultMatrix;
            }
            // The bone is affected by an animation
            else
            {
                // Find the current transform in the animation for the bone
                _currentMatrixBuffer = _currentAnimation.GetCurrentBoneTransform(this);
                // If the bone is affected by a blend animation, blend the
                // current animation transform with the current blend animation
                // transform
                if (_currentBlendAnimation != null && _doesBlendContainChannel)
                {
                    _blendMatrix = _currentBlendAnimation.GetCurrentBoneTransform(this);

                    Util.SlerpMatrix(ref _currentMatrixBuffer, ref _blendMatrix, BlendFactor, out _returnMatrix);
                }
                // Else just return the current animation transform
                else
                    return _currentMatrixBuffer;
            }

            return _returnMatrix;
        }
    }
}
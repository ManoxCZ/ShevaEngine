﻿using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// A collection of AnimationInfo objects.
    /// </summary>
    public class AnimationInfoCollection : List<AnimationInfo>
    {
        /// <summary>
        /// Gets a collection of animations stored in the model.
        /// </summary>
        /// <param name="model">The model that contains the animations.</param>
        /// <returns>The animations stored in the model.</returns>
        public static AnimationInfoCollection FromModel(Model model)
        {
            // Grab the tag that was set in the processor; this is a dictionary so that users can extend
            // the processor and pass their own data into the program without messing up the animation data
            Dictionary<string, object> modelTagData = (Dictionary<string, object>)model.Tag;

            if (modelTagData == null || !modelTagData.ContainsKey("Animations"))
            {
                return new AnimationInfoCollection();
            }
            else
            {
                AnimationInfoCollection animations = (AnimationInfoCollection)modelTagData["Animations"];
                return animations;
            }
        }

        ///// <summary>
        ///// Gets the AnimationInfo object at the given index.
        ///// </summary>
        ///// <param name="index">The index of the AnimationInfo object.</param>
        ///// <returns>The AnimationInfo object at the given index.</returns>
        //public AnimationInfo this[int index]
        //{
        //    get
        //    {
        //        return this[index];
        //    }
        //}
    }
}

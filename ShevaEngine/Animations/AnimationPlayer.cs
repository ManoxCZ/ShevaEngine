using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// The animation player is in charge of decoding bone position
    /// matrices from an animation clip.
    /// </summary>
    public class AnimationPlayer
    {
        // Information about the currently playing animation clip.
        private AnimationClip _currentClipValue;
        private TimeSpan _currentTimeValue;
        private int _currentKeyframe;

        private AnimationClip _previousClipValue;
        private TimeSpan _previousTimeValue;
        private int _previousKeyframe;
        /// <summary>Interpolation Time.</summary>
        private double _interpolationTime;
        /// <summary>Actual Interpolation Time.</summary>
        private double _actualInterpolationTime;

        // Current animation transform matrices.
        Matrix[] BoneTransforms;
        Matrix[] PreviousBoneTransforms;
        Matrix[] worldTransforms;
        /// <summary>Animation transforms.</summary>
        public Matrix[] AnimationTransforms { get; private set; }


        // Backlink to the bind pose and skeleton hierarchy data.
        SkinningData SkinningDataValue;
        ///// <summary>Attached objects.</summary>
        //private List<AttachToAnimationBridge>[] AttachedObjects;

        /// <summary>
        /// Constructs a new animation player.
        /// </summary>
        public AnimationPlayer(SkinningData skinningData)
        {
            if (skinningData == null)
                throw new ArgumentNullException("skinningData");

            SkinningDataValue = skinningData;

            BoneTransforms = new Matrix[skinningData.BindPose.Count];
            PreviousBoneTransforms = new Matrix[skinningData.BindPose.Count];
            worldTransforms = new Matrix[skinningData.BindPose.Count];
            AnimationTransforms = new Matrix[skinningData.BindPose.Count];
            //AttachedObjects = new List<AttachToAnimationBridge>[skinningData.BindPose.Count];

            //for (int i = 0; i < skinningData.BindPose.Count; i++)
            //    AttachedObjects[i] = new List<AttachToAnimationBridge>();
        }
        
        /// <summary>
        /// Starts decoding the specified animation clip.
        /// </summary>
        public void StartClip(string clipName)
        {
            if (!SkinningDataValue.AnimationClips.ContainsKey(clipName))
                if (SkinningDataValue.AnimationClips.Count > 0)
                    clipName = SkinningDataValue.AnimationClips.First<KeyValuePair<string, AnimationClip>>(item => true).Key;
                else
                    throw new InvalidOperationException("No animations !");

            _previousClipValue = _currentClipValue;
            _previousTimeValue = _currentTimeValue;
            _previousKeyframe = _currentKeyframe;

            for (int i = BoneTransforms.Length - 1; i >= 0; i--)
                PreviousBoneTransforms[i] = BoneTransforms[i];

            _currentClipValue = SkinningDataValue.AnimationClips[clipName];
            _currentTimeValue = TimeSpan.Zero;
            _currentKeyframe = 0;

            _interpolationTime = 0.1;
            _actualInterpolationTime = _interpolationTime;

            // Initialize bone transforms to the bind pose.
            SkinningDataValue.BindPose.CopyTo(BoneTransforms, 0);
        }
        
        /// <summary>
        /// Advances the current animation position.
        /// </summary>
        public void Update(GameTime time)
        {
            Matrix rootTransform = Matrix.Identity;

            UpdateBoneTransforms(time.ElapsedGameTime);
            UpdateWorldTransforms(rootTransform, BoneTransforms);
            UpdateSkinTransforms();
        }


        /// <summary>
        /// Helper used by the Update method to refresh the BoneTransforms data.
        /// </summary>
        public void UpdateBoneTransforms(TimeSpan time)
        {
            if (_currentClipValue == null)
                throw new InvalidOperationException(
                            "AnimationPlayer.Update was called before StartClip");

            // Update the animation position.
            _currentTimeValue += time;

            // If we reached the end, loop back to the start.
            while (_currentTimeValue.TotalSeconds >= _currentClipValue.Duration.TotalSeconds)
            {
                _currentTimeValue = _currentTimeValue.Subtract(_currentClipValue.Duration);
                _currentKeyframe = 0;
            }

            // Read keyframe matrices.
            IList<Keyframe> currentkeyframes = _currentClipValue.Keyframes;

            if (_previousClipValue != null)
            {
                // Update the animation position.
                _previousTimeValue += time;

                // If we reached the end, loop back to the start.
                while (_previousTimeValue.TotalSeconds >= _previousClipValue.Duration.TotalSeconds)
                {
                    _previousTimeValue = _previousTimeValue.Subtract(_previousClipValue.Duration);
                    _previousKeyframe = 1;
                }

                // Read keyframe matrices.
                IList<Keyframe> previousKeyframes = _previousClipValue.Keyframes;                

                while (_currentKeyframe < currentkeyframes.Count)
                {
                    Keyframe currentKeyframe = currentkeyframes[_currentKeyframe];

                    // Stop when we've read up to the current time position.
                    if (currentKeyframe.Time > _currentTimeValue)
                        break;

                    // Use this keyframe.
                    BoneTransforms[currentKeyframe.Bone] = currentKeyframe.Transform;

                    _currentKeyframe++;
                }
                
                while (_previousKeyframe < previousKeyframes.Count)
                {
                    Keyframe previousKeyframe = previousKeyframes[_previousKeyframe];

                    // Stop when we've read up to the current time position.
                    if (previousKeyframe.Time > _previousTimeValue)
                        break;

                    PreviousBoneTransforms[previousKeyframe.Bone] = previousKeyframe.Transform;

                    // Use this keyframe.
                    //boneTransforms[previousKeyframe.Bone] =
                    //    Matrix.Lerp(boneTransforms[previousKeyframe.Bone], previousKeyframe.Transform, 0.5f);

                    _previousKeyframe++;
                }

                for (int i = 0; i < BoneTransforms.Length; i++)
                {
                    double test = (float)(_actualInterpolationTime / _interpolationTime);

                    BoneTransforms[i] = Matrix.Lerp(BoneTransforms[i], PreviousBoneTransforms[i],
                        (float)(_actualInterpolationTime / _interpolationTime));
                }

                _actualInterpolationTime -= time.TotalSeconds;

                if (_actualInterpolationTime < 0)
                    _previousClipValue = null;
            }
            else
            {
                while (_currentKeyframe < currentkeyframes.Count)
                {
                    Keyframe currentKeyframe = currentkeyframes[_currentKeyframe];

                    // Stop when we've read up to the current time position.
                    if (currentKeyframe.Time > _currentTimeValue)
                        break;

                    // Use this keyframe.
                    BoneTransforms[currentKeyframe.Bone] = currentKeyframe.Transform;

                    _currentKeyframe++;
                }
            }
        }


        /// <summary>
        /// Helper used by the Update method to refresh the WorldTransforms data.
        /// </summary>
        public void UpdateWorldTransforms(Matrix rootTransform, Matrix[] boneTransforms)
        {
            // Root bone.
            worldTransforms[0] = boneTransforms[0] * rootTransform;

            // Child bones.
            for (int bone = 1; bone < worldTransforms.Length; bone++)
            {
                int parentBone = SkinningDataValue.SkeletonHierarchy[bone];

                worldTransforms[bone] = boneTransforms[bone] *
                                             worldTransforms[parentBone];

                //for (int i = AttachedObjects[bone].Count - 1; i >= 0; i--)
                //    AttachedObjects[bone][i].AnimationWorld = worldTransforms[bone];
            }
        }


        /// <summary>
        /// Helper used by the Update method to refresh the SkinTransforms data.
        /// </summary>
        public void UpdateSkinTransforms()
        {
            for (int bone = 0; bone < AnimationTransforms.Length; bone++)
            {
                AnimationTransforms[bone] =
                    SkinningDataValue.InverseBindPose[bone] * worldTransforms[bone];
            }
        }


        /// <summary>
        /// Gets the current bone transform matrices, relative to their parent bones.
        /// </summary>
        public Matrix[] GetBoneTransforms()
        {
            return BoneTransforms;
        }


        /// <summary>
        /// Gets the current bone transform matrices, in absolute format.
        /// </summary>
        public Matrix[] GetWorldTransforms()
        {
            return worldTransforms;
        }


        /// <summary>
        /// Gets the current bone transform matrices,
        /// relative to the skinning bind pose.
        /// </summary>
        public Matrix[] GetSkinTransforms()
        {
            return AnimationTransforms;
        }


        /// <summary>
        /// Gets the clip currently being decoded.
        /// </summary>
        public AnimationClip CurrentClip => _currentClipValue;


        /// <summary>
        /// Gets the current play position.
        /// </summary>
        public TimeSpan CurrentTime => _currentTimeValue;

        ///// <summary>
        ///// Method attaches scene object to bone.
        ///// </summary>
        ///// <param name="sceneNode"></param>
        ///// <param name="boneName"></param>
        ///// <returns></returns>
        //public bool AttachSceneObjectToBone(AttachToAnimationBridge sceneObject, string boneName)
        //{
        //    if (SkinningDataValue.BoneIndices.ContainsKey(boneName))
        //    {
        //        AttachedObjects[SkinningDataValue.BoneIndices[boneName]].Add(sceneObject);

        //        return true;
        //    }

        //    return false;
        //}
    }
}

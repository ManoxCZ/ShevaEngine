﻿using Microsoft.Xna.Framework;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// Represents a keyframe in an animation track.
    /// </summary>
    public class BoneKeyframe
    {
        /// <summary>The transform for the keyframe.</summary>
        public Matrix Transform { get; set; }
        /// <summary>The time for the keyframe.</summary>
        public long Time { get; set; }

        /// <summary>
        /// Konstruktor pro content pipeline.
        /// </summary>
        public BoneKeyframe()
        {

        }

        /// <summary>
        /// Creats a new BoneKeyframe.
        /// </summary>
        /// <param name="time">The time in ticks for the keyframe.</param>
        /// <param name="transform">The transform for the keyframe.</param>        
        public BoneKeyframe(Matrix transform, long time)
        {
            Transform = transform;
            Time = time;
        }
    }    
}

﻿using System.Collections.Generic;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// A collection of animation channels or tracks, which are sections of an
    /// animation that run for one bone.
    /// </summary>
    public class AnimationChannelCollection : Dictionary<string, BoneKeyframeCollection>
    {
        public List<string> AffectedBones => new List<string>(Keys);

        /// <summary>
        /// Constructor.
        /// </summary>
        public AnimationChannelCollection()
        {
        }

        // This immutable data structure should not be created by the library user
        public AnimationChannelCollection(Dictionary<string, BoneKeyframeCollection> channels)
            : base(channels)
        {
            ////            Debugger.Launch();

            //            // Find the affected bones
            //            //affectedBones = new List<string>();
            //            BoneKeyframes = new Dictionary<string, BoneKeyframeCollection>();

            //            foreach (BoneKeyframeCollection frames in channels)
            //            {
            //                BoneKeyframes.Add(frames.BoneName, frames);
            //              //  affectedBones.Add(frames.BoneName);
            //            }                       
        }

        ///// <summary>
        ///// Gets the BoneKeyframeCollection that is associated with the given bone.
        ///// </summary>
        ///// <param name="boneName">The name of the bone that contains a track in this
        ///// AnimationChannelCollection.</param>
        ///// <returns>The track associated with the given bone.</returns>
        //[ContentSerializerIgnore]
        //public BoneKeyframeCollection this[string boneName]
        //{
        //    get { return this[boneName]; }
        //}

        /// <summary>
        /// See AnimationInfo's equivalent method for documentation        
        /// </summary>
        public bool AffectsBone(string boneName)
        {
            return ContainsKey(boneName);
        }
    }
}

﻿//using ShevaEngine.Animations.AnimTree.Parameters;
//using ShevaEngine.Animations.AnimTree.States;
//using System.Collections.Generic;

//namespace ShevaEngine.Animations.AnimTree
//{
//    /// <summary>
//    /// Animation graph.
//    /// </summary>    
//    public class AnimationGraph
//    {
//        public List<AnimationState> AnimationStates;
//        private IAnimationState _activeState;
//        public List<IAnimationParameter> Parameters;



//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public AnimationGraph()
//        {
//            AnimationStates = new List<AnimationState>();
//            Parameters = new List<IAnimationParameter>();
//        }

//        /// <summary>
//        /// Intialize.
//        /// </summary>
//        public void Initialize(AnimationController controller)
//        { 
//            foreach (AnimationState item in AnimationStates)
//                item.Initialize(controller);
//        }

//        /// <summary>
//        /// Update.
//        /// </summary>
//        public void Update(AnimationController controller)
//        {
//            _activeState?.Update(controller);
//        }

//        /// <summary>
//        /// Build blend tree.
//        /// </summary>
//        public void BuildBlendTree(FastList<AnimationOperation> animationList)
//        {
//            _activeState?.BuildBlendTree(animationList);
//        }

//        /// <summary>
//        /// Set active state.
//        /// </summary>
//        public bool SetActiveState(IAnimationState newActiveState)
//        {
//            _activeState = newActiveState;

//            return true;
//        }

//        /// <summary>
//        /// Create parameter.
//        /// </summary>
//        public AnimationParameter<T> CreateParameter<T>(string name, T defaultValue = default(T))
//        {
//            AnimationParameter<T> newParameter = new AnimationParameter<T>(name, defaultValue);

//            Parameters.Add(newParameter);

//            return newParameter;
//        }

//        /// <summary>
//        /// Get parameter.
//        /// </summary>
//        public AnimationParameter<T> GetParameter<T>(string name)
//        {
//            foreach (IAnimationParameter item in Parameters)
//                if (item.Name == name && item is AnimationParameter<T>)
//                    return item as AnimationParameter<T>;

//            return null;
//        }

//        /// <summary>
//        /// Set parameter.
//        /// </summary>        
//        public bool SetParameter<T>(string name, T data)
//        {
//            AnimationParameter<T> parameter = GetParameter<T>(name);

//            if (parameter != null)
//            {
//                ((AnimationParameter<T>)parameter).Data = data;

//                return true;
//            }

//            return false;
//        }

//        /// <summary>
//        /// Raise event.
//        /// </summary>
//        public bool RaiseEvent(AnimationController controller, string eventName)
//        {
//            _activeState.ProcessEvent(controller, eventName);

//            return true;
//        }
//    }
//}

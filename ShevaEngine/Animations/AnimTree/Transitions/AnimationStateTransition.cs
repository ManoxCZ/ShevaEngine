﻿//using ShevaEngine.Animations.AnimTree.States;

//namespace ShevaEngine.Animations.AnimTree.Transitions
//{
//    /// <summary>
//    /// Animation state transition.
//    /// </summary>
//    public abstract class AnimationStateTransition : IAnimationState
//    {        
//        public AnimationState FromState;
//        public AnimationState ToState;
//        public float Time;
//        protected float ActualTime;
//        protected bool Running;


//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        protected AnimationStateTransition()
//        {            
//            Running = false;
//        }

//        /// <summary>
//        /// Build blend tree.
//        /// </summary>
//        public virtual void BuildBlendTree(FastList<AnimationOperation> blendStack)
//        {
//            FromState.BuildBlendTree(blendStack);
//            ToState.BuildBlendTree(blendStack);

//            blendStack.Add(AnimationOperation.NewBlend(CoreAnimationOperation.Blend, ActualTime / Time));
//        }

//        /// <summary>
//        /// Update method.
//        /// </summary>
//        public virtual void Update(AnimationController controller)
//        {
//            if (Running)
//            {
//                ActualTime += (float)controller.Game.UpdateTime.Elapsed.TotalSeconds;

//                if (ActualTime >= Time)
//                {
//                    Running = false;

//                    controller.AnimationGraph.SetActiveState(ToState);
//                }
//            }            
//        }     
        
//        /// <summary>
//        /// Process event.
//        /// </summary>
//        public virtual void ProcessEvent(AnimationController controller, string eventName)
//        {

//        }
//    }
//}

﻿//using System.Collections.Generic;
//using ShevaEngine.Animations.AnimTree.Conditions;

//namespace ShevaEngine.Animations.AnimTree.Transitions
//{
//    /// <summary>
//    /// Condition animation state transition.
//    /// </summary>
//    public class ConditionsAnimationStateTransition : AnimationStateTransition
//    {
//        public List<IAnimationCondition> Conditions;


//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public ConditionsAnimationStateTransition()
//        {
//            Conditions = new List<IAnimationCondition>();
//        }

//        /// <summary>
//        /// Update method.
//        /// </summary>
//        public void CheckConditions(AnimationController controller)
//        {
//            if (IsTrue(controller))
//            {
//                ActualTime = 0;

//                Running = true;

//                controller.AnimationGraph.SetActiveState(this);
//            }
//        }

//        /// <summary>
//        /// Is true.
//        /// </summary>
//        private bool IsTrue(AnimationController controller)
//        {
//            foreach (IAnimationCondition condition in Conditions)
//                if (!condition.IsTrue(controller))
//                    return false;

//            return true;
//        }
//    }
//}

﻿//using ShevaEngine.Animations.AnimTree.Parameters;

//namespace ShevaEngine.Animations.AnimTree.Conditions
//{
//    /// <summary>
//    /// Float operation.
//    /// </summary>
//    public enum FloatOperation
//    {
//        Equal,
//        LessEqual,
//        Less,
//        GreaterEqual,
//        Greater,
//    }

//    /// <summary>
//    /// Float animation parameter aniamtion condition.
//    /// </summary>
//    public class FloatAnimationParameterAnimationCondition : AnimationParameterAnimationCondition
//    {
//        public float ReferenceValue;
//        public FloatOperation Operation;


//        /// <summary>
//        /// Is condition true.
//        /// </summary>
//        public override bool IsConditionTrue(IAnimationParameter parameter)
//        {
//            if (parameter is AnimationParameter<float>)
//            {
//                switch (Operation)
//                {
//                    case FloatOperation.Equal:
//                        return ((AnimationParameter<float>)parameter).Data == ReferenceValue;
//                    case FloatOperation.LessEqual:
//                        return ((AnimationParameter<float>)parameter).Data <= ReferenceValue;
//                    case FloatOperation.Less:
//                        return ((AnimationParameter<float>)parameter).Data < ReferenceValue;
//                    case FloatOperation.GreaterEqual:
//                        return ((AnimationParameter<float>)parameter).Data >= ReferenceValue;
//                    case FloatOperation.Greater:
//                        return ((AnimationParameter<float>)parameter).Data > ReferenceValue;                    
//                }
//            }

//            return false;
//        }
//    }
//}

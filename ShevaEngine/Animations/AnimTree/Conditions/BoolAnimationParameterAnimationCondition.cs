﻿//using ShevaEngine.Animations.AnimTree.Parameters;

//namespace ShevaEngine.Animations.AnimTree.Conditions
//{
//    /// <summary>
//    /// Boolean operation.
//    /// </summary>
//    public enum BooleanOperation
//    {
//        Equal,
//        NotEqual,
//    }

//    /// <summary>
//    /// Bool animation parameter aniamtion condition.
//    /// </summary>
//    public class BoolAnimationParameterAnimationCondition : AnimationParameterAnimationCondition
//    {
//        public bool ReferenceValue;
//        public BooleanOperation Operation;


//        /// <summary>
//        /// Is condition true.
//        /// </summary>
//        public override bool IsConditionTrue(IAnimationParameter parameter)
//        {            
//            if (parameter is AnimationParameter<bool>)            
//            {
//                switch (Operation)
//                {
//                    case BooleanOperation.Equal:
//                        return ((AnimationParameter<bool>)parameter).Data == ReferenceValue;                        
//                    case BooleanOperation.NotEqual:
//                        return ((AnimationParameter<bool>)parameter).Data != ReferenceValue;
//                }
//            }

//            return false;
//        }
//    }
//}

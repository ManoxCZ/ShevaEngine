﻿//using ShevaEngine.Animations.AnimTree.Parameters;

//namespace ShevaEngine.Animations.AnimTree.Conditions
//{
//    /// <summary>
//    /// Int operation.
//    /// </summary>
//    public enum IntOperation
//    {
//        Equal,
//        LessEqual,
//        Less,
//        GreaterEqual,
//        Greater,
//    }

//    /// <summary>
//    /// Int animation parameter aniamtion condition.
//    /// </summary>
//    public class IntAnimationParameterAnimationCondition : AnimationParameterAnimationCondition
//    {
//        public int ReferenceValue;
//        public IntOperation Operation;


//        /// <summary>
//        /// Is condition true.
//        /// </summary>
//        public override bool IsConditionTrue(IAnimationParameter parameter)
//        {
//            if (parameter is AnimationParameter<int>)
//            {
//                switch (Operation)
//                {
//                    case IntOperation.Equal:
//                        return ((AnimationParameter<int>)parameter).Data == ReferenceValue;
//                    case IntOperation.LessEqual:
//                        return ((AnimationParameter<int>)parameter).Data <= ReferenceValue;
//                    case IntOperation.Less:
//                        return ((AnimationParameter<int>)parameter).Data < ReferenceValue;
//                    case IntOperation.GreaterEqual:
//                        return ((AnimationParameter<int>)parameter).Data >= ReferenceValue;
//                    case IntOperation.Greater:
//                        return ((AnimationParameter<int>)parameter).Data > ReferenceValue;                    
//                }
//            }

//            return false;
//        }
//    }
//}

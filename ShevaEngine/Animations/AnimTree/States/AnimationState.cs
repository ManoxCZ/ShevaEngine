﻿//using System.Collections.Generic;
//using ShevaEngine.Animations.AnimTree.Transitions;

//namespace ShevaEngine.Animations.AnimTree.States
//{
//    /// <summary>
//    /// Animation state.
//    /// </summary>
//    public sealed class AnimationState : IAnimationState
//    {
//        public string Name;
//        public AnimationStateData AnimationStateData;
//        public long CurrentTicks;
//        public List<AnimationStateTransition> Transitions;


//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public AnimationState()
//        {
//            Transitions = new List<AnimationStateTransition>();
//        }

//        /// <summary>
//        /// Initialize.
//        /// </summary>
//        public void Initialize(AnimationController controller)
//        {
//            AnimationStateData?.Initialize(controller, this);
//        }

//        /// <summary>
//        /// Build blend tree.
//        /// </summary>
//        public void BuildBlendTree(FastList<AnimationOperation> blendStack)
//        {
//            AnimationStateData?.BuildBlendTree(blendStack, this);
//        }

//        /// <summary>
//        /// Update method.
//        /// </summary>
//        public void Update(AnimationController controller)
//        {
//            CurrentTicks += controller.Game.UpdateTime.Elapsed.Ticks;

//            AnimationStateData?.Update(controller, this);

//            foreach (AnimationStateTransition transition in Transitions)
//                if (transition is ConditionsAnimationStateTransition)
//                    (transition as ConditionsAnimationStateTransition).CheckConditions(controller);
//        }

//        /// <summary>
//        /// Process event.
//        /// </summary>
//        public void ProcessEvent(AnimationController controller, string eventName)
//        {
//            foreach (AnimationStateTransition transition in Transitions)
//                if (transition is EventAnimationStateTransition)
//                    controller.AnimationGraph.SetActiveState(transition);                    
//        }

//        /// <summary>
//        /// ToString override.
//        /// </summary>
//        /// <returns></returns>
//        public override string ToString()
//        {
//            return Name;
//        }
//    }    
//}

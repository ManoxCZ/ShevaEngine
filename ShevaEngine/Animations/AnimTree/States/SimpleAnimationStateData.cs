﻿//using System;

//namespace ShevaEngine.Animations.AnimTree.States
//{
//    /// <summary>
//    /// Simple animation state.
//    /// </summary>
//    public class SimpleAnimationStateData : AnimationStateData
//    {
//        public AnimationClip Clip;
//        //[DataMemberIgnore]
//        public AnimationClipEvaluator Evaluator;


//        /// <summary>
//        /// Initialize.
//        /// </summary>
//        public override void Initialize(AnimationController controller, AnimationState state)
//        {
//            Evaluator = controller.AnimationComponent.Blender.CreateEvaluator(Clip);
//        }

//        /// <summary>
//        /// Update.
//        /// </summary>
//        public override void Update(AnimationController controller, AnimationState state)
//        {
//        }

//        /// <summary>
//        /// Build blend tree.
//        /// </summary>
//        public override void BuildBlendTree(FastList<AnimationOperation> blendStack, AnimationState state)
//        {
//            blendStack.Add(AnimationOperation.NewPush(Evaluator, TimeSpan.FromTicks(state.CurrentTicks % (long)Clip.Duration.Ticks)));
//        }
//    }
//}

﻿//using ShevaEngine.Animations.AnimTree.Parameters;
//using System;

//namespace ShevaEngine.Animations.AnimTree.States
//{
//    /// <summary>
//    /// Blend tree animation state.
//    /// </summary>
//    public class BlendTreeAnimationStateData : AnimationStateData
//    {
//        public AnimationClip[] Clips;
//        public float[] Parameters;
//        public AnimationClipEvaluator[] Evaluators;
//        public AnimationParameter<float> ControlParameter;


//        /// <summary>
//        /// Initialize.
//        /// </summary>
//        public override void Initialize(AnimationController controller, AnimationState state)
//        {
//            Evaluators = new AnimationClipEvaluator[Clips.Length];

//            for (int i = 0; i < Clips.Length; i++)
//                Evaluators[i] = controller.AnimationComponent.Blender.CreateEvaluator(Clips[i]);
//        }

//        /// <summary>
//        /// Update.
//        /// </summary>
//        public override void Update(AnimationController controller, AnimationState state)
//        {

//        }

//        /// <summary>
//        /// Build blend tree.
//        /// </summary>
//        public override void BuildBlendTree(FastList<AnimationOperation> blendStack, AnimationState state)
//        {
//            for (int i = 0; i < Parameters.Length - 1; i++)
//                if (Parameters[i] <= ControlParameter.Data && Parameters[i + 1] > ControlParameter.Data)
//                {
//                    blendStack.Add(AnimationOperation.NewPush(Evaluators[i],
//                        TimeSpan.FromTicks(state.CurrentTicks % (long)Clips[i].Duration.Ticks)));
//                    blendStack.Add(AnimationOperation.NewPush(Evaluators[i + 1],
//                        TimeSpan.FromTicks(state.CurrentTicks % (long)Clips[i + 1].Duration.Ticks)));

//                    float aspect = (ControlParameter.Data - Parameters[i]) / (Parameters[i + 1] - Parameters[i]);

//                    blendStack.Add(AnimationOperation.NewBlend(CoreAnimationOperation.Blend, aspect));

//                    return;
//                }

//            blendStack.Add(AnimationOperation.NewPush(Evaluators[Evaluators.Length - 1],
//                TimeSpan.FromTicks(state.CurrentTicks % (long)Clips[Clips.Length - 1].Duration.Ticks)));
//        }
//    }
//}
﻿//using System;

//namespace ShevaEngine.Animations.AnimTree.States
//{
//    /// <summary>
//    /// Simple animation state.
//    /// </summary>
//    public class MultipleAnimationStateData : AnimationStateData
//    {
//        public AnimationClip[] Clips;
//        //[DataMemberIgnore]
//        public AnimationClipEvaluator[] Evaluators;
//        private int ActualClipIndex = 0;
//        public AnimationClip ActualClip => Clips[ActualClipIndex];
//        public AnimationClipEvaluator ActualEvaluator => Evaluators[ActualClipIndex];


//        /// <summary>
//        /// Initialize.
//        /// </summary>
//        public override void Initialize(AnimationController controller, AnimationState state)
//        {
//            Evaluators = new AnimationClipEvaluator[Clips.Length];

//            for (int i = 0; i < Clips.Length; i++)
//                Evaluators[i] = controller.AnimationComponent.Blender.CreateEvaluator(Clips[i]);
//        }

//        /// <summary>
//        /// Update.
//        /// </summary>
//        public override void Update(AnimationController controller, AnimationState state)
//        {
//        }

//        /// <summary>
//        /// Build blend tree.
//        /// </summary>
//        public override void BuildBlendTree(FastList<AnimationOperation> blendStack, AnimationState state)
//        {
//            blendStack.Add(AnimationOperation.NewPush(ActualEvaluator, TimeSpan.FromTicks(state.CurrentTicks % (long)ActualClip.Duration.Ticks)));
//        }
//    }
//}
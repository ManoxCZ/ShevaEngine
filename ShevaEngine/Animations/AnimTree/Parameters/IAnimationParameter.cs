﻿namespace ShevaEngine.Animations.AnimTree.Parameters
{
    /// <summary>
    /// Interface animation parameter.
    /// </summary>
    public interface IAnimationParameter
    {
        /// <summary>Name.</summary>
        string Name { get; }
    }
}

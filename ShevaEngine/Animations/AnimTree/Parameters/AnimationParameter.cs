﻿namespace ShevaEngine.Animations.AnimTree.Parameters
{
    /// <summary>
    /// Animation parameter.
    /// </summary>
    public class AnimationParameter<T> : IAnimationParameter
    {
        public string Name { get; }
        public T Data;


        /// <summary>
        /// Constructor.
        /// </summary>
        internal AnimationParameter(string name, T defaultValue = default(T))
        {
            Name = name;
            Data = defaultValue;
        }
    }
}

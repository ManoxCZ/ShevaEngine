﻿//using ShevaEngine.Entities.Components;

//namespace ShevaEngine.Animations.AnimTree
//{
//    /// <summary>
//    /// Animation controller.
//    /// </summary>    
//    public class AnimationController : SyncScriptComponent//, IBlendTreeBuilder
//    {
//        public AnimationComponent AnimationComponent;
//        //[DataMemberIgnore]
//        private AnimationGraph _animationGraph;
//        //[DataMemberIgnore]
//        public AnimationGraph AnimationGraph
//        {
//            get { return _animationGraph; }
//            set
//            {
//                if (_animationGraph != value)
//                {
//                    _animationGraph = value;

//                    _animationGraph.Initialize(this);
//                }
//            }
//        }


//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public AnimationController()
//        {
//            AnimationGraph = null;
//        }

//        /// <summary>
//        /// Start.
//        /// </summary>
//        public override void Start()
//        {
//            base.Start();
            
//            AnimationComponent.BlendTreeBuilder = this;                  
//        }

//        /// <summary>
//        /// Update.
//        /// </summary>
//        public override void Update()
//        {
//            AnimationGraph?.Update(this);
//        }

//        /// <summary>
//        /// Build blend tree.
//        /// </summary>
//        public void BuildBlendTree(FastList<AnimationOperation> animationList)
//        {
//            AnimationGraph?.BuildBlendTree(animationList);
//        }

//        /// <summary>
//        /// Set parameter.
//        /// </summary>        
//        public bool SetParameter<T>(string name, T data)
//        {
//            if (AnimationGraph != null)
//                return AnimationGraph.SetParameter<T>(name, data);

//            return false;
//        }       
        
//        /// <summary>
//        /// Raise event.
//        /// </summary>
//        public bool RaiseEvent(string eventName)
//        {
//            if (AnimationGraph != null)
//                return AnimationGraph.RaiseEvent(this, eventName);

//            return false;
//        }
//    }
//}

﻿using System;
using System.Collections.Generic;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// Animations manager.
    /// </summary>
    public class AnimationsManager
    {
        /// <summary>Singleton.</summary>
        public static readonly AnimationsManager Instance = new AnimationsManager();

        /// <summary>Animation players.</summary>
        private List<AnimationPlayer> AnimationPlayers;
        /// <summary>Indexer.</summary>
        private Int32 Indexer;

        /// <summary>
        /// Constructor.
        /// </summary>
        public AnimationsManager()
        {
            AnimationPlayers = new List<AnimationPlayer>();
        }

        /// <summary>
        /// Method Update().
        /// </summary>
        /// <param name="time"></param>
        public void Update(Microsoft.Xna.Framework.GameTime time)
        {
            for (Indexer = AnimationPlayers.Count - 1; Indexer >= 0; Indexer--)                
                AnimationPlayers[Indexer].Update(time);
        }

        /// <summary>
        /// Method creates new Animation Player.
        /// </summary>
        /// <param name="skinnigData"></param>
        /// <returns></returns>
        internal AnimationPlayer CreateAnimationPlayer(SkinningData skinningData)
        {
            AnimationPlayer newAnimationPlayer = new AnimationPlayer(skinningData);

            AnimationPlayers.Add(newAnimationPlayer);

            return newAnimationPlayer;
        }
    }
}

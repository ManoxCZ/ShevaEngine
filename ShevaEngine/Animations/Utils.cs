using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// Provides various animation utilities.
    /// </summary>
    public sealed class Util
    {
        public const long TicksPer60Fps = TimeSpan.TicksPerSecond / 60;

        private static Quaternion _qStart, _qEnd, _qResult;
        private static Vector3 _curTrans, _nextTrans, _lerpedTrans;
        private static Vector3 _curScale, _nextScale, _lerpedScale;
        private static Matrix _startRotation, _endRotation;
        private static Matrix _returnMatrix;


        /// <summary>
        /// Roughly decomposes two matrices and performs spherical linear interpolation
        /// </summary>
        /// <param name="start">Source matrix for interpolation</param>
        /// <param name="end">Destination matrix for interpolation</param>
        /// <param name="slerpAmount">Ratio of interpolation</param>
        /// <returns>The interpolated matrix</returns>
        public static Matrix SlerpMatrix(Matrix start, Matrix end, float slerpAmount)
        {
            if (start == end)
                return start;
            // Get rotation components and interpolate (not completely accurate but I don't want 
            // to get into polar decomposition and this seems smooth enough)
            Quaternion.CreateFromRotationMatrix(ref start, out _qStart);
            Quaternion.CreateFromRotationMatrix(ref end, out _qEnd);
            Quaternion.Lerp(ref _qStart, ref _qEnd, slerpAmount, out _qResult);

            // Get final translation components
            _curTrans.X = start.M41;
            _curTrans.Y = start.M42;
            _curTrans.Z = start.M43;
            _nextTrans.X = end.M41;
            _nextTrans.Y = end.M42;
            _nextTrans.Z = end.M43;
            Vector3.Lerp(ref _curTrans, ref _nextTrans, slerpAmount, out _lerpedTrans);

            // Get final scale component
            Matrix.CreateFromQuaternion(ref _qStart, out _startRotation);
            Matrix.CreateFromQuaternion(ref _qEnd, out _endRotation);
            _curScale.X = start.M11 - _startRotation.M11;
            _curScale.Y = start.M22 - _startRotation.M22;
            _curScale.Z = start.M33 - _startRotation.M33;
            _nextScale.X = end.M11 - _endRotation.M11;
            _nextScale.Y = end.M22 - _endRotation.M22;
            _nextScale.Z = end.M33 - _endRotation.M33;
            Vector3.Lerp(ref _curScale, ref _nextScale, slerpAmount, out _lerpedScale);

            // Create the rotation matrix from the slerped quaternions
            Matrix.CreateFromQuaternion(ref _qResult, out _returnMatrix);

            // Set the translation
            _returnMatrix.M41 = _lerpedTrans.X;
            _returnMatrix.M42 = _lerpedTrans.Y;
            _returnMatrix.M43 = _lerpedTrans.Z;

            // And the lerped scale component
            _returnMatrix.M11 += _lerpedScale.X;
            _returnMatrix.M22 += _lerpedScale.Y;
            _returnMatrix.M33 += _lerpedScale.Z;
            return _returnMatrix;
        }

        /// <summary>
        /// Roughly decomposes two matrices and performs spherical linear interpolation
        /// </summary>
        /// <param name="start">Source matrix for interpolation</param>
        /// <param name="end">Destination matrix for interpolation</param>
        /// <param name="slerpAmount">Ratio of interpolation</param>
        /// <param name="result">Stores the result of hte interpolation.</param>
        public static void SlerpMatrix(ref Matrix start, ref Matrix end, float slerpAmount, out Matrix result)
        {
            if (start == end)
            {
                result = start;
                return;
            }
            // Get rotation components and interpolate (not completely accurate but I don't want 
            // to get into polar decomposition and this seems smooth enough)
            Quaternion.CreateFromRotationMatrix(ref start, out _qStart);
            Quaternion.CreateFromRotationMatrix(ref end, out _qEnd);
            Quaternion.Lerp(ref _qStart, ref _qEnd, slerpAmount, out _qResult);

            // Get final translation components
            _curTrans.X = start.M41;
            _curTrans.Y = start.M42;
            _curTrans.Z = start.M43;
            _nextTrans.X = end.M41;
            _nextTrans.Y = end.M42;
            _nextTrans.Z = end.M43;
            Vector3.Lerp(ref _curTrans, ref _nextTrans, slerpAmount, out _lerpedTrans);

            // Get final scale component
            Matrix.CreateFromQuaternion(ref _qStart, out _startRotation);
            Matrix.CreateFromQuaternion(ref _qEnd, out _endRotation);
            _curScale.X = start.M11 - _startRotation.M11;
            _curScale.Y = start.M22 - _startRotation.M22;
            _curScale.Z = start.M33 - _startRotation.M33;
            _nextScale.X = end.M11 - _endRotation.M11;
            _nextScale.Y = end.M22 - _endRotation.M22;
            _nextScale.Z = end.M33 - _endRotation.M33;
            Vector3.Lerp(ref _curScale, ref _nextScale, slerpAmount, out _lerpedScale);

            // Create the rotation matrix from the slerped quaternions
            Matrix.CreateFromQuaternion(ref _qResult, out result);

            // Set the translation
            result.M41 = _lerpedTrans.X;
            result.M42 = _lerpedTrans.Y;
            result.M43 = _lerpedTrans.Z;

            // Add the lerped scale component
            result.M11 += _lerpedScale.X;
            result.M22 += _lerpedScale.Y;
            result.M33 += _lerpedScale.Z;
        }


        /// <summary>
        /// Determines whether or not a ModelMeshPart is skinned.
        /// </summary>
        /// <param name="meshPart">The part to check.</param>
        /// <returns>True if the part is skinned.</returns>
        public static bool IsSkinned(ModelMeshPart meshPart)
        {
            VertexElement[] ves = meshPart.VertexBuffer.VertexDeclaration.GetVertexElements();
            foreach (VertexElement ve in ves)
            {
                //(BlendIndices with UsageIndex = 0) specifies matrix indices for fixed-function vertex 
                // processing using indexed paletted skinning.
                if (ve.VertexElementUsage == VertexElementUsage.BlendIndices
                    && ve.UsageIndex == 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Determines whether or not a ModelMesh is skinned.
        /// </summary>
        /// <param name="mesh">The mesh to check.</param>
        /// <returns>True if the mesh is skinned.</returns>
        public static bool IsSkinned(ModelMesh mesh)
        {
            foreach (ModelMeshPart mmp in mesh.MeshParts)
            {
                if (IsSkinned(mmp))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Determines whether or not a Model is skinned.
        /// </summary>
        /// <param name="model">The model to check.</param>
        /// <returns>True if the model is skinned.</returns>
        public static bool IsSkinned(Model model)
        {
            foreach (ModelMesh mm in model.Meshes)
            {
                if (IsSkinned(mm))
                    return true;
            }
            return false;
        }
    }
}
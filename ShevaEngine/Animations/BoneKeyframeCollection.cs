﻿using System.Collections.Generic;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// A collection of BoneKeyFrames that represents an animation track.
    /// </summary>
    public class BoneKeyframeCollection : List<BoneKeyframe>
    {
        /// <summary>Gets the duration of the animation track.</summary>
        public long Duration { get; private set; }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public BoneKeyframeCollection()
            : base()
        {

        }

        // Only allow creation from inside the library (only in AnimationReader)
        public BoneKeyframeCollection(string boneName,
            IList<BoneKeyframe> list)
            : base(list)
        {
            Duration = list[list.Count - 1].Time;
        }

        /// <summary>
        /// Gets the index in the track at the given time.
        /// </summary>
        /// <param name="ticks">The time for which the index is found.</param>
        /// <returns>The index in the track at the given time.</returns>
        public int GetIndexByTime(long ticks)
        {
            // Since the animation is usually interpolated to 60 fps, this will
            // almost always be the index to return
            int firstFrameIndexToCheck = (int)(ticks / Util.TicksPer60Fps);
            // Do out of bounds checking
            if (firstFrameIndexToCheck >= base.Count)
                firstFrameIndexToCheck = base.Count - 1;
            // Increment the index until the time at the next index is greater than the
            // specified time
            while (firstFrameIndexToCheck < base.Count - 1 && base[firstFrameIndexToCheck + 1].Time < ticks)
            {
                ++firstFrameIndexToCheck;
            }
            // Decrement the index till the time at the index is not greater than the
            // specified time
            while (firstFrameIndexToCheck >= 0 && base[firstFrameIndexToCheck].Time >
                ticks)
            {
                --firstFrameIndexToCheck;
            }

            return firstFrameIndexToCheck;
        }
    }
}

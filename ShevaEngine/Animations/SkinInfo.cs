using Microsoft.Xna.Framework;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// A structure that contains information for a bindpose skin offset.
    /// Represents the inverse bind pose for a bone.
    /// </summary>
    public struct SkinInfo
    {
        /// <summary>The name of the bone attached to the transform.</summary>
        public string BoneName { get; }
        /// <summary>The transform for the bone.</summary>
        public Matrix InverseBindPoseTransform { get; }
        /// <summary>The index in the MatrixPalette for the bone.</summary>
        public int PaletteIndex { get; }
        /// <summary>The index of the bone.</summary>
        public int BoneIndex { get; }

        /// <summary>
        /// Creates a new SkinInfo.
        /// </summary>
        /// <param name="name">The name of the bone attached to the transform.</param>
        /// <param name="inverseBindPoseTransform">The inverse bind pose transform for the bone.</param>
        /// <param name="paletteIndex">The index in the MatrixPalette for the bone.</param>
        /// <param name="boneIndex">The index of the bone.</param>
        public SkinInfo(string name, Matrix inverseBindPoseTransform, int paletteIndex, int boneIndex)
        {
            BoneName = name;
            InverseBindPoseTransform = inverseBindPoseTransform;
            PaletteIndex = paletteIndex;
            BoneIndex = boneIndex;
        }
    }
}
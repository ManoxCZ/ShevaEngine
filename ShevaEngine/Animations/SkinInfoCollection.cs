﻿using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// A collection of SkinInfo objects.
    /// </summary>
    public class SkinInfoCollection : List<SkinInfo>
    {
        /// <summary>
        /// Finds the skinning info for the model and calculates the inverse
        /// reference poses required for animation.
        /// </summary>
        /// <param name="model">The model that contains the skinning info.</param>
        /// <returns>A collection of SkinInfo objects.</returns>
        public static SkinInfoCollection FromModel(Model model)
        {
            // This is created in the content pipeline
            Dictionary<string, object> modelTagData = (Dictionary<string, object>)model.Tag;
            
            // An array of bone names that are used by the palette
            SkinInfoCollection[] info = (SkinInfoCollection[])modelTagData["SkinInfo"];

            return info[0];
        }
    }
}

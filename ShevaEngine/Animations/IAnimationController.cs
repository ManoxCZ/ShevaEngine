using System;
using Microsoft.Xna.Framework;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// An interface used by BonePose that allows an animation to affect the bone
    /// as a function of time.
    /// </summary>
    public interface IAnimationController
    {
        /// <summary>
        /// Gets the current transform for the given BonePose object in the animation.
        /// This is only called when a bone pose is affected by the current animation.
        /// </summary>
        /// <param name="pose">The BonePose object querying for the current transform in
        /// the animation.</param>
        /// <returns>The current transform of the bone.</returns>
        Matrix GetCurrentBoneTransform(BonePose pose);
        /// <summary>
        /// Gets a value determining whether the animation can potentially affect the
        /// given BonePose.
        /// </summary>
        /// <param name="pose">The BonePose to test.</param>
        /// <returns>True if the animation can affect the bone and contains a track
        /// for it.</returns>
        bool ContainsAnimationTrack(BonePose pose);

        /// <summary>
        /// Raised when the animation tracks have changed so that different bones are affect.
        /// </summary>
        event EventHandler AnimationTracksChanged;

        /// <summary>
        /// Metoda restartuje AnimationController.
        /// </summary>
        void Reset();

        /// <summary>
        /// Metoda Update().
        /// </summary>
        /// <param name="gameTime"></param>
        void Update(GameTime gameTime);
    }
}

using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace ShevaEngine.Animations
{
    /// <summary>
    /// Combines all the data needed to render and animate a skinned object.
    /// This is typically stored in the Tag property of the Model being animated.
    /// </summary>
    public class SkinningData
    {
        /// <summary>
        /// Gets a collection of animation clips. These are stored by name in a
        /// dictionary, so there could for instance be clips for "Walk", "Run",
        /// "JumpReallyHigh", etc.
        /// </summary>
        public Dictionary<string, AnimationClip> AnimationClips { get; }

        /// <summary> Bindpose matrices for each bone in the skeleton, relative to the parent bone. </summary>
        public List<Matrix> BindPose { get; private set; }

        /// <summary> Vertex to bonespace transforms for each bone in the skeleton.</summary>
        public List<Matrix> InverseBindPose { get; private set; }

        /// <summary> For each bone in the skeleton, stores the index of the parent bone.</summary>
        public List<int> SkeletonHierarchy { get; private set; }

        /// <summary> Dictionary mapping bone names to their indices in the preceding lists.</summary>
        public Dictionary<string, int> BoneIndices { get; private set; }


        /// <summary>
        /// Constructs a new skinning data object.
        /// </summary>
        public SkinningData(Dictionary<string, AnimationClip> animationClips,
            List<Matrix> bindPose, List<Matrix> inverseBindPose,
            List<int> skeletonHierarchy,
            Dictionary<string, int> boneIndices)
        {
            AnimationClips = animationClips;
            BindPose = bindPose;
            InverseBindPose = inverseBindPose;
            SkeletonHierarchy = skeletonHierarchy;
            BoneIndices = boneIndices;
        }
    }
}

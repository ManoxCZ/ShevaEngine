﻿using ImGuiNET;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ShevaEngine.Entities.Components;
using ShevaEngine.Input;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ShevaEngine.ImmediateGui
{
    /// <summary>
    /// ImGUI manager
    /// </summary>
    public class ImGuiManager
    {
        private readonly ShevaGame _game;
        private readonly List<Texture2D> _textures;
        private readonly RasterizerState _rasterizerState;
        private readonly SpriteEffect _spriteEffect;

        private int _fontTexture;
        private int _pressCount;
        private readonly IntPtr _textInputBuffer;
        private readonly int _textInputBufferLength;
        private float _wheelPosition;
        private float _sliderVal;
        private System.Numerics.Vector4 _buttonColor = new System.Numerics.Vector4(55f / 255f, 155f / 255f, 1f, 1f);
        private bool _mainWindowOpened;
        private readonly float _scaleFactor;
        private System.Numerics.Vector3 _positionValue = new System.Numerics.Vector3(500);
        

        /// <summary>
        /// Constructor.
        /// </summary>        
        public unsafe ImGuiManager(ShevaGame game)
        {
            _game = game;

            _textures = new List<Texture2D>();

            _rasterizerState = new RasterizerState
            {
                CullMode = CullMode.None,
                ScissorTestEnable = true
            };

            _spriteEffect = new SpriteEffect(_game.GraphicsSystem.GraphicsDevice);

            int desiredWidth = 800;

            _scaleFactor = (float)_game.GraphicsSystem.GraphicsDevice.PresentationParameters.BackBufferWidth / desiredWidth;

            ImGui.GetIO().FontAtlas.AddDefaultFont();

            // TODO: set up key mapping
//            gw.TextInput += OnKeyPress;

            _textInputBufferLength = 1024;
            _textInputBuffer = Marshal.AllocHGlobal(_textInputBufferLength);

            long* ptr = (long*)_textInputBuffer.ToPointer();

            for (int i = 0; i < 1024 / sizeof(long); i++)
                ptr[i] = 0;
            
            CreateDeviceObjects();
        }

        #region Keys

        private void OnKeyPress(object sender, TextInputEventArgs e)
        {
            Console.Write("Char typed: " + e.Character);
            ImGui.AddInputCharacter(e.Character);
        }

        private static void SetOpenTkKeyMappings()
        {
            IO io = ImGui.GetIO();
            io.KeyMap[GuiKey.Tab] = (int)Keys.Tab;
            io.KeyMap[GuiKey.LeftArrow] = (int)Keys.Left;
            io.KeyMap[GuiKey.RightArrow] = (int)Keys.Right;
            io.KeyMap[GuiKey.UpArrow] = (int)Keys.Up;
            io.KeyMap[GuiKey.DownArrow] = (int)Keys.Down;
            io.KeyMap[GuiKey.PageUp] = (int)Keys.PageUp;
            io.KeyMap[GuiKey.PageDown] = (int)Keys.PageDown;
            io.KeyMap[GuiKey.Home] = (int)Keys.Home;
            io.KeyMap[GuiKey.End] = (int)Keys.End;
            io.KeyMap[GuiKey.Delete] = (int)Keys.Delete;
            io.KeyMap[GuiKey.Backspace] = (int)Keys.Back;
            io.KeyMap[GuiKey.Enter] = (int)Keys.Enter;
            io.KeyMap[GuiKey.Escape] = (int)Keys.Escape;
            io.KeyMap[GuiKey.A] = (int)Keys.A;
            io.KeyMap[GuiKey.C] = (int)Keys.C;
            io.KeyMap[GuiKey.V] = (int)Keys.V;
            io.KeyMap[GuiKey.X] = (int)Keys.X;
            io.KeyMap[GuiKey.Y] = (int)Keys.Y;
            io.KeyMap[GuiKey.Z] = (int)Keys.Z;
        }

        /// <summary>
        /// On key down.
        /// </summary>
        private void OnKeyDown(ShevaKeyboard keyboard, Keys key)
        {
            ImGui.GetIO().KeysDown[(int)key] = true;

            UpdateModifiers(keyboard);
        }

        /// <summary>
        /// On key up.
        /// </summary>
        private void OnKeyUp(ShevaKeyboard keyboard, Keys key)
        {
            ImGui.GetIO().KeysDown[(int)key] = false;

            UpdateModifiers(keyboard);
        }

        /// <summary>
        /// Update modifiers.
        /// </summary>        
        private static void UpdateModifiers(ShevaKeyboard keyboard)
        {
            IO io = ImGui.GetIO();
            io.AltPressed = keyboard.IsKeyDown(Keys.LeftAlt) || keyboard.IsKeyDown(Keys.RightAlt);
            io.CtrlPressed = keyboard.IsKeyDown(Keys.LeftControl) || keyboard.IsKeyDown(Keys.RightControl);
            io.ShiftPressed = keyboard.IsKeyDown(Keys.LeftShift) || keyboard.IsKeyDown(Keys.RightShift);
        }

        #endregion

        private unsafe void CreateDeviceObjects()
        {
            IO io = ImGui.GetIO();

            // Build texture atlas
            FontTextureData texData = io.FontAtlas.GetTexDataAsRGBA32();
            var tex = new Texture2D(_game.GraphicsSystem.GraphicsDevice, texData.Width, texData.Height);
            if (texData.BytesPerPixel != 4)
                throw new Exception();
            var pixelCount = texData.Width * texData.Height;
            var pixelData = new Color[pixelCount];
            for (var i = 0; i < pixelCount; i++)
                pixelData[i] = new Color((int)texData.Pixels[4 * i], (int)texData.Pixels[4 * i + 1],
                    (int)texData.Pixels[4 * i + 2], (int)texData.Pixels[4 * i + 3]);
            tex.SetData(pixelData);
            _fontTexture = _textures.Count;
            _textures.Add(tex);
            io.FontAtlas.SetTexID(_fontTexture);
            // Store the texture identifier in the ImFontAtlas substructure.
            io.FontAtlas.SetTexID(_fontTexture);

            // Cleanup (don't clear the input data if you want to append new fonts later)
            //io.Fonts->ClearInputData();
            io.FontAtlas.ClearTexData();
        }

        /// <summary>
        /// Render frame.
        /// </summary>
        public unsafe void RenderFrame(Rectangle cameraViewport, IEnumerable<IOnGUIComponent> components)
        {
            IO io = ImGui.GetIO();
            io.DeltaTime = (float)1 / 60;

            UpdateImGuiInput(_game.Input, io);

            var pp = _game.GraphicsSystem.GraphicsDevice.PresentationParameters;
            io.DisplaySize = new System.Numerics.Vector2(pp.BackBufferWidth, pp.BackBufferHeight);
            io.DisplayFramebufferScale = new System.Numerics.Vector2(_scaleFactor);

            ImGui.NewFrame();

            BeginBaseWindow(cameraViewport);

            foreach (IOnGUIComponent entityComponent in components)
                entityComponent.OnGui();

            //SubmitImGuiStuff();

            EndBaseWindow();

            ImGui.Render();

            DrawData* data = ImGui.GetDrawData();
            RenderImDrawData(data);
        }

        /// <summary>
        /// Begin base window.
        /// </summary>
        private void BeginBaseWindow(Rectangle cameraViewport)
        {
            ImGui.GetStyle().WindowRounding = 0;

            ImGui.SetNextWindowSize(new System.Numerics.Vector2(cameraViewport.Width, cameraViewport.Height), SetCondition.Once);
            ImGui.SetNextWindowPos(new System.Numerics.Vector2(cameraViewport.X, cameraViewport.Y), SetCondition.Once);
            
            ImGui.BeginWindow("Base window", ref _mainWindowOpened, 0.0f, WindowFlags.NoTitleBar | WindowFlags.NoResize);
        }

        /// <summary>
        /// End base window.
        /// </summary>
        private void EndBaseWindow()
        {
            ImGui.EndWindow();
        }

        public unsafe void SubmitImGuiStuff()
        {
            

            ImGui.BeginMainMenuBar();
            if (ImGui.BeginMenu("Help"))
            {
                if (ImGui.MenuItem("About", "Ctrl-Alt-A", false, true))
                {
                }
                ImGui.EndMenu();
            }
            ImGui.EndMainMenuBar();

            ImGui.Text("Hello,");
            ImGui.Text("World!");
            ImGui.Text("From ImGui.NET. ...Did that work?");

            ImGui.BeginWindow("I'm a nested window!", ref _mainWindowOpened, WindowFlags.Default);

            System.Numerics.Vector2 pos = ImGui.GetIO().MousePosition;
            bool leftPressed = ImGui.GetIO().MouseDown[0];
            ImGui.Text("Current mouse position: " + pos + ". Pressed=" + leftPressed);

            if (ImGui.Button("Increment the counter."))
            {
                _pressCount += 1;
            }

            ImGui.Text($"Button pressed {_pressCount} times.", new System.Numerics.Vector4(0, 1, 1, 1));

            ImGui.InputTextMultiline("Input some text:",
                _textInputBuffer, (uint)_textInputBufferLength,
                new System.Numerics.Vector2(360, 240),
                InputTextFlags.Default,
                OnTextEdited);

            ImGui.SliderFloat("SlidableValue", ref _sliderVal, -50f, 100f, $"{_sliderVal:##0.00}", 1.0f);
            ImGui.DragVector3("Vector3", ref _positionValue, -100, 100);

            if (ImGui.TreeNode("First Item"))
            {
                ImGui.Text("Word!");
                ImGui.TreePop();
            }
            if (ImGui.TreeNode("Second Item"))
            {
                ImGui.ColorButton(_buttonColor, false, true);
                if (ImGui.Button("Push me to change color", new System.Numerics.Vector2(120, 30)))
                {
                    _buttonColor = new System.Numerics.Vector4(_buttonColor.Y + .25f, _buttonColor.Z, _buttonColor.X, _buttonColor.W);
                    if (_buttonColor.X > 1.0f)
                    {
                        _buttonColor.X -= 1.0f;
                    }
                }

                ImGui.TreePop();
            }

            if (ImGui.Button("Press me!", new System.Numerics.Vector2(100, 30)))
            {
                ImGuiNative.igOpenPopup("SmallButtonPopup");
            }

            if (ImGui.BeginPopup("SmallButtonPopup"))
            {
                ImGui.Text("Here's a popup menu.");
                ImGui.Text("With two lines.");

                ImGui.EndPopup();
            }

            if (ImGui.Button("Open Modal window"))
            {
                ImGui.OpenPopup("ModalPopup");
            }
            if (ImGui.BeginPopupModal("ModalPopup"))
            {
                ImGui.Text("You can't press on anything else right now.");
                ImGui.Text("You are stuck here.");
                if (ImGui.Button("OK", new System.Numerics.Vector2(0, 0))) { }
                ImGui.SameLine();
                ImGui.Dummy(100f, 0f);
                ImGui.SameLine();
                if (ImGui.Button("Please go away", new System.Numerics.Vector2(0, 0))) { ImGui.CloseCurrentPopup(); }

                ImGui.EndPopup();
            }

            ImGui.Text("I have a context menu.");
            if (ImGui.BeginPopupContextItem("ItemContextMenu"))
            {
                if (ImGui.Selectable("How's this for a great menu?")) { }
                ImGui.Selectable("Just click somewhere to get rid of me.");
                ImGui.EndPopup();
            }

            ImGui.EndWindow();            
        }

        private unsafe int OnTextEdited(TextEditCallbackData* data)
        {
            char currentEventChar = (char)data->EventChar;
            return 0;
        }

        /// <summary>
        /// Update ImGui input.
        /// </summary>
        private void UpdateImGuiInput(InputManager input, IO io)
        {
            UpdateModifiers(input.Keyboard);            

            Point windowPoint = input.Mouse.Position;
            io.MousePosition = new System.Numerics.Vector2(windowPoint.X / io.DisplayFramebufferScale.X, windowPoint.Y / io.DisplayFramebufferScale.Y);

            io.MouseDown[0] = input.Mouse.LeftButton == ButtonState.Pressed;
            io.MouseDown[1] = input.Mouse.RightButton == ButtonState.Pressed;
            io.MouseDown[2] = input.Mouse.MiddleButton == ButtonState.Pressed;

            float newWheelPos = input.Mouse.WheelDelta;

            float delta = newWheelPos - _wheelPosition;
            _wheelPosition = newWheelPos;
            io.MouseWheel = delta;
        }

        /// <summary>
        /// Render ImGui draw data.
        /// </summary>
        private unsafe void RenderImDrawData(DrawData* drawData)
        {
            if (drawData == null)
            {
                Console.WriteLine("No frame rendered for ImGUI.NET, but draw was called.");
                return;
            }

            PresentationParameters pp = _game.GraphicsSystem.GraphicsDevice.PresentationParameters;
            // Rendering
            int displayW = pp.BackBufferWidth;
            int displayH = pp.BackBufferHeight;

            Vector4 clearColor = new Vector4(114f / 255f, 144f / 255f, 154f / 255f, 1.0f);
            _game.GraphicsSystem.GraphicsDevice.Viewport = new Viewport(0, 0, displayW, displayH);
            //_game.GraphicsSystem.GraphicsDevice.Clear(new Color(clearColor.X, clearColor.Y, clearColor.Z, clearColor.W));

            // We are using the OpenGL fixed pipeline to make the example code simpler to read!
            // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled, vertex/texcoord/color pointers.
            _game.GraphicsSystem.GraphicsDevice.BlendState = BlendState.NonPremultiplied;
            _game.GraphicsSystem.GraphicsDevice.RasterizerState = _rasterizerState;
            _game.GraphicsSystem.GraphicsDevice.DepthStencilState = DepthStencilState.None;

            // Handle cases of screen coordinates != from framebuffer coordinates (e.g. retina displays)
            IO io = ImGui.GetIO();
            ImGui.ScaleClipRects(drawData, io.DisplayFramebufferScale);

            // Render command lists

            for (int n = 0; n < drawData->CmdListsCount; n++)
            {
                NativeDrawList* cmdList = drawData->CmdLists[n];
                DrawVert* vtxBuffer = (DrawVert*)cmdList->VtxBuffer.Data;
                ushort* idxBuffer = (ushort*)cmdList->IdxBuffer.Data;

                var vertices = new VertexPositionColorTexture[cmdList->VtxBuffer.Size];
                for (int i = 0; i < vertices.Length; i++)
                {
                    vertices[i] = new VertexPositionColorTexture(
                        new Vector3(vtxBuffer[i].pos.X, vtxBuffer[i].pos.Y, 0),
                        new Color(vtxBuffer[i].col),
                        new Vector2(vtxBuffer[i].uv.X, vtxBuffer[i].uv.Y));
                }

                _spriteEffect.CurrentTechnique.Passes[0].Apply();

                for (int cmdI = 0; cmdI < cmdList->CmdBuffer.Size; cmdI++)
                {
                    DrawCmd* pcmd = &(((DrawCmd*)cmdList->CmdBuffer.Data)[cmdI]);
                    if (pcmd->UserCallback != IntPtr.Zero)
                    {
                        throw new NotImplementedException();
                    }
                    else
                    {
                        _game.GraphicsSystem.GraphicsDevice.Textures[0] = _textures[pcmd->TextureId.ToInt32()];
                        _game.GraphicsSystem.GraphicsDevice.ScissorRectangle = new Rectangle(
                            (int)pcmd->ClipRect.X,
                            (int)pcmd->ClipRect.Y,
                            (int)(pcmd->ClipRect.Z - pcmd->ClipRect.X),
                            (int)(pcmd->ClipRect.W - pcmd->ClipRect.Y));
                        short[] indices = new short[pcmd->ElemCount];
                        for (int i = 0; i < indices.Length; i++) { indices[i] = (short)idxBuffer[i]; }
                        _game.GraphicsSystem.GraphicsDevice.DrawUserIndexedPrimitives(
                            PrimitiveType.TriangleList, vertices, 0,
                            vertices.Length, indices, 0, (int)pcmd->ElemCount / 3);
                    }
                    idxBuffer += pcmd->ElemCount;
                }
            }
        }
    }
}

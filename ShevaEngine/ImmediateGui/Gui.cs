﻿using ImGuiNET;
using Microsoft.Xna.Framework;

namespace ShevaEngine.ImmediateGui
{
    /// <summary>
    /// Gui.
    /// </summary>
    public class Gui
    {

        public static bool BeginMainMenuBar()
        {
            return ImGui.BeginMainMenuBar();
        }

        public static bool BeginMenu(string label)
        {
            return ImGui.BeginMenu(label);
        }

        public static bool BeginWindow(string windowTitle) => BeginWindow(windowTitle, GuiWindowFlags.Default);

        public static bool BeginWindow(string windowTitle, GuiWindowFlags flags)
        {
            bool opened = true;
            return ImGui.BeginWindow(windowTitle, ref opened, (WindowFlags)flags);
        }

        public static bool BeginWindow(string windowTitle, ref bool opened, GuiWindowFlags flags)
        {
            return ImGui.BeginWindow(windowTitle, ref opened, (WindowFlags)flags);
        }

        public static bool BeginWindow(string windowTitle, ref bool opened, float backgroundAlpha, GuiWindowFlags flags)
        {
            return ImGui.BeginWindow(windowTitle, ref opened, new System.Numerics.Vector2(), backgroundAlpha, (WindowFlags)flags);
        }

        public static bool BeginWindow(string windowTitle, ref bool opened, Vector2 startingSize, GuiWindowFlags flags)
        {
            return ImGui.BeginWindow(windowTitle, ref opened, new System.Numerics.Vector2(startingSize.X, startingSize.Y), 1f, (WindowFlags)flags);
        }

        public static bool BeginWindow(string windowTitle, ref bool opened, Vector2 startingSize, float backgroundAlpha, GuiWindowFlags flags)
        {
            return ImGui.BeginWindow(windowTitle, ref opened, new System.Numerics.Vector2(startingSize.X, startingSize.Y), backgroundAlpha, (WindowFlags)flags);
        }

        public static bool Button(string message)
        {
            return ImGui.Button(message, System.Numerics.Vector2.Zero);
        }

        public static bool Button(string message, Vector2 size)
        {
            return ImGui.Button(message, new System.Numerics.Vector2(size.X, size.Y));
        }

        public static void EndMainMenuBar()
        {
            ImGui.EndMainMenuBar();
        }

        public static void EndMenu()
        {
            ImGui.EndMenu();
        }

        public static bool MenuItem(string label)
        {
            return ImGui.MenuItem(label, string.Empty, false, true);
        }

        public static bool MenuItem(string label, string shortcut)
        {
            return ImGui.MenuItem(label, shortcut, false, true);
        }

        public static bool MenuItem(string label, bool enabled)
        {
            return ImGui.MenuItem(label, string.Empty, false, enabled);
        }

        public static bool MenuItem(string label, string shortcut, bool selected, bool enabled)
        {
            return ImGui.MenuItem(label, shortcut, selected, enabled);
        }

        public static void Text(string message)
        {
            ImGui.Text(message);
        }

        public static void Text(string message, Vector4 color)
        {
            ImGui.Text(message, new System.Numerics.Vector4(color.X, color.Y, color.Z, color.W));
        }
    }
}

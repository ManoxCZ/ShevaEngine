﻿using Newtonsoft.Json.Serialization;
using ShevaEngine.Assets;
using System;
using System.Collections.Generic;

namespace ShevaEngine.Serialization
{
    /// <summary>
    /// Id reference resolver.
    /// </summary>
    internal class AssetReferenceResolver : IReferenceResolver
    {
        private readonly ShevaGame _game;
        private readonly IDictionary<Guid, Asset> _assets = new SortedDictionary<Guid, Asset>();


        /// <summary>
        /// Constructor.
        /// </summary>
        public AssetReferenceResolver(ShevaGame game)
        {
            _game = game;
        }

        /// <summary>
        /// Resolve reference.
        /// </summary>
        public object ResolveReference(object context, string reference)
        {
            Guid id = Guid.Parse(reference);

            return _assets.ContainsKey(id) ? _assets[id] : null;
        }

        /// <summary>
        /// Get reference.
        /// </summary>
        public string GetReference(object context, object value)
        {
            Asset asset = value as Asset;

            return asset != null ? asset.Id.ToString() : string.Empty;
        }

        /// <summary>
        /// Is referenced.
        /// </summary>
        public bool IsReferenced(object context, object value)
        {
            Asset asset = value as Asset;

            if (asset != null)
            {
                if (!_assets.ContainsKey(asset.Id))
                {
                    _assets.Add(asset.Id, asset);

                    return false;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Add reference.
        /// </summary>
        public void AddReference(object context, string reference, object value)
        {
            Asset asset = value as Asset;            

            if (string.IsNullOrEmpty(reference) || asset == null)
                return;

            Guid id = Guid.Parse(reference);

            _assets.Add(id, asset);
        }
    }
}

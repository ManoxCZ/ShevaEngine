﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;
using Newtonsoft.Json;
using ShevaEngine.Assets;
using ShevaEngine.Entities;
using ShevaEngine.Entities.Components;

namespace ShevaEngine.Serialization
{
    /// <summary>
    /// Sheva serializer.
    /// </summary>
    public class ShevaSerializer
    {
        /// <summary>
        /// Serialize method.
        /// </summary>
        public static void Serialize<T>(T data, string filename)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                ReferenceResolverProvider = () => new AssetReferenceResolver(null),
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                TypeNameHandling = TypeNameHandling.Auto                
                //Converters = new List<JsonConverter>(new JsonConverter[] { new EntityComponentSerializer() }),
            };


            using (StreamWriter writer = new StreamWriter(filename))
            {
                writer.Write(JsonConvert.SerializeObject(data, settings));
            }
        }


        /// <summary>
        /// Deserialize method.
        /// </summary>
        public static T Deserialize<T>(ShevaGame game, string filename) where T : new()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                ReferenceResolverProvider = () => new AssetReferenceResolver(game),
                ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                TypeNameHandling = TypeNameHandling.Auto
                //Converters = new List<JsonConverter>(new JsonConverter[] { new EntityComponentSerializer() }),
            };


            using (StreamReader reader = new StreamReader(filename))
            {
                return (T)JsonConvert.DeserializeObject<T>(reader.ReadToEnd(), settings);
            }

            return default(T);
        }
    }
}

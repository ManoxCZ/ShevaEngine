﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Entities.Components;
using ShevaEngine.Renderers.RenderingPipelines;
using System;
using System.Collections.Generic;
using ShevaEngine.Graphics.Shaders;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// The G Buffer.
    /// </summary>
    internal class GBuffer
    {
        private readonly GraphicsDevice _device;
        private readonly SortedDictionary<Guid, GBufferRenderTargets> _cameraRenderTargets;
        private readonly Shader _shader;        
        ///// <summary>Last draw call.</summary>
        //private RenderingPackage LastRenderingPair;
        ///// <summary>Initial draw call.</summary>
        //private RenderingPackage InitialRenderingPair;
        ///// <summary>Animated.</summary>
        //private Boolean Animated;
        ///// <summary>Matrices.</summary>
        //private List<Matrix> WorldMatrices;
        ///// <summary>Matrices.</summary>
        //private List<Matrix> WorldViewMatrices;
        ///// <summary>DrawCall key.</summary>
        //private DrawCallKey DrawCallKey;
        ///// <summary>DrawCall key.</summary>
        //private DrawCallKey LastDrawCallKey;

        /// <summary>
        /// This shader generates a depth and normal map.
        /// It also generates a special buffer with motion vectors for motion blur and the specular power of the material.
        /// It stores the result in two textures, the normals (normalMapTexture) and the depth (depthMapTexture).
        /// The depth texture has a texture with a 32 bits single channel precision, and the normal has a half vector 2 format (r16f g16f). 
        /// The normals are store with spherical coordinates and the depth is store using the equation: -DepthVS / FarPlane.
        /// </summary>
        internal GBuffer(ShevaGame game)
        {
            _device = game.GraphicsSystem.GraphicsDevice;
            _cameraRenderTargets = new SortedDictionary<Guid, GBufferRenderTargets>();

            _shader = game.AssetsManager.Get<Shader>(@"Assets\Shaders\GBuffer\GBuffer.shader");
            
            //InitialRenderingPair = new RenderingPackage();
            //InitialRenderingPair.DrawCall = DrawCallManager.Instance.CreateIndexedPrimitivesDrawCall(
            //    PrimitiveType.LineList, 0, 0, 0, 0, 0);
            //InitialRenderingPair.DrawCallKey = new DrawCallKey(UInt16.MaxValue, UInt16.MaxValue, UInt16.MaxValue, UInt16.MaxValue);

            //WorldMatrices = new List<Matrix>();
            //WorldViewMatrices = new List<Matrix>();
        }

        /// <summary>
        /// Render the object without taking care of the illumination information.
        /// </summary>
        //private void RenderObjects(RenderingPipeline pipeline)
        //{
        //    Animated = false;

        //    Effect.Parameters["FarPlane"].SetValue(Viewport.Camera.FarPlane);

        //    LastRenderingPair = InitialRenderingPair;
        //    LastDrawCallKey = InitialRenderingPair.DrawCallKey;

        //    WorldMatrices.Clear();
        //    WorldViewMatrices.Clear();

        //    for (int i = 0; i < pipeline.ActualCount; i++)
        //    {
        //        if (pipeline.DrawCalls[i].DrawCallKey != LastRenderingPair.DrawCallKey ||
        //            pipeline.DrawCalls[i].DrawCall != LastRenderingPair.DrawCall)
        //        {
        //            if (WorldMatrices.Count > 0)
        //                DrawCall.RenderDrawCalls(Effect, LastRenderingPair.DrawCall,
        //                    WorldMatrices.ToArray(), WorldViewMatrices.ToArray());

        //            WorldMatrices.Clear();
        //            WorldViewMatrices.Clear();

        //            DrawCallKey = pipeline.DrawCalls[i].DrawCallKey;

        //            if ((DrawCallKey.MaterialID != LastDrawCallKey.MaterialID ||
        //                DrawCallKey.MaterialParamsID != LastDrawCallKey.MaterialParamsID) &&
        //                DrawCallKey.MaterialParamsID != DrawCallKey.None)
        //            {
        //                GraphicsMaterial material = MaterialManager.Instance[DrawCallKey.MaterialID].GraphicsProfile;

        //                if (material.ParametersManager[DrawCallKey.MaterialParamsID].IsAnimated)
        //                {
        //                    material.ParametersManager[DrawCallKey.MaterialParamsID].AnimatedParameters.Set(Effect);

        //                    Animated = true;
        //                }
        //                else
        //                    Animated = false;
        //            }

        //            if (!MaterialManager.Instance[DrawCallKey.MaterialID].GraphicsProfile.LightingEnabled)
        //                continue;

        //            if (DrawCallKey.TextureState != DrawCallKey.None)
        //            {
        //                if (DrawCallKey.TextureState != LastDrawCallKey.TextureState)
        //                {
        //                    GraphicsMaterial material = MaterialManager.Instance[DrawCallKey.MaterialID].GraphicsProfile;
        //                    TextureState textureState = TextureManager.Instance[DrawCallKey.TextureState];

        //                    Effect.Parameters["SpecularPower"].SetValue(material.SpecularPower);

        //                    if (textureState.ContainsKey("Specular"))
        //                    {
        //                        Effect.Parameters["SpecularMap"].SetValue(textureState["Specular"]);
        //                        Effect.Parameters["SpecularTextured"].SetValue(true);
        //                    }
        //                    else
        //                        Effect.Parameters["SpecularTextured"].SetValue(false);

        //                    if (textureState.ContainsKey("Normal"))
        //                    {
        //                        Effect.Parameters["NormalMap"].SetValue(textureState["Normal"]);
        //                        Effect.Parameters["DiffuseMap"].SetValue(textureState["Diffuse"]);

        //                        if (!Animated)
        //                            Effect.CurrentTechnique = Effect.Techniques["GBufferWithNormalMap"];
        //                        else
        //                            Effect.CurrentTechnique = Effect.Techniques["GBufferWithNormalMapAnimated"];
        //                    }
        //                    else
        //                        if (textureState.ContainsKey("Diffuse"))
        //                    {
        //                        Effect.Parameters["DiffuseMap"].SetValue(textureState["Diffuse"]);

        //                        if (!Animated)
        //                            Effect.CurrentTechnique = Effect.Techniques["GBufferWithDiffuseMap"];
        //                        else
        //                            Effect.CurrentTechnique = Effect.Techniques["GBufferWithDiffuseMapAnimated"];
        //                    }
        //                    else
        //                            if (!Animated)
        //                        Effect.CurrentTechnique = Effect.Techniques["GBufferWithoutTexture"];
        //                    else
        //                        Effect.CurrentTechnique = Effect.Techniques["GBufferWithoutTextureAnimated"];

        //                    Effect.CurrentTechnique.Passes[0].Apply();
        //                }
        //            }

        //            if (DrawCallKey.BuffersID != DrawCallKey.None)
        //                GraphicsBuffersManager.Instance.SetBuffers(DrawCallKey.BuffersID);

        //            Effect.Parameters["View"].SetValue(Viewport.Camera.ViewMatrix);
        //            Effect.Parameters["Projection"].SetValue(Viewport.Camera.ProjectionMatrix);
        //            Effect.CurrentTechnique.Passes[0].Apply();

        //            WorldMatrices.Add(pipeline.DrawCalls[i].Matrix);
        //            WorldViewMatrices.Add(Matrix.Transpose(Matrix.Invert(
        //                pipeline.DrawCalls[i].Matrix * Viewport.Camera.ViewMatrix)));

        //            LastDrawCallKey = DrawCallKey;
        //            LastRenderingPair = pipeline.DrawCalls[i];
        //        }
        //        else
        //        {
        //            WorldMatrices.Add(pipeline.DrawCalls[i].Matrix);
        //            WorldViewMatrices.Add(Matrix.Transpose(Matrix.Invert(
        //                pipeline.DrawCalls[i].Matrix * Viewport.Camera.ViewMatrix)));
        //        }
        //    }

        //    if (WorldMatrices.Count > 0)
        //        DrawCall.RenderDrawCalls(Effect, LastRenderingPair.DrawCall,
        //            WorldMatrices.ToArray(), WorldViewMatrices.ToArray());
        //}

        /// <summary>
        /// It generates the depth map, normal map and motion vector/specular power map of the objects given.
        /// </summary>
        internal GBufferRenderTargets GenerateGBuffer(CameraComponent cameraComponent, Rectangle viewport, IRenderingPipeline pipeline)
        {
            _device.BlendState = BlendState.Opaque;

            _device.RasterizerState = RasterizerState.CullNone;

            DepthStencilState depthStencilState = new DepthStencilState
            {
                DepthBufferEnable = true,
                DepthBufferFunction = CompareFunction.LessEqual,
                DepthBufferWriteEnable = true
            };

            _device.DepthStencilState = depthStencilState;

            if (!_cameraRenderTargets.ContainsKey(cameraComponent.Id))
                _cameraRenderTargets.Add(cameraComponent.Id, new GBufferRenderTargets());

            GBufferRenderTargets renderTargets = _cameraRenderTargets[cameraComponent.Id];

            renderTargets.Update(_device, viewport);

            // With multiple render targets the performance can be improved.
            _device.SetRenderTargets(
                renderTargets.Depth,
                renderTargets.Normal,
                renderTargets.MotionVectorsSpecularPowerTexture);

            _device.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.White, 1.0f, 0);
            
            //RenderObjects(renderingPipeline);

            _device.SetRenderTarget(null);

            _device.BlendState = BlendState.AlphaBlend;
            _device.DepthStencilState = DepthStencilState.Default;
            _device.RasterizerState = RasterizerState.CullNone;

            return renderTargets;
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Graphics.Shaders;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// Über post processing effect.
    /// </summary>
    public class PostProcessing
    {
        private readonly GraphicsDevice _device;
        private readonly Shader _shader;
        private readonly Effect _effect;
        private readonly ScreenPlane _screenPlane;

        /// <summary>
        /// Constructor.
        /// </summary>
        public PostProcessing(ShevaGame game)
        {
            _device = game.GraphicsSystem.GraphicsDevice;

            _shader = game.AssetsManager.Get<Shader>(@"Assets\Shaders\PostProcessSimple.shader");
            _effect = game.GraphicsSystem.ShadersManager[_shader];

            _effect.Parameters["World"].SetValue(Matrix.Identity);
            _effect.Parameters["View"].SetValue(Matrix.Identity);
            _effect.Parameters["Proj"].SetValue(Matrix.Identity);

            _screenPlane = new ScreenPlane(_device);
        }

        /// <summary>
        /// Render.
        /// </summary>
        public void Render(RenderTarget2D sceneTexture)
        {
            _device.BlendState = BlendState.Opaque;
            _device.DepthStencilState = DepthStencilState.None;

            _effect.Parameters["Diffuse"].SetValue(sceneTexture);

            _effect.CurrentTechnique.Passes[0].Apply();

            _screenPlane.Render();
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Entities.Components;
using ShevaEngine.Graphics;
using ShevaEngine.Graphics.Shaders;
using ShevaEngine.Renderers.Shadows;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// Light Pre Pass Point Light.
    /// This class could be mixed with the directionalLight class or could be coded with the partial class command.
    /// </summary>
    internal class LightPrePass
    {
        private readonly GraphicsSystem _system;
        private readonly Shader _shader;
        private readonly Effect _effect;
        private readonly Sphere _sphere;
    

        /// <summary>
        /// Load shader
        /// </summary>
        internal LightPrePass(ShevaGame game)
        {
            _system = game.GraphicsSystem;

            _shader = game.AssetsManager.Get<Shader>(@"Assets\Shaders\LightPrePass\PointLight.shader");
            _effect = _system.ShadersManager[_shader];
            
            _sphere = new Sphere(_system.GraphicsDevice, 50, 50, 1);
        }

        /// <summary>
        /// Render to the light pre pass texture.
        /// </summary>
        public void RenderNoneSM(LightComponent light, CameraComponent camera,
            RenderTarget2D depthTexture, RenderTarget2D normalTexture, RenderTarget2D motionVectorSpecularPowerTexture,
            RenderTarget2D lightPrePassMap)
        {
              Vector3 lightPosition = light.GetComponent<TransformComponent>().WorldMatrix.Translation;

            _effect.Parameters["World"].SetValue(Matrix.CreateScale(light.Radius) * Matrix.CreateTranslation(lightPosition));
            _effect.Parameters["View"].SetValue(camera.ViewMatrix);
            _effect.Parameters["ViewI"].SetValue(Matrix.Invert(camera.ViewMatrix));
            _effect.Parameters["Projection"].SetValue(camera.ProjectionMatrix);

            _effect.Parameters["halfPixel"].SetValue(new Vector2(0.5f / depthTexture.Width, 0.5f / depthTexture.Height));
            _effect.Parameters["lightColor"].SetValue(light.Color.ToVector3());
            _effect.Parameters["lightPosition"].SetValue(Vector3.Transform(lightPosition, camera.ViewMatrix));
            _effect.Parameters["lightIntensity"].SetValue(light.Intensity);
            _effect.Parameters["lightRadius"].SetValue(light.Radius);
            _effect.Parameters["depthTexture"].SetValue(depthTexture);
            _effect.Parameters["normalTexture"].SetValue(normalTexture);
            _effect.Parameters["motionVectorSpecularPowerTexture"].SetValue(motionVectorSpecularPowerTexture);
            _effect.Parameters["farPlane"].SetValue(camera.FarPlane);

            _effect.Parameters["lightView"].SetValue(Matrix.CreateLookAt(
                lightPosition, lightPosition - new Vector3(0, 1, 0), Vector3.UnitZ));
            _effect.Parameters["Camera"].SetValue(Matrix.Invert(camera.ViewMatrix));

            float cameraToCenter = Vector3.Distance(camera.GetComponent<TransformComponent>().WorldMatrix.Translation, lightPosition) - camera.NearPlane;

            _effect.Parameters["insideBoundingLightObject"].SetValue(cameraToCenter < light.Radius);

            _system.GraphicsDevice.RasterizerState = cameraToCenter < light.Radius ? RasterizerState.CullClockwise : RasterizerState.CullCounterClockwise;

            _effect.CurrentTechnique = _effect.Techniques["NoneSM"];
            _effect.CurrentTechnique.Passes[0].Apply();

            _sphere.Render();
        }

        /// <summary>
        /// Render to the light pre pass texture.
        /// </summary>
        public void RenderDualParaboloidSM(LightComponent light, ShadowMapDualParaboloid shadowMap, CameraComponent camera,
            RenderTarget2D depthTexture, RenderTarget2D normalTexture, RenderTarget2D motionVectorSpecularPowerTexture,
            RenderTarget2D lightPrePassMap)
        {
            Vector3 lightPosition = light.GetComponent<TransformComponent>().WorldMatrix.Translation;

            _effect.Parameters["World"].SetValue(Matrix.CreateScale(light.Radius) * Matrix.CreateTranslation(lightPosition));
            _effect.Parameters["View"].SetValue(camera.ViewMatrix);
            _effect.Parameters["ViewI"].SetValue(Matrix.Invert(camera.ViewMatrix));
            _effect.Parameters["Projection"].SetValue(camera.ProjectionMatrix);

            _effect.Parameters["halfPixel"].SetValue(new Vector2(0.5f / depthTexture.Width, 0.5f / depthTexture.Height));
            _effect.Parameters["lightColor"].SetValue(light.Color.ToVector3());
            _effect.Parameters["lightPosition"].SetValue(Vector3.Transform(lightPosition, camera.ViewMatrix));
            _effect.Parameters["lightIntensity"].SetValue(light.Intensity);
            _effect.Parameters["lightRadius"].SetValue(light.Radius);
            _effect.Parameters["depthTexture"].SetValue(depthTexture);
            _effect.Parameters["normalTexture"].SetValue(normalTexture);
            _effect.Parameters["motionVectorSpecularPowerTexture"].SetValue(motionVectorSpecularPowerTexture);
            _effect.Parameters["farPlane"].SetValue(camera.FarPlane);

            _effect.Parameters["shadowMapTop"].SetValue(shadowMap.ShadowMapTop);
            _effect.Parameters["shadowMapBottom"].SetValue(shadowMap.ShadowMapBottom);

            _effect.Parameters["lightView"].SetValue(Matrix.CreateLookAt(
                lightPosition, lightPosition - new Vector3(0, 1, 0), Vector3.UnitZ));
            _effect.Parameters["Camera"].SetValue(Matrix.Invert(camera.ViewMatrix));

            float cameraToCenter = Vector3.Distance(camera.GetComponent<TransformComponent>().WorldMatrix.Translation, lightPosition) - camera.NearPlane;

            _effect.Parameters["insideBoundingLightObject"].SetValue(cameraToCenter < light.Radius);

            if (cameraToCenter < light.Radius)
                _system.GraphicsDevice.RasterizerState = RasterizerState.CullClockwise;
            else
                _system.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

            _effect.CurrentTechnique = _effect.Techniques["DualParaboloidSM"];
            _effect.CurrentTechnique.Passes[0].Apply();

            _sphere.Render();
        }
    }
}

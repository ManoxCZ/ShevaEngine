﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// Sphere object.
    /// </summary>
    public class Sphere
    {
        private readonly GraphicsDevice _device;
        private readonly VertexBuffer _vertexBuffer;
        private readonly IndexBuffer _indexBuffer;
        private readonly int _numberIndices;


        /// <summary>
        /// Creates a sphere model.
        /// </summary>
        /// <param name="device"></param>
        /// <param name="stacks">Stacks</param>
        /// <param name="slices">Slices</param>
        /// <param name="radius">Radius</param>
        public Sphere(GraphicsDevice device, int stacks, int slices, float radius)
        {
            _device = device;

            // Calculates the resulting number of vertices and indices
            int numberVertices = (stacks + 1) * (slices + 1);
            _numberIndices = (3 * stacks * (slices + 1)) * 2;
            
            int[] indices = new int[_numberIndices];
            VertexPositionNormalTexture[] vertices = new VertexPositionNormalTexture[numberVertices];

            float stackAngle = MathHelper.Pi / stacks;
            float sliceAngle = (float)(Math.PI * 2.0) / slices;

            // Generate the group of Stacks for the sphere  
            int wVertexIndex = 0;
            int vertexCount = 0;
            int indexCount = 0;

            for (int stack = 0; stack < (stacks + 1); stack++)
            {

                float r = (float)Math.Sin(stack * stackAngle);
                float y = (float)Math.Cos(stack * stackAngle);

                // Generate the group of segments for the current Stack  
                for (int slice = 0; slice < (slices + 1); slice++)
                {
                    float x = r * (float)Math.Sin(slice * sliceAngle);
                    float z = r * (float)Math.Cos(slice * sliceAngle);
                    vertices[vertexCount].Position = new Vector3(x * radius, y * radius, z * radius);

                    vertices[vertexCount].Normal = Vector3.Normalize(new Vector3(x, y, z));

                    vertices[vertexCount].TextureCoordinate = new Vector2(slice / (float)slices, stack / (float)stacks);
                    vertexCount++;

                    if (stack != (stacks - 1))
                    {
                        // First Face
                        indices[indexCount++] = wVertexIndex + (slices + 1);

                        indices[indexCount++] = wVertexIndex;

                        indices[indexCount++] = wVertexIndex + 1;

                        // Second Face
                        indices[indexCount++] = wVertexIndex + (slices);

                        indices[indexCount++] = wVertexIndex;

                        indices[indexCount++] = wVertexIndex + (slices + 1);

                        wVertexIndex++;
                    }
                }
            }
            
            _vertexBuffer = new VertexBuffer(_device, typeof(VertexPositionNormalTexture), numberVertices, BufferUsage.None);
            _vertexBuffer.SetData(vertices, 0, vertices.Length);

            _indexBuffer = new IndexBuffer(_device, typeof(int), _numberIndices, BufferUsage.None);
            _indexBuffer.SetData(indices, 0, indices.Length);
        }

        /// <summary>
        /// Render a screen plane.
        /// </summary>
        public void Render()
        {
            _device.SetVertexBuffer(_vertexBuffer);
            _device.Indices = _indexBuffer;

            _device.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, _numberIndices / 3);
        }
    }
}

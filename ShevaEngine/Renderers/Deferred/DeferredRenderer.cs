﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Entities.Components;
using ShevaEngine.Renderers.RenderingPipelines;
using ShevaEngine.Renderers.Shadows;
using ShevaEngine.Scenes;
using System;
using System.Collections.Generic;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// Deferred renderer.
    /// </summary>
    public class DeferredRenderer : Renderer
    {
        private readonly SortedDictionary<Guid, DeferredRendererTargets> _cameraRenderTargets;

        /// <summary>Additive blending state. The resulting color will be added to current render target color.</summary>
        private readonly BlendState _additiveBlendingState;

        private readonly AmbientLight _ambientLight;
        private readonly LightPrePass _lightPrePass;
        private readonly PostProcessing _postProcessing;
        private readonly GBuffer _gBuffer;


        /// <summary>
        /// Deferred renderer constructor.
        /// </summary>
        public DeferredRenderer(ShevaGame game)
            : base(game)
        {
            _ambientLight = new AmbientLight(game);
            _lightPrePass = new LightPrePass(game);
            _cameraRenderTargets = new SortedDictionary<Guid, DeferredRendererTargets>();
            _gBuffer = new GBuffer(game);

            _additiveBlendingState = new BlendState
            {
                AlphaBlendFunction = BlendFunction.Add,
                AlphaDestinationBlend = Blend.One,
                AlphaSourceBlend = Blend.One,
                ColorBlendFunction = BlendFunction.Add,
                ColorDestinationBlend = Blend.One,
                ColorSourceBlend = Blend.One,
            };

            _postProcessing = new PostProcessing(game);
        }

        /// <summary>
        /// Render method.
        /// </summary>
        public override void RenderScene(CameraComponent cameraComponent, Scene scene)
        {
            IRenderingPipeline pipeline = new DeferredRenderingPipeline();

            scene.UpdateRenderingPipeline(pipeline, cameraComponent);

            Rectangle viewportRectangle = GetCameraViewport(cameraComponent);

            DeferredRendererTargets renderTargets = GetTargetsForCamera(cameraComponent, viewportRectangle);

            GBufferRenderTargets gBufferTargets =
                _gBuffer.GenerateGBuffer(cameraComponent, viewportRectangle, pipeline);

            UpdateLightMap(cameraComponent, scene, renderTargets, gBufferTargets);

            UpdateSceneMap(cameraComponent, renderTargets, pipeline);

            //UpdatePostProcesses(renderTargets);

            UpdateFinalTarget(cameraComponent, viewportRectangle, renderTargets);
        }

        /// <summary>
        /// Get targets for camera.
        /// </summary>
        private DeferredRendererTargets GetTargetsForCamera(CameraComponent cameraComponent, Rectangle cameraViewport)
        {
            if (!_cameraRenderTargets.ContainsKey(cameraComponent.Id))
                _cameraRenderTargets.Add(cameraComponent.Id, new DeferredRendererTargets());

            DeferredRendererTargets renderTargets = _cameraRenderTargets[cameraComponent.Id];

            renderTargets.Update(Device, cameraViewport);

            return renderTargets;
        }

        /// <summary>
        /// Update Light Map. This is the light pre pass.
        /// </summary>
        private void UpdateLightMap(CameraComponent cameraComponent, Scene scene, DeferredRendererTargets renderTargets,
            GBufferRenderTargets gBufferTargets)
        {
            Device.SetRenderTarget(renderTargets.LightMap);

            if (!LightingEnabled)
            {
                Device.Clear(new Color(1.0f, 1.0f, 1.0f, 1.0f));

                Device.SetRenderTarget(null);

                return;
            }

            Device.Clear(new Color(0.0f, 0.0f, 0.0f, 0.0f));

            Device.BlendState = _additiveBlendingState;
            Device.DepthStencilState = DepthStencilState.None;

            _ambientLight.Render(cameraComponent, gBufferTargets.Normal, renderTargets.LightMap);

            foreach (LightComponent lightComponent in scene.GetComponentsOfType<LightComponent>())
                if (lightComponent.IsActive)
                    switch (lightComponent.ShadowMapType)
                    {
                        case ShadowMapType.None:
                            _lightPrePass.RenderNoneSM(lightComponent, cameraComponent,
                                gBufferTargets.Depth, gBufferTargets.Normal,
                                gBufferTargets.MotionVectorsSpecularPowerTexture, renderTargets.LightMap);
                            break;
                        case ShadowMapType.DualParaboloid:
                            _lightPrePass.RenderDualParaboloidSM(lightComponent,
                                lightComponent.ShadowMap as ShadowMapDualParaboloid, cameraComponent,
                                gBufferTargets.Depth, gBufferTargets.Normal,
                                gBufferTargets.MotionVectorsSpecularPowerTexture, renderTargets.LightMap);
                            break;
                    }

            Device.SetRenderTarget(null);
        }

        /// <summary>
        /// Fill Scene Map (material pass)
        /// </summary>
        private void UpdateSceneMap(CameraComponent cameraComponent, DeferredRendererTargets renderTargets,
            IRenderingPipeline pipeline)
        {
            RasterizerState rState = new RasterizerState
            {
                FillMode = FillMode.WireFrame,
                CullMode = CullMode.CullCounterClockwiseFace
            };
            //ShevaEngine.Core.ShevaEngine.Instance.GraphicsDevice.RasterizerState = rState;

            Device.BlendState = BlendState.Opaque;
            Device.RasterizerState = RasterizerState.CullNone;

            DepthStencilState dState = new DepthStencilState
            {
                DepthBufferEnable = true,
                DepthBufferFunction = CompareFunction.LessEqual,
                DepthBufferWriteEnable = true
            };

            Device.DepthStencilState = dState;

            Device.SetRenderTarget(renderTargets.SceneFinalTexture);

            Device.Clear(cameraComponent.ClearColor);

            //            if (pipeline.ActualCount == 0)
            //                return;

            //            Effect actualEffect = null;

            //            LastRenderingPair = InitialRenderingPair;
            //            Matrices.Clear();


            //            Matrices.Add(pipeline.DrawCalls[0].Matrix);

            //            DrawCallKey = pipeline.DrawCalls[0].DrawCallKey;

            //            GraphicsMaterial material = MaterialManager.Instance[DrawCallKey.MaterialID].GraphicsProfile;

            //            actualEffect = material.Effect;

            //            actualEffect.Parameters["View"].SetValue(Viewport.Camera.ViewMatrix);
            //            actualEffect.Parameters["Proj"].SetValue(Viewport.Camera.ProjectionMatrix);

            //            actualEffect.Parameters["LightMap"].SetValue(LightMap);
            //            actualEffect.Parameters["CameraPosition"].SetValue(Viewport.Camera.Position);

            //            material.SetDefaultGraphicsMaterialParameters();
            //            LastDrawCallKey.MaterialParamsID = DrawCallKey.None;

            //            actualEffect.CurrentTechnique.Passes[0].Apply();

            //            if (DrawCallKey.TextureState != DrawCallKey.None)
            //            {
            //                TextureState textureState = TextureManager.Instance[DrawCallKey.TextureState];

            //                foreach (KeyValuePair<string, Texture> texture in textureState)
            //                    actualEffect.Parameters[texture.Key].SetValue(texture.Value);
            //            }

            //            if (DrawCallKey.MaterialParamsID != DrawCallKey.None)
            //                MaterialManager.Instance[DrawCallKey.MaterialID].GraphicsProfile.SetGraphicsMaterialParameters(DrawCallKey.MaterialParamsID);

            //            if (DrawCallKey.BuffersID != DrawCallKey.None)
            //                GraphicsBuffersManager.Instance.SetBuffers(DrawCallKey.BuffersID);

            //            LastDrawCallKey = DrawCallKey;
            //            LastRenderingPair = pipeline.DrawCalls[0];

            //            for (int i = 1; i < pipeline.ActualCount; i++)
            //            {
            //                if (pipeline.DrawCalls[i].DrawCallKey != LastRenderingPair.DrawCallKey ||
            //                    pipeline.DrawCalls[i].DrawCall != LastRenderingPair.DrawCall)
            //                {
            //                    DrawCall.RenderDrawCalls(actualEffect, LastRenderingPair.DrawCall, Matrices.ToArray());

            //                    Matrices.Clear();
            //                    Matrices.Add(pipeline.DrawCalls[i].Matrix);

            //                    DrawCallKey = pipeline.DrawCalls[i].DrawCallKey;

            //                    if (DrawCallKey.MaterialID != LastDrawCallKey.MaterialID)
            //                    {
            //                        material = MaterialManager.Instance[DrawCallKey.MaterialID].GraphicsProfile;

            //                        actualEffect = material.Effect;

            //                        actualEffect.Parameters["View"].SetValue(Viewport.Camera.ViewMatrix);
            //                        actualEffect.Parameters["Proj"].SetValue(Viewport.Camera.ProjectionMatrix);

            //                        actualEffect.Parameters["LightMap"].SetValue(LightMap);
            //                        actualEffect.Parameters["CameraPosition"].SetValue(Viewport.Camera.Position);

            //                        material.SetDefaultGraphicsMaterialParameters();
            //                        LastDrawCallKey.MaterialParamsID = DrawCallKey.None;

            //                        actualEffect.CurrentTechnique.Passes[0].Apply();                        
            //                    }


            //                    if (DrawCallKey.TextureState != DrawCallKey.None &&
            //                        DrawCallKey.TextureState != LastDrawCallKey.TextureState)
            //                    {
            //                        TextureState textureState = TextureManager.Instance[DrawCallKey.TextureState];

            //                        foreach (KeyValuePair<string, Texture> texture in textureState)
            //                            actualEffect.Parameters[texture.Key].SetValue(texture.Value);                        
            //                    }

            //                    if (DrawCallKey.MaterialParamsID != DrawCallKey.None &&
            //                        DrawCallKey.MaterialParamsID != LastDrawCallKey.MaterialParamsID)
            //                    {
            //                        MaterialManager.Instance[DrawCallKey.MaterialID].GraphicsProfile.SetGraphicsMaterialParameters(DrawCallKey.MaterialParamsID);
            //                    }


            //                    if (DrawCallKey.BuffersID != DrawCallKey.None &&
            //                        DrawCallKey.BuffersID != LastDrawCallKey.BuffersID)
            //                        GraphicsBuffersManager.Instance.SetBuffers(DrawCallKey.BuffersID);

            //                    LastDrawCallKey = DrawCallKey;
            //                    LastRenderingPair = pipeline.DrawCalls[i];
            //                }
            //                else
            //                    Matrices.Add(pipeline.DrawCalls[i].Matrix);
            //            }

            //            DrawCall.RenderDrawCalls(actualEffect, LastRenderingPair.DrawCall, Matrices.ToArray());

            //            if (Viewport.Gui != null)
            //                Viewport.Gui.Draw();

            Device.SetRenderTarget(null);
            Device.RasterizerState = RasterizerState.CullNone;
        }

        /// <summary>
        /// Render to frame buffer and post process.
        /// </summary>
        private void UpdatePostProcesses(DeferredRendererTargets renderTargets)
        {
            Device.SetRenderTarget(renderTargets.SceneFinalTexture);
            Device.Clear(Color.Black);

            _postProcessing.Render(renderTargets.SceneHDRTexture);

            Device.SetRenderTarget(null);
        }

        /// <summary>
        /// Update final target.
        /// </summary>
        private void UpdateFinalTarget(CameraComponent cameraComponent, Rectangle cameraViewport, DeferredRendererTargets renderTargets)
        {
            UpdateFinalNullTarget(cameraComponent, cameraViewport, renderTargets);            
        }

        /// <summary>
        /// Update final null target.
        /// </summary>
        private void UpdateFinalNullTarget(CameraComponent cameraComponent, Rectangle cameraViewport, DeferredRendererTargets renderTargets)
        {
            SetViewport(cameraViewport);

            RenderRenderTarget(renderTargets.SceneFinalTexture);
        }
    }
}

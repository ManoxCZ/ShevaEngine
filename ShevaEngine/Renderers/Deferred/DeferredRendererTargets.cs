﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// Deferred renderers targets.
    /// </summary>
    internal class DeferredRendererTargets
    {
        private Vector2 _size = Vector2.Zero;
        /// <summary>Light Pre Pass Map.</summary>
        internal RenderTarget2D LightMap { get; private set; }
        /// <summary>Scene texture in HDR format (before tone mapping).</summary>
        internal RenderTarget2D SceneHDRTexture { get; private set; }
        /// <summary>Scene Final Texture (post processed).</summary>
        internal RenderTarget2D SceneFinalTexture { get; private set; }


        /// <summary>
        /// Update.
        /// </summary>        
        internal void Update(GraphicsDevice device, Rectangle cameraViewport)
        {
            if ((int)_size.X != cameraViewport.Width || (int)_size.Y != cameraViewport.Height)
            {
                // Regenerate render targets.
                _size = new Vector2(cameraViewport.Width, cameraViewport.Height);

                LightMap = Renderer.CreateRenderTarget(device, _size, RenderTargetSizeType.FullScreen,
                    SurfaceFormat.Color, DepthFormat.None);

                SceneHDRTexture = Renderer.CreateRenderTarget(device, _size, RenderTargetSizeType.FullScreen,
                    SurfaceFormat.HdrBlendable, DepthFormat.Depth24);

                SceneFinalTexture = Renderer.CreateRenderTarget(device, _size, RenderTargetSizeType.FullScreen,
                    SurfaceFormat.Color, DepthFormat.None);
            }
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// GBuffer render targets.
    /// </summary>
    internal class GBufferRenderTargets
    {
        private Vector2 _size = Vector2.Zero;
        /// <summary>Single Z-Buffer texture with 32 bits single channel precision. Equation: -DepthVS / FarPlane</summary>
        internal RenderTarget2D Depth { get; private set; }
        /// <summary>Normal Map in half vector 2 format (r16f g16f) and using spherical coordinates.</summary>
        internal RenderTarget2D Normal { get; private set; }
        /// <summary>
        /// R: Motion vector X
        /// G: Motion vector Y
        /// B: Specular Power.
        /// A: Unused... yet.
        /// </summary>
        internal RenderTarget2D MotionVectorsSpecularPowerTexture { get; private set; }

        
        /// <summary>
        /// Update.
        /// </summary>        
        internal void Update(GraphicsDevice device, Rectangle cameraViewport)
        {
            if ((int)_size.X != cameraViewport.Width || (int)_size.Y != cameraViewport.Height)
            {
                // Regenerate render targets.
                _size = new Vector2(cameraViewport.Width, cameraViewport.Height);
                
                // Multisampling on normal and depth maps makes no physical sense!!
                // 32 bits single channel precision
                Depth = Renderer.CreateRenderTarget(device, _size, RenderTargetSizeType.FullScreen, true);

                // Half vector 2 format (r16f g16f). Be careful, in some GPUs this surface format is changed to the RGBA1010102 format. 
                // The XBOX 360 supports it though (http://blogs.msdn.com/b/shawnhar/archive/2010/07/09/rendertarget-formats-in-xna-game-studio-4-0.aspx)
                Normal = Renderer.CreateRenderTarget(device, _size, RenderTargetSizeType.FullScreen, SurfaceFormat.HalfVector2, false);

                // R: Motion vector X
                // G: Motion vector Y
                // B: Specular Power.
                // A: Unused... yet.
                MotionVectorsSpecularPowerTexture = Renderer.CreateRenderTarget(device, _size, RenderTargetSizeType.FullScreen, SurfaceFormat.HalfVector2, false);
            }
        }
    }
}

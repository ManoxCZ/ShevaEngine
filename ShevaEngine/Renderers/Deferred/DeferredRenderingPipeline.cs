﻿using Microsoft.Xna.Framework;
using ShevaEngine.Graphics.Materials;
using ShevaEngine.Graphics.Meshes;
using ShevaEngine.Renderers.RenderingPipelines;
using System;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// Rendering pipeline.
    /// </summary>
    public class DeferredRenderingPipeline : IRenderingPipeline
    {
        private readonly RenderingPipelineSwitchNode<UInt16> _rootNode;


        /// <summary>
        /// Constructor.
        /// </summary>
        public DeferredRenderingPipeline()
        {
            _rootNode = new RenderingPipelineSwitchNode<UInt16>();
        }

        /// <summary>
        /// Clear.
        /// </summary>
        public void Clear()
        {
            _rootNode.Clear();
        }

        /// <summary>
        /// Add mesh.
        /// </summary>
        public void AddMesh(UInt16 layer, Mesh mesh, GraphicsMaterial material, Matrix worldMatrix)
        {
            AddMeshLayer(layer, mesh, material, worldMatrix, _rootNode);
        }

        private void AddMeshLayer(UInt16 layer, Mesh mesh, GraphicsMaterial material, Matrix worldMatrix, RenderingPipelineSwitchNode<UInt16> layersNode)
        {
            //layersNode
        }
    }
}

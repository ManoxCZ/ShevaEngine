﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Entities.Components;
using ShevaEngine.Graphics.Shaders;
using ShevaEngine.Renderers.SphericalHarmonics;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// Ambient Light.
    /// </summary>
    public class AmbientLight
    {
        private readonly Shader _shader;
        private readonly Effect _effect;
        private readonly ScreenPlane _screenPlane;
        /// <summary>The ambient light color When the light pre pass is cleared this color is used to fill the buffer.
        /// The intensity property doesn’t affect this color.</summary>         
        public Color Color = new Color(0, 0, 0);
        /// <summary>Light intensity. The intensity doesn’t affect the color property.</summary>
        private float _intensity = 0.01f;
        /// <summary>Ambient Occlusion Strength.</summary>
        public float AmbientOcclusionStrength = 5;
        /// <summary>Spherical Harmonic Ambient Light. They are great for store low frequency ambient colors and are very fast.</summary>
        public SphericalHarmonicL2 SphericalHarmonicAmbientLight;
        /// <summary>Ambient Occlusion Effect. If null no ambient occlusion will be used.</summary>
        //public AmbientOcclusion AmbientOcclusion;
        /// <summary>Light intensity. The intensity doesn’t affect the color property.</summary>
        public float Intensity
        {
            get => _intensity;
            set
            {
                _intensity = value;
                if (_intensity < 0)
                    _intensity = 0;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public AmbientLight(ShevaGame game)
        {
            _shader = game.AssetsManager.Get<Shader>(@"Assets\Shaders\LightPrePass\AmbientLight.shader");
            _effect = game.GraphicsSystem.ShadersManager[_shader];
            
            _screenPlane = new ScreenPlane(game.GraphicsSystem.GraphicsDevice);

            SphericalHarmonicAmbientLight = new SphericalHarmonicL2();
            SphericalHarmonicAmbientLight.Fill(1, 1, 1);
            //SphericalHarmonicAmbientLight.Fill(0, 0, 0);
        }


        /// <summary>
        /// Render to the light pre pass texture.
        /// </summary>
        public void Render(CameraComponent cameraComponent, RenderTarget2D normalMap, RenderTarget2D lightPrePassMap)
        {
            _effect.Parameters["HalfPixel"].SetValue(new Vector2(-1f / lightPrePassMap.Width, 1f / lightPrePassMap.Height));
            _effect.Parameters["NormalMapHV2"].SetValue(normalMap);
            _effect.Parameters["sphericalHarmonicBase"].SetValue(SphericalHarmonicAmbientLight.Coeficients);
            _effect.Parameters["intensity"].SetValue(Intensity);
            _effect.Parameters["viewI"].SetValue(Matrix.Invert(cameraComponent.ViewMatrix));
            _effect.Parameters["ambientOcclusionStrength"].SetValue(AmbientOcclusionStrength);

            //if (AmbientOcclusion == null)
            _effect.CurrentTechnique = _effect.Techniques["AmbientLightSH"];
            //else
            //{
            //    Effect.CurrentTechnique = Effect.Techniques["AmbientLightSHSSAO"];
            //    // And finally to the ambient light shader.
            //    SetAmbientOcclusionTexture(AmbientOcclusion.AmbientOcclusionTexture);
            //}

            _effect.CurrentTechnique.Passes[0].Apply();

            _screenPlane.Render();
        }
    }
}

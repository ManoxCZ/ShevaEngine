﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShevaEngine.Renderers.Deferred
{
    /// <summary>
    /// A screen plane used for most screen shaders.
    /// </summary>
    internal class ScreenPlane
    {
        private readonly GraphicsDevice _device;
        private readonly VertexBuffer _vertexBuffer;

        /// <summary>
        /// Constructor.
        /// </summary>
        public ScreenPlane(GraphicsDevice device)
        {
            _device = device;

            VertexPositionTexture[] vertices = 
                {
                    new VertexPositionTexture(new Vector3(-1.0f, -1.0f, 0f), new Vector2(0, 1)),
                    new VertexPositionTexture(new Vector3(-1.0f, 1.0f, 0f),  new Vector2(0, 0)),
                    new VertexPositionTexture(new Vector3(1.0f, -1.0f, 0f),  new Vector2(1, 1)),
                    new VertexPositionTexture(new Vector3(1.0f, 1.0f, 0f),   new Vector2(1, 0)),
                };
            
            _vertexBuffer = new VertexBuffer(_device, typeof(VertexPositionTexture), vertices.Length, BufferUsage.WriteOnly);
            _vertexBuffer.SetData(vertices);
        }

        /// <summary>
        /// Render a screen plane.
        /// </summary>
        public void Render()
        {
            _device.SetVertexBuffer(_vertexBuffer);
            _device.DrawPrimitives(PrimitiveType.TriangleStrip, 0, 2);
        }
    }
}

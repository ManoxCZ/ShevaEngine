﻿namespace ShevaEngine.Renderers
{
    /// <summary>
    /// Antialiasing Type.
    /// </summary>
    public enum RenderTargetAntialiasingType
    {
        /// <summary>System value.</summary>
        System,
        /// <summary>No antialiasing.</summary>
        NoAntialiasing
    }
}

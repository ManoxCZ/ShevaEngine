﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Entities.Components;
using ShevaEngine.Graphics;
using ShevaEngine.Graphics.DrawCalls;
using ShevaEngine.Graphics.Effects;
using System;
using System.Collections.Generic;

namespace ShevaEngine.Renderers
{
    public class RenderingContext
    {
        private readonly GraphicsSystem _graphicsSystem;
        private Effect _actualEffect;
        private readonly CameraComponent _camera;


        /// <summary>
        /// Constructor.
        /// </summary>
        public RenderingContext(GraphicsSystem graphicsSystem, CameraComponent camera)
        {
            _graphicsSystem = graphicsSystem;
            _camera = camera;
            _actualEffect = null;
        }

        /// <summary>
        /// Set material.
        /// </summary>
        public void SetMaterial(UInt16 materialId)
        {
            _actualEffect = new AlphaTestEffect(_graphicsSystem.GraphicsDevice);
            _actualEffect.CurrentTechnique.Passes[0].Apply();

            // Setup camera.
            _actualEffect.Parameters[EffectConstants.ViewMatrix].SetValue(_camera.ViewMatrix);
            _actualEffect.Parameters[EffectConstants.ProjMatrix].SetValue(_camera.ProjectionMatrix);
        }

        /// <summary>
        /// Draw mesh instances.
        /// </summary>
        public void DrawMeshInstances(UInt16 meshId, List<Matrix> worldMatrices)
        {
            if (_actualEffect == null)
                return;

            DrawCall drawCall = _graphicsSystem.DrawCallManager[meshId];

            if (drawCall == null)
                return;

            // Setting buffers.
            _graphicsSystem.GraphicsDevice.SetVertexBuffer(drawCall.VertexBuffer);
            _graphicsSystem.GraphicsDevice.Indices = drawCall.IndexBuffer;

            // Render instances.
            foreach (Matrix worldMatrix in worldMatrices)
            {
                _actualEffect.Parameters[EffectConstants.WorldMatrix].SetValue(worldMatrix);                

                drawCall.Render(_graphicsSystem.GraphicsDevice);
            }
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Entities.Components;
using ShevaEngine.Renderers.RenderingPipelines;
using ShevaEngine.Scenes;
using System;
using System.Collections.Generic;
using ShevaEngine.Renderers.Deferred;

namespace ShevaEngine.Renderers.Shadows
{
    /// <summary>
    /// Dual paraboloid SM.
    /// </summary>
    public class ShadowMapDualParaboloid : ShadowMap
    {
        public RenderTarget2D ShadowMapTop;
        public RenderTarget2D ShadowMapBottom;
        public Int32 ShadowMapSize;
        private Matrix _lightViewCamera;
        private readonly Effect _shadowMapEffect;
        //private RenderingPackage LastRenderingPair;
        //private RenderingPackage InitialRenderingPair;
        //private GraphicsMaterial LastMaterial;
        //private DynamicVertexBuffer InstancesVertexBuffer;
        private List<Matrix> _matrices;


        /// <summary>
        /// Constructor.
        /// </summary>
        public ShadowMapDualParaboloid(GraphicsDevice device)
            : base(device)
        {            
            ShadowMapSize = 1024;

            ShadowMapTop = new RenderTarget2D(device, ShadowMapSize, ShadowMapSize, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8);
            ShadowMapBottom = new RenderTarget2D(device, ShadowMapSize, ShadowMapSize, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8);

            //ShadowMapEffect = MaterialManager.Instance.GetEffect(@"Shadows\DualParaboloidSM");

            //InitialRenderingPair = new RenderingPackage();
            //InitialRenderingPair.DrawCall = DrawCallManager.Instance.CreateIndexedPrimitivesDrawCall(PrimitiveType.LineList, 0, 0, 0, 0, 0);
            //InitialRenderingPair.DrawCallKey = new DrawCallKey(UInt16.MaxValue, UInt16.MaxValue, UInt16.MaxValue, UInt16.MaxValue);

            _matrices = new List<Matrix>();

            //InstancesVertexBuffer = new DynamicVertexBuffer(ShevaEngine.Core.ShevaEngine.Instance.GraphicsDevice,
            //            InstanceVertexDeclaration.BlendWeight, 128, BufferUsage.WriteOnly);
        }


        /// <summary>
        /// Method creates shadow map.
        /// </summary>
        public override void UpdateShadowMap(LightComponent lightComponent, Scene scene)
        {
            Device.SetRenderTarget(ShadowMapTop);

            Device.Clear(ClearOptions.DepthBuffer | ClearOptions.Target, Color.White, 1.0f, 1);

            Device.BlendState = BlendState.Opaque;
            //Device.RenderState.AlphaBlendEnable = false;            

            DepthStencilState depthStencilState = new DepthStencilState
            {
                DepthBufferEnable = true,
                DepthBufferWriteEnable = true,
                DepthBufferFunction = CompareFunction.Less
            };

            Device.DepthStencilState = depthStencilState;

            RasterizerState rasterizerState = new RasterizerState
            {
                CullMode = CullMode.CullCounterClockwiseFace
            };

            Device.RasterizerState = rasterizerState;

            Vector3 lightPosition = lightComponent.GetComponent<TransformComponent>().WorldMatrix.Translation;

            _lightViewCamera = Matrix.CreateLookAt(lightPosition, lightPosition - new Vector3(0, 1, 0), Vector3.UnitZ);

            IRenderingPipeline pipeline = new DeferredRenderingPipeline();

            scene.UpdateRenderingPipeline(pipeline, lightComponent);

            _shadowMapEffect.Parameters["ShadowMapFarPlane"].SetValue(lightComponent.Radius);
            _shadowMapEffect.Parameters["View"].SetValue(_lightViewCamera);

            _shadowMapEffect.Parameters["ShadowMapDirection"].SetValue(1.0f);

            _shadowMapEffect.CurrentTechnique.Passes[0].Apply();

            //RenderPipeline(pipeline);

            Device.SetRenderTarget(ShadowMapBottom);

            Device.Clear(ClearOptions.DepthBuffer | ClearOptions.Target, Color.White, 1.0f, 1);

            rasterizerState = new RasterizerState
            {
                CullMode = CullMode.CullClockwiseFace
            };

            Device.RasterizerState = rasterizerState;

            _shadowMapEffect.Parameters["ShadowMapDirection"].SetValue(-1.0f);

            //RenderPipeline(pipeline);

            Device.SetRenderTarget(null);

            //using (StreamWriter sw = new StreamWriter(@"C:\Marek\SMTop.jpg"))
            //{
            //    ShadowMapTop.SaveAsJpeg(sw.BaseStream, ShadowMapSize, ShadowMapSize);
            //}

            //using (StreamWriter sw = new StreamWriter(@"C:\Marek\SMBottom.jpg"))
            //{
            //    ShadowMapBottom.SaveAsJpeg(sw.BaseStream, ShadowMapSize, ShadowMapSize);
            //}
        }

        ///// <summary>
        ///// Render pipeline method.
        ///// </summary>
        //private void RenderPipeline(RenderingPipeline pipeline)
        //{
        //    LastRenderingPair = InitialRenderingPair;
        //    Matrices.Clear();

        //    for (int i = 0; i < pipeline.ActualCount; i++)
        //    {
        //        if (pipeline.DrawCalls[i].DrawCall != LastRenderingPair.DrawCall)
        //        {
        //            if (Matrices.Count > 0)
        //                DrawCall.RenderDrawCalls(ShadowMapEffect, LastRenderingPair.DrawCall, Matrices.ToArray());

        //            Matrices.Clear();

        //            if (pipeline.DrawCalls[i].DrawCallKey.MaterialID != LastRenderingPair.DrawCallKey.MaterialID)
        //            {
        //                LastMaterial = MaterialManager.Instance[pipeline.DrawCalls[i].DrawCallKey.MaterialID].GraphicsProfile;

        //                if (!LastMaterial.CastShadows)
        //                {
        //                    LastRenderingPair = pipeline.DrawCalls[i];

        //                    continue;
        //                }
        //            }

        //            if (pipeline.DrawCalls[i].DrawCallKey.TextureState != LastRenderingPair.DrawCallKey.TextureState)
        //            {
        //                TextureState textureState = TextureManager.Instance[pipeline.DrawCalls[i].DrawCallKey.TextureState];

        //                //if (textureState.ContainsKey("Diffuse"))
        //                //ShadowMapEffect.Parameters["Diffuse"].SetValue(textureState["Diffuse"]);
        //            }

        //            if (pipeline.DrawCalls[i].DrawCallKey.MaterialParamsID == DrawCallKey.None)
        //                ShadowMapEffect.CurrentTechnique = ShadowMapEffect.Techniques["NoAnimations"];
        //            else if (pipeline.DrawCalls[i].DrawCallKey.MaterialParamsID != LastRenderingPair.DrawCallKey.MaterialParamsID)
        //            {
        //                if (LastMaterial.ParametersManager[pipeline.DrawCalls[i].DrawCallKey.MaterialParamsID].IsAnimated)
        //                {
        //                    LastMaterial.ParametersManager[pipeline.DrawCalls[i].DrawCallKey.MaterialParamsID].AnimatedParameters.Set(
        //                        ShadowMapEffect);

        //                    ShadowMapEffect.CurrentTechnique = ShadowMapEffect.Techniques["WithAnimations"];
        //                }
        //                else
        //                    ShadowMapEffect.CurrentTechnique = ShadowMapEffect.Techniques["NoAnimations"];
        //            }

        //            if (pipeline.DrawCalls[i].DrawCallKey.BuffersID != DrawCallKey.None)
        //                GraphicsBuffersManager.Instance.SetBuffers(pipeline.DrawCalls[i].DrawCallKey.BuffersID);

        //            Matrices.Add(pipeline.DrawCalls[i].Matrix);

        //            LastRenderingPair = pipeline.DrawCalls[i];
        //        }
        //        else if (LastMaterial.CastShadows)
        //            Matrices.Add(pipeline.DrawCalls[i].Matrix);
        //    }

        //    if (Matrices.Count > 0)
        //        DrawCall.RenderDrawCalls(ShadowMapEffect, LastRenderingPair.DrawCall, Matrices.ToArray());
        //}
    }
}

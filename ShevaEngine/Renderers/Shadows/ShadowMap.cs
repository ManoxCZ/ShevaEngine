﻿using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Entities.Components;
using ShevaEngine.Scenes;

namespace ShevaEngine.Renderers.Shadows
{
    /// <summary>
    /// Shadow map.
    /// </summary>
    public abstract class ShadowMap
    {
        protected GraphicsDevice Device;


        /// <summary>
        /// Constructor.
        /// </summary>        
        protected ShadowMap(GraphicsDevice device)
        {
            Device = device;
        }

        /// <summary>
        /// Method updates shadow map.
        /// </summary>
        public abstract void UpdateShadowMap(LightComponent light, Scene scene); 
    }
}

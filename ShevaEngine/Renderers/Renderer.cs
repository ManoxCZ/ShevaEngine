﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShevaEngine.Entities.Components;
using ShevaEngine.Graphics.Shaders;
using ShevaEngine.ImmediateGui;
using ShevaEngine.Renderers.Deferred;
using ShevaEngine.Scenes;
using System;

namespace ShevaEngine.Renderers
{
    /// <summary>
    /// Renderer.
    /// </summary>
    public abstract class Renderer
    {
        protected ShevaGame Game;
        protected GraphicsDevice Device => Game.GraphicsDevice;
        public bool LightingEnabled { get; set; } = true;
        private readonly Shader _screenPlaneShader;
        private readonly Effect _screenPlaneEffect;
        private readonly ScreenPlane _screenPlane;
        private readonly ImGuiManager _imGuiManager;


        /// <summary>
        /// Constructor.
        /// </summary>        
        protected Renderer(ShevaGame game)
        {
            Game = game;

            _screenPlaneShader = game.AssetsManager.Get<Shader>(@"Assets\Shaders\ScreenPlane.shader");
            _screenPlaneEffect = game.GraphicsSystem.ShadersManager[_screenPlaneShader];

            _screenPlane = new ScreenPlane(game.GraphicsSystem.GraphicsDevice);

            _imGuiManager = new ImGuiManager(game);
        }

        /// <summary>
        /// Render.
        /// </summary>
        public virtual void Render(CameraComponent camera, Scene scene)
        {
            RenderScene(camera, scene);

            RenderGui(camera, scene);
        }

        /// <summary>
        /// Render.
        /// </summary>
        public abstract void RenderScene(CameraComponent camera, Scene scene);

        /// <summary>
        /// Render.
        /// </summary>
        public virtual void RenderGui(CameraComponent camera, Scene scene)
        {
            Rectangle cameraViewport = GetCameraViewport(camera);

            _imGuiManager.RenderFrame(cameraViewport, scene.OnGuiComponents);
        }

        /// <summary>
        /// Set viewport.
        /// </summary>
        protected Rectangle GetCameraViewport(CameraComponent camera)
        {
            return new Rectangle(
                (int)(Game.Window.ClientBounds.Width * (camera.Viewport.X / 1000.0f)),
                (int)(Game.Window.ClientBounds.Height * (camera.Viewport.Y / 1000.0f)),
                (int)(Game.Window.ClientBounds.Width * (camera.Viewport.Width / 1000.0f)),
                (int)(Game.Window.ClientBounds.Height * (camera.Viewport.Height / 1000.0f)));
        }

        /// <summary>
        /// Set viewport.
        /// </summary>        
        protected void SetViewport(Rectangle viewport)
        {
            Device.Viewport = new Viewport(viewport);
        }

        /// <summary>
        /// Render render target.
        /// </summary>
        protected void RenderRenderTarget(RenderTarget2D renderTarget)
        {
            Device.BlendState = BlendState.Opaque;
            Device.DepthStencilState = DepthStencilState.None;

            _screenPlaneEffect.Parameters["Diffuse"].SetValue(renderTarget);

            _screenPlaneEffect.CurrentTechnique.Passes[0].Apply();

            _screenPlane.Render();
        }

        /// <summary>
        /// Creates a render target for render to textures.
        /// </summary>
        /// <param name="device"></param>
        /// <param name="viewportSize"></param>
        /// <param name="sizeType">Render target size</param>
        /// <param name="highPrecisionSingleChannelFormat">Uses a rgba 8 bit per channel format or a 32 bit single channel format</param>
        /// <param name="hasDepthBuffer">has depth buffer?</param>
        /// <param name="antialiasingType">Multi sampling type: System value or no antialiasing.</param>
        internal static RenderTarget2D CreateRenderTarget(GraphicsDevice device, Vector2 viewportSize, RenderTargetSizeType sizeType,
            bool highPrecisionSingleChannelFormat = false, bool hasDepthBuffer = true,
            RenderTargetAntialiasingType antialiasingType = RenderTargetAntialiasingType.NoAntialiasing)
        {
            Vector2 size = CalculateSize(viewportSize, sizeType);

            return new RenderTarget2D(device,
                (int)size.X,
                (int)size.Y,
                false,
                highPrecisionSingleChannelFormat ? SurfaceFormat.Single : SurfaceFormat.Color,
                hasDepthBuffer ? DepthFormat.Depth24 : DepthFormat.None,
                antialiasingType == RenderTargetAntialiasingType.System
                    ? device.PresentationParameters.MultiSampleCount
                    : 0,
                RenderTargetUsage.PlatformContents);
        }

        /// <summary>
        /// Creates a render target for render to textures.
        /// </summary>
        /// <param name="device"></param>
        /// <param name="viewportSize"></param>
        /// <param name="sizeType">Render target size</param>
        /// <param name="surfaceFormat">Surface format</param>
        /// <param name="hasDepthBuffer">Has depth buffer?</param>
        /// <param name="antialiasingType">Multi sampling type: System value or no antialiasing.</param>
        internal static RenderTarget2D CreateRenderTarget(GraphicsDevice device, Vector2 viewportSize,
            RenderTargetSizeType sizeType,
            SurfaceFormat surfaceFormat, bool hasDepthBuffer = true,
            RenderTargetAntialiasingType antialiasingType = RenderTargetAntialiasingType.NoAntialiasing)
        {
            Vector2 size = CalculateSize(viewportSize, sizeType);

            return new RenderTarget2D(device,
                (int)size.X,
                (int)size.Y,
                false,
                surfaceFormat,
                hasDepthBuffer ? DepthFormat.Depth24 : DepthFormat.None,
                antialiasingType == RenderTargetAntialiasingType.System
                    ? device.PresentationParameters.MultiSampleCount
                    : 0,
                RenderTargetUsage.DiscardContents);
        }

        /// <summary>
        /// Creates a render target for render to textures.
        /// </summary>
        /// <param name="device"></param>
        /// <param name="viewportSize"></param>
        /// <param name="sizeType">Render target size</param>
        /// <param name="surfaceFormat">Surface format</param>
        /// <param name="depthFormat">Depth Format</param>
        /// <param name="antialiasingType">Multi sampling type: System value or no antialiasing.</param>
        internal static RenderTarget2D CreateRenderTarget(GraphicsDevice device, Vector2 viewportSize,
            RenderTargetSizeType sizeType,
            SurfaceFormat surfaceFormat, DepthFormat depthFormat,
            RenderTargetAntialiasingType antialiasingType = RenderTargetAntialiasingType.NoAntialiasing)
        {
            Vector2 size = CalculateSize(viewportSize, sizeType);

            return new RenderTarget2D(device,
                (int)size.X,
                (int)size.Y,
                false,
                surfaceFormat,
                depthFormat,
                antialiasingType == RenderTargetAntialiasingType.System
                    ? device.PresentationParameters.MultiSampleCount
                    : 0,
                RenderTargetUsage.DiscardContents);
        }

        /// <summary>
        /// Calculate size from size type.
        /// </summary>
        internal static Vector2 CalculateSize(Vector2 viewportSize, RenderTargetSizeType sizeType)
        {
            int width;
            int height;
            switch (sizeType)
            {
                case RenderTargetSizeType.FullScreen:
                    width = (int)viewportSize.X;
                    height = (int)viewportSize.Y;
                    break;
                case RenderTargetSizeType.HalfScreen:
                    width = (int)viewportSize.X / 2;
                    height = (int)viewportSize.Y / 2;
                    break;
                case RenderTargetSizeType.QuarterScreen:
                    width = (int)viewportSize.X / 4;
                    height = (int)viewportSize.Y / 4;
                    break;
                case RenderTargetSizeType.Square256X256:
                    width = 256;
                    height = 256;
                    break;
                case RenderTargetSizeType.Square512X512:
                    width = 512;
                    height = 512;
                    break;
                case RenderTargetSizeType.Square1024X1024:
                    width = 1024;
                    height = 1024;
                    break;
                default:
                    throw new Exception("Render Target error. Size type doesn't exist (probably a bug).");
            }
            return new Vector2(width, height);
        }
    }
}

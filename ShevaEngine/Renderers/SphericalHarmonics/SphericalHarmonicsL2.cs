﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShevaEngine.Renderers.SphericalHarmonics
{
    /// <summary>
    /// Spherical Harmonics for RGB colors.
    /// A spherical harmonic approximates a spherical function using only a few values.
    /// They are great to store low frequency ambient colors and are very fast.
    /// </summary>
    public class SphericalHarmonicL2 : SphericalHarmonic
    {
        /// <summary>
        /// Spherical Harmonic RGB coefficients.
        /// </summary>
        public override Vector3[] Coeficients { get { return new[] { C0, C1, C2, C3, C4, C5, C6, C7, C8 }; } }

        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C0;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C1;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C2;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C3;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C4;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C5;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C6;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C7;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C8;

        /// <summary>
        /// Fill the constants with a color.
        /// </summary>
        public override void Fill(float red, float green, float blue)
        {
            C0 = new Vector3(red, green, blue);
            C1 = new Vector3(red, green, blue);
            C2 = new Vector3(red, green, blue);
            C3 = new Vector3(red, green, blue);
            C4 = new Vector3(red, green, blue);
            C5 = new Vector3(red, green, blue);
            C6 = new Vector3(red, green, blue);
            C7 = new Vector3(red, green, blue);
            C8 = new Vector3(red, green, blue);
        }

        /// <summary>
        /// Add light from a given direction to the SH function. See the class remarks for further details on how an SH can be used to approximate lighting.
        /// Input light inputRGB will be multiplied by weight, and weight will be added to weighting.
        /// </summary>
        /// <param name="rgb">Input light intensity in RGB (usually gamma space) format for the given direction</param>
        /// <param name="direction">direction of the incoming light</param>
        /// <param name="weight">Weighting for this light, usually 1.0f. Use this value, and weighting if averaging a large number of lighting samples (eg, when converting a cube map to an SH)</param>
        public override void AddLight(Vector3 rgb, Vector3 direction, float weight = 1.0f)
        {
            direction = Vector3.Normalize(direction);

            // Slightly different result than the Xen method. The Xen method requires less shader operations.
            #region Ravi Ramamoorthi method

            /*
            // L_{00}.  Note that Y_{00} = 0.282095
            float c = 0.282095f;
            c0 += rgb * c * weight;

            // L_{1m}. -1 <= m <= 1.  The linear terms
            c = 0.488603f;
            c1 += rgb * (c * direction.Y) * weight;   // Y_{1-1} = 0.488603 y
            c2 += rgb * (c * direction.Z) * weight;   // Y_{10}  = 0.488603 z
            c3 += rgb * (c * direction.X) * weight;   // Y_{11}  = 0.488603 x

            // The Quadratic terms, L_{2m} -2 <= m <= 2

            // First, L_{2-2}, L_{2-1}, L_{21} corresponding to xy,yz,xz
            c = 1.092548f;
            c4 += rgb * (c * direction.X * direction.Y) * weight; // Y_{2-2} = 1.092548 xy
            c5 += rgb * (c * direction.Y * direction.Z) * weight; // Y_{2-1} = 1.092548 yz
            c7 += rgb * (c * direction.X * direction.Z) * weight; // Y_{21}  = 1.092548 xz

            // L_{20}.  Note that Y_{20} = 0.315392 (3z^2 - 1)
            c = 0.315392f;
            c6 += rgb * (c * (3 * direction.Z * direction.Z - 1)) * weight;

            // L_{22}.  Note that Y_{22} = 0.546274 (x^2 - y^2)
            c = 0.546274f;
            c8 += rgb * (c * (direction.X * direction.X - direction.Y * direction.Y)) * weight;
            */

            #endregion

            #region Xen method

            float x = direction.X;
            float y = direction.Y;
            float z = direction.Z;

            float r = rgb.X * weight;
            float g = rgb.Y * weight;
            float b = rgb.Z * weight;

            // Spherical Harmonic constants
            const float f0 = 0.25f;
            const float f1 = 0.5f;
            const float f2 = 0.937500143128f;
            const float f3 = 0.234375035782f;

            // Axis constants for SH input
            float const1 = (f1 * (x));
            float const2 = (f1 * (y));
            float const3 = (f1 * (z));
            float const4 = (f2 * (x * y));
            float const5 = (f2 * (y * z));
            float const6 = (f2 * (x * z));
            float const7 = (f3 * (z * z - (1.0f / 3.0f)));
            float const8 = (f3 * (x * x - y * y));

            // Red channel
            C0.X += r * f0;
            C1.X += r * const1;
            C2.X += r * const2;
            C3.X += r * const3;
            C4.X += r * const4;
            C5.X += r * const5;
            C6.X += r * const6;
            C7.X += r * const7;
            C8.X += r * const8;
            // Green channel
            C0.Y += g * f0;
            C1.Y += g * const1;
            C2.Y += g * const2;
            C3.Y += g * const3;
            C4.Y += g * const4;
            C5.Y += g * const5;
            C6.Y += g * const6;
            C7.Y += g * const7;
            C8.Y += g * const8;
            // Blue channel
            C0.Z += b * f0;
            C1.Z += b * const1;
            C2.Z += b * const2;
            C3.Z += b * const3;
            C4.Z += b * const4;
            C5.Z += b * const5;
            C6.Z += b * const6;
            C7.Z += b * const7;
            C8.Z += b * const8;

            #endregion

            // Store the accumulated weighting, useful if averaging the light input is desired.
            Weighting += weight;
        }

        /// <summary>
        /// Sample the spherical harmonic in the given direction.
        /// </summary>
        public override Vector3 SampleDirection(Vector3 direction)
        {
            direction = Vector3.Normalize(direction);
            float x = direction.X;
            float y = direction.Y;
            float z = direction.Z;

            float xy = x * y;
            float yz = y * z;
            float xz = x * z;

            float zz3 = (z * z) - (1.0f / 3.0f);
            float xxyy = (x * x) - (y * y);

            float r = C0.X + C1.X * x + C2.X * y + C3.X * z + C4.X * xy +
                C5.X * yz + C6.X * xz + C7.X * zz3 + C8.X * xxyy;
            float g = C0.Y + C1.Y * x + C2.Y * y + C3.Y * z + C4.Y * xy +
                C5.Y * yz + C6.Y * xz + C7.Y * zz3 + C8.Y * xxyy;
            float b = C0.Z + C1.Z * x + C2.Z * y + C3.Z * z + C4.Z * xy +
                C5.Z * yz + C6.Z * xz + C7.Z * zz3 + C8.Z * xxyy;

            return new Vector3(r, g, b);
        }

        /// <summary>
        /// Add two spherical harmonics together.
        /// </summary>
        public static SphericalHarmonicL2 Add(SphericalHarmonicL2 x, SphericalHarmonicL2 y)
        {
            return new SphericalHarmonicL2
            {
                Weighting = x.Weighting + y.Weighting,
                C0 = { X = x.C0.X + y.C0.X, Y = x.C0.Y + y.C0.Y, Z = x.C0.Z + y.C0.Z },
                C1 = { X = x.C1.X + y.C1.X, Y = x.C1.Y + y.C1.Y, Z = x.C1.Z + y.C1.Z },
                C2 = { X = x.C2.X + y.C2.X, Y = x.C2.Y + y.C2.Y, Z = x.C2.Z + y.C2.Z },
                C3 = { X = x.C3.X + y.C3.X, Y = x.C3.Y + y.C3.Y, Z = x.C3.Z + y.C3.Z },
                C4 = { X = x.C4.X + y.C4.X, Y = x.C4.Y + y.C4.Y, Z = x.C4.Z + y.C4.Z },
                C5 = { X = x.C5.X + y.C5.X, Y = x.C5.Y + y.C5.Y, Z = x.C5.Z + y.C5.Z },
                C6 = { X = x.C6.X + y.C6.X, Y = x.C6.Y + y.C6.Y, Z = x.C6.Z + y.C6.Z },
                C7 = { X = x.C7.X + y.C7.X, Y = x.C7.Y + y.C7.Y, Z = x.C7.Z + y.C7.Z },
                C8 = { X = x.C8.X + y.C8.X, Y = x.C8.Y + y.C8.Y, Z = x.C8.Z + y.C8.Z }
            };
        } // Add

        /// <summary>
        /// Add two spherical harmonics together
        /// </summary>
        public static SphericalHarmonicL2 operator +(SphericalHarmonicL2 x, SphericalHarmonicL2 y)
        {
            return new SphericalHarmonicL2
            {
                Weighting = x.Weighting + y.Weighting,
                C0 = { X = x.C0.X + y.C0.X, Y = x.C0.Y + y.C0.Y, Z = x.C0.Z + y.C0.Z },
                C1 = { X = x.C1.X + y.C1.X, Y = x.C1.Y + y.C1.Y, Z = x.C1.Z + y.C1.Z },
                C2 = { X = x.C2.X + y.C2.X, Y = x.C2.Y + y.C2.Y, Z = x.C2.Z + y.C2.Z },
                C3 = { X = x.C3.X + y.C3.X, Y = x.C3.Y + y.C3.Y, Z = x.C3.Z + y.C3.Z },
                C4 = { X = x.C4.X + y.C4.X, Y = x.C4.Y + y.C4.Y, Z = x.C4.Z + y.C4.Z },
                C5 = { X = x.C5.X + y.C5.X, Y = x.C5.Y + y.C5.Y, Z = x.C5.Z + y.C5.Z },
                C6 = { X = x.C6.X + y.C6.X, Y = x.C6.Y + y.C6.Y, Z = x.C6.Z + y.C6.Z },
                C7 = { X = x.C7.X + y.C7.X, Y = x.C7.Y + y.C7.Y, Z = x.C7.Z + y.C7.Z },
                C8 = { X = x.C8.X + y.C8.X, Y = x.C8.Y + y.C8.Y, Z = x.C8.Z + y.C8.Z }
            };
        } // Add

        /// <summary>
        /// Multiply a spherical harmonic by a constant scale factor
        /// </summary>
        public static SphericalHarmonicL2 Multiply(SphericalHarmonicL2 x, float scale)
        {
            return new SphericalHarmonicL2
            {
                Weighting = x.Weighting * scale,
                C0 = { X = x.C0.X * scale, Y = x.C0.Y * scale, Z = x.C0.Z * scale },
                C1 = { X = x.C1.X * scale, Y = x.C1.Y * scale, Z = x.C1.Z * scale },
                C2 = { X = x.C2.X * scale, Y = x.C2.Y * scale, Z = x.C2.Z * scale },
                C3 = { X = x.C3.X * scale, Y = x.C3.Y * scale, Z = x.C3.Z * scale },
                C4 = { X = x.C4.X * scale, Y = x.C4.Y * scale, Z = x.C4.Z * scale },
                C5 = { X = x.C5.X * scale, Y = x.C5.Y * scale, Z = x.C5.Z * scale },
                C6 = { X = x.C6.X * scale, Y = x.C6.Y * scale, Z = x.C6.Z * scale },
                C7 = { X = x.C7.X * scale, Y = x.C7.Y * scale, Z = x.C7.Z * scale },
                C8 = { X = x.C8.X * scale, Y = x.C8.Y * scale, Z = x.C8.Z * scale }
            };
        } // Multiply

        /// <summary>
        /// Multiply a spherical harmonic by a constant scale factor.
        /// </summary>
        public static SphericalHarmonicL2 operator *(SphericalHarmonicL2 x, float scale)
        {
            return new SphericalHarmonicL2
            {
                Weighting = x.Weighting * scale,
                C0 = { X = x.C0.X * scale, Y = x.C0.Y * scale, Z = x.C0.Z * scale },
                C1 = { X = x.C1.X * scale, Y = x.C1.Y * scale, Z = x.C1.Z * scale },
                C2 = { X = x.C2.X * scale, Y = x.C2.Y * scale, Z = x.C2.Z * scale },
                C3 = { X = x.C3.X * scale, Y = x.C3.Y * scale, Z = x.C3.Z * scale },
                C4 = { X = x.C4.X * scale, Y = x.C4.Y * scale, Z = x.C4.Z * scale },
                C5 = { X = x.C5.X * scale, Y = x.C5.Y * scale, Z = x.C5.Z * scale },
                C6 = { X = x.C6.X * scale, Y = x.C6.Y * scale, Z = x.C6.Z * scale },
                C7 = { X = x.C7.X * scale, Y = x.C7.Y * scale, Z = x.C7.Z * scale },
                C8 = { X = x.C8.X * scale, Y = x.C8.Y * scale, Z = x.C8.Z * scale }
            };
        } // Multiply

        /// <summary>
        /// Linear interpolate (Lerp) between two spherical harmonics based on a interpolation factor.
        /// </summary>
        /// <param name="factor">Determines the interpolation point. When factor is 1.0, the output will be x, when factor is 0.0, the output will be y</param>
        public static SphericalHarmonicL2 Lerp(ref SphericalHarmonicL2 x, ref SphericalHarmonicL2 y, float factor)
        {
            float xs = factor;
            float ys = 1.0f - factor;
            return new SphericalHarmonicL2
            {
                Weighting = x.Weighting * xs + y.Weighting * ys,
                C0 = { X = xs * x.C0.X + ys * y.C0.X, Y = xs * x.C0.Y + ys * y.C0.Y, Z = xs * x.C0.Z + ys * y.C0.Z },
                C1 = { X = xs * x.C1.X + ys * y.C1.X, Y = xs * x.C1.Y + ys * y.C1.Y, Z = xs * x.C1.Z + ys * y.C1.Z },
                C2 = { X = xs * x.C2.X + ys * y.C2.X, Y = xs * x.C2.Y + ys * y.C2.Y, Z = xs * x.C2.Z + ys * y.C2.Z },
                C3 = { X = xs * x.C3.X + ys * y.C3.X, Y = xs * x.C3.Y + ys * y.C3.Y, Z = xs * x.C3.Z + ys * y.C3.Z },
                C4 = { X = xs * x.C4.X + ys * y.C4.X, Y = xs * x.C4.Y + ys * y.C4.Y, Z = xs * x.C4.Z + ys * y.C4.Z },
                C5 = { X = xs * x.C5.X + ys * y.C5.X, Y = xs * x.C5.Y + ys * y.C5.Y, Z = xs * x.C5.Z + ys * y.C5.Z },
                C6 = { X = xs * x.C6.X + ys * y.C6.X, Y = xs * x.C6.Y + ys * y.C6.Y, Z = xs * x.C6.Z + ys * y.C6.Z },
                C7 = { X = xs * x.C7.X + ys * y.C7.X, Y = xs * x.C7.Y + ys * y.C7.Y, Z = xs * x.C7.Z + ys * y.C7.Z },
                C8 = { X = xs * x.C8.X + ys * y.C8.X, Y = xs * x.C8.Y + ys * y.C8.Y, Z = xs * x.C8.Z + ys * y.C8.Z }
            };
        } // Lerp

        /// <summary>
        /// Generate a spherical harmonic from the faces of a cubemap, treating each pixel as a light source and averaging the result.
        /// </summary>
        public static SphericalHarmonicL2 GenerateSphericalHarmonicFromCubeMap(TextureCube cubeMap)
        {
            SphericalHarmonicL2 sh = new SphericalHarmonicL2();

            // Extract the 6 faces of the cubemap.
            for (int face = 0; face < 6; face++)
            {
                CubeMapFace faceId = (CubeMapFace)face;

                // Get the transformation for this face,
                Matrix cubeFaceMatrix;
                switch (faceId)
                {
                    case CubeMapFace.PositiveX:
                        cubeFaceMatrix = Matrix.CreateLookAt(Vector3.Zero, new Vector3(1, 0, 0), new Vector3(0, 1, 0));
                        break;
                    case CubeMapFace.NegativeX:
                        cubeFaceMatrix = Matrix.CreateLookAt(Vector3.Zero, new Vector3(-1, 0, 0), new Vector3(0, 1, 0));
                        break;
                    case CubeMapFace.PositiveY:
                        cubeFaceMatrix = Matrix.CreateLookAt(Vector3.Zero, new Vector3(0, 1, 0), new Vector3(0, 0, 1));
                        break;
                    case CubeMapFace.NegativeY:
                        cubeFaceMatrix = Matrix.CreateLookAt(Vector3.Zero, new Vector3(0, -1, 0), new Vector3(0, 0, -1));
                        break;
                    case CubeMapFace.PositiveZ:
                        cubeFaceMatrix = Matrix.CreateLookAt(Vector3.Zero, new Vector3(0, 0, -1), new Vector3(0, 1, 0));
                        break;
                    case CubeMapFace.NegativeZ:
                        cubeFaceMatrix = Matrix.CreateLookAt(Vector3.Zero, new Vector3(0, 0, 1), new Vector3(0, 1, 0));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                Color[] colorArray = new Color[cubeMap.Size * cubeMap.Size];
                cubeMap.GetData(faceId, colorArray);

                throw new Exception("Opravit");
                // Extract the spherical harmonic for this face and accumulate it.
                //sh += ExtractSphericalHarmonicForCubeFace(cubeFaceMatrix, colorArray, cubeMap.Size, cubeMap.IsRgbm, cubeMap.RgbmMaxRange);
            }

            //average out over the sphere
            return sh.GetWeightedAverageLightInputFromSphere();
        }

        /// <summary>
        /// Use this function after accumulating multiple lights with the AddLight method.
        /// This method averages all the calls to AddLight() based on the accumulated weighting, assuming they were light input over a sphere.
        /// For example, if generating a spherical harmonic from a cube map, treat each pixel as a light source by calling AddLight(),
        /// then call this method to get the average for entire the sphere.
        /// </summary>
        private SphericalHarmonicL2 GetWeightedAverageLightInputFromSphere()
        {
            // Average out the entire spherical harmonic.
            // The 4 is because the SH lighting input is being sampled over a cosine weighted hemisphere.
            // The hemisphere halves the divider, the cosine weighting halves it again.
            if (Weighting > 0)
                return this * (4.0f / Weighting);
            return this;
        }

        private static SphericalHarmonicL2 ExtractSphericalHarmonicForCubeFace(Matrix faceTransform, Color[] colorDataRgb, int faceSize, bool isRgbm, float rgbmMaxRange)
        {
            SphericalHarmonicL2 sh = new SphericalHarmonicL2();

            // For each pixel in the face, generate it's SH contribution.
            // Treat each pixel in the cube as a light source, which gets added to the SH.
            // This is used to generate an indirect lighting SH for the scene.

            float directionStep = 2.0f / (faceSize - 1.0f);
            int pixelIndex = 0;

            float dirY = 1.0f;
            for (int y = 0; y < faceSize; y++)
            {
                SphericalHarmonicL2 lineSh = new SphericalHarmonicL2();
                float dirX = -1.0f;

                for (int x = 0; x < faceSize; x++)
                {
                    //the direction to the pixel in the cube
                    Vector3 direction = new Vector3(dirX, dirY, 1);
                    Vector3.TransformNormal(ref direction, ref faceTransform, out direction);

                    //length of the direction vector
                    float length = direction.Length();
                    //approximate area of the pixel (pixels close to the cube edges appear smaller when projected)
                    float weight = 1.0f / length;

                    //normalise:
                    direction.X *= weight;
                    direction.Y *= weight;
                    direction.Z *= weight;

                    Vector3 rgbFloat;

                    throw new Exception("Opravit");
                    //if (isRgbm)
                    //{
                    //    Color rgbm = colorDataRgb[pixelIndex++];
                    //    rgbFloat = RgbmHelper.RgbmGammaToFloatLinear(rgbm, rgbmMaxRange);
                    //}
                    //else
                    //{
                    //    Color rgb = colorDataRgb[pixelIndex++];
                    //    rgbFloat = new Vector3(GammaLinearSpaceHelper.GammaToLinear(rgb).X, GammaLinearSpaceHelper.GammaToLinear(rgb).Y, GammaLinearSpaceHelper.GammaToLinear(rgb).Z);
                    //}

                    //Add it to the SH
                    lineSh.AddLight(rgbFloat, direction, weight);

                    dirX += directionStep;
                }

                //average the SH
                if (lineSh.Weighting > 0)
                    lineSh *= 1 / lineSh.Weighting;

                // Add the line to the full SH
                // (SH is generated line by line to ease problems with floating point accuracy loss)
                sh += lineSh;

                dirY -= directionStep;
            }

            if (sh.Weighting > 0)
                sh *= 1 / sh.Weighting;

            return sh;
        }
    }
}

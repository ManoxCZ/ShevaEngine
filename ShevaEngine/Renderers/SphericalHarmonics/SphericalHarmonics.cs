﻿using Microsoft.Xna.Framework;

namespace ShevaEngine.Renderers.SphericalHarmonics
{
    /// <summary>
    /// Spherical Harmonics for RGB colors.
    /// A spherical harmonic approximates a spherical function using only a few values.
    /// They are great for store low frequency ambient colors and are very fast.
    /// </summary>
    public abstract class SphericalHarmonic
    {
        /// <summary>Accumulated weighting value. This value is provided as a helper to make averaging easier. It stores accumulated 
        /// weighting values from calls to AddLight. See AddLight method for further details.</summary>
        protected float Weighting;
        /// <summary>Coeficients.</summary>
        public virtual Vector3[] Coeficients { get { return null; } }

        /// <summary>
        /// Fill the constants with a color.
        /// </summary>
        public virtual void Fill(float red, float green, float blue)
        {
        }

        /// <summary>
        /// Add light from a given direction to the SH function. See the class remarks for further details on how an SH can be used to approximate lighting.
        /// Input light inputRGB will be multiplied by weight, and weight will be added to weighting.
        /// </summary>
        /// <param name="inputRgb">Input light intensity in RGB (usually gamma space) format for the given direction</param>
        /// <param name="direction">direction of the incoming light</param>
        /// <param name="weight">Weighting for this light, usually 1.0f. Use this value, and weighting if averaging a large number of lighting samples (eg, when converting a cube map to an SH)</param>
        public virtual void AddLight(Vector3 inputRgb, Vector3 direction, float weight = 1.0f)
        {
        }

        /// <summary>
        /// Sample the spherical harmonic in the given direction.
        /// </summary>
        public virtual Vector3 SampleDirection(Vector3 direction)
        {
            return Vector3.Zero;
        }
    }
}

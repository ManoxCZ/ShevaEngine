﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ShevaEngine.Renderers.SphericalHarmonics
{
    /// <summary>
    /// Spherical Harmonics for RGB colors.
    /// A spherical harmonic approximates a spherical function using only a few values.
    /// They are great to store low frequency ambient colors and are very fast.
    /// </summary>
    public class SphericalHarmonicL1 : SphericalHarmonic
    {
        /// <summary>Spherical Harmonic RGB coefficients.</summary>
        public override Vector3[] Coeficients => new[] { C0, C1, C2, C3 };

        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C0;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C1;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C2;
        /// <summary>Spherical Harmonic RGB coefficient.</summary>
        public Vector3 C3;

        /// <summary>
        /// Fill the constants with a color.
        /// </summary>
        public override void Fill(float red, float green, float blue)
        {
            C0 = new Vector3(red, green, blue);
            C1 = new Vector3(red, green, blue);
            C2 = new Vector3(red, green, blue);
            C3 = new Vector3(red, green, blue);
        }

        /// <summary>
        /// Add light from a given direction to the SH function. See the class remarks for further details on how an SH can be used to approximate lighting.
        /// Input light inputRGB will be multiplied by weight, and weight will be added to weighting.
        /// </summary>
        /// <param name="rgb">Input light intensity in RGB (usually gamma space) format for the given direction</param>
        /// <param name="direction">direction of the incoming light</param>
        /// <param name="weight">Weighting for this light, usually 1.0f. Use this value, and weighting if averaging a large number of lighting samples (eg, when converting a cube map to an SH)</param>
        public override void AddLight(Vector3 rgb, Vector3 direction, float weight = 1.0f)
        {
            direction = Vector3.Normalize(direction);

            float x = direction.X;
            float y = direction.Y;
            float z = direction.Z;

            float r = rgb.X * weight;
            float g = rgb.Y * weight;
            float b = rgb.Z * weight;

            // Spherical Harmonic constants
            const float f0 = 0.25f;
            const float f1 = 0.5f;

            // Axis constants for SH input
            float const1 = (f1 * (x));
            float const2 = (f1 * (y));
            float const3 = (f1 * (z));

            // Red channel
            C0.X += r * f0;
            C1.X += r * const1;
            C2.X += r * const2;
            C3.X += r * const3;
            // Green channel
            C0.Y += g * f0;
            C1.Y += g * const1;
            C2.Y += g * const2;
            C3.Y += g * const3;
            // Blue channel
            C0.Z += b * f0;
            C1.Z += b * const1;
            C2.Z += b * const2;
            C3.Z += b * const3;

            // Store the accumulated weighting, useful if averaging the light input is desired.
            Weighting += weight;
        } // AddLight

        /// <summary>
        /// Sample the spherical harmonic in the given direction.
        /// </summary>
        public override Vector3 SampleDirection(Vector3 direction)
        {
            direction = Vector3.Normalize(direction);
            float x = direction.X;
            float y = direction.Y;
            float z = direction.Z;

            float r = C0.X + C1.X * x + C2.X * y + C3.X * z;
            float g = C0.Y + C1.Y * x + C2.Y * y + C3.Y * z;
            float b = C0.Z + C1.Z * x + C2.Z * y + C3.Z * z;

            return new Vector3(r, g, b);
        }

        /// <summary>
        /// Generate a spherical harmonic from the faces of a cubemap, treating each pixel as a light source and averaging the result.
        /// </summary>
        public static SphericalHarmonicL1 GenerateSphericalHarmonicFromCubeMap(TextureCube cubeMap)
        {
            SphericalHarmonicL1 sh = new SphericalHarmonicL1();

            // Extract the 6 faces of the cubemap.
            for (int face = 0; face < 6; face++)
            {
                CubeMapFace faceId = (CubeMapFace)face;

                // Get the transformation for this face,
                switch (faceId)
                {
                    case CubeMapFace.PositiveX:
                        Matrix.CreateLookAt(Vector3.Zero, new Vector3(1, 0, 0), new Vector3(0, 1, 0));
                        break;
                    case CubeMapFace.NegativeX:
                        Matrix.CreateLookAt(Vector3.Zero, new Vector3(-1, 0, 0), new Vector3(0, 1, 0));
                        break;
                    case CubeMapFace.PositiveY:
                        Matrix.CreateLookAt(Vector3.Zero, new Vector3(0, 1, 0), new Vector3(0, 0, 1));
                        break;
                    case CubeMapFace.NegativeY:
                        Matrix.CreateLookAt(Vector3.Zero, new Vector3(0, -1, 0), new Vector3(0, 0, -1));
                        break;
                    case CubeMapFace.PositiveZ:
                        Matrix.CreateLookAt(Vector3.Zero, new Vector3(0, 0, -1), new Vector3(0, 1, 0));
                        break;
                    case CubeMapFace.NegativeZ:
                        Matrix.CreateLookAt(Vector3.Zero, new Vector3(0, 0, 1), new Vector3(0, 1, 0));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                Color[] colorArray = new Color[cubeMap.Size * cubeMap.Size];
                cubeMap.GetData<Color>(faceId, colorArray);

                // Extract the spherical harmonic for this face and accumulate it.
                throw new Exception("Opravit");
                //sh += ExtractSphericalHarmonicForCubeFace(cubeFaceMatrix, colorArray, cubeMap.Size, cubeMap.IsRgbm, cubeMap.RgbmMaxRange);
            }

            //average out over the sphere
            return sh.GetWeightedAverageLightInputFromSphere();
        }

        /// <summary>
        /// Use this function after accumulating multiple lights with the AddLight method.
        /// This method averages all the calls to AddLight() based on the accumulated weighting, assuming they were light input over a sphere.
        /// For example, if generating a spherical harmonic from a cube map, treat each pixel as a light source by calling AddLight(),
        /// then call this method to get the average for entire the sphere.
        /// </summary>
        private SphericalHarmonicL1 GetWeightedAverageLightInputFromSphere()
        {
            // Average out the entire spherical harmonic.
            // The 4 is because the SH lighting input is being sampled over a cosine weighted hemisphere.
            // The hemisphere halves the divider, the cosine weighting halves it again.
            if (Weighting > 0)
                return this * (4.0f / Weighting);
            return this;
        }

        private static SphericalHarmonicL1 ExtractSphericalHarmonicForCubeFace(Matrix faceTransform, Color[] colorDataRgb, int faceSize, bool isRgbm, float rgbmMaxRange)
        {
            SphericalHarmonicL1 sh = new SphericalHarmonicL1();

            // For each pixel in the face, generate it's SH contribution.
            // Treat each pixel in the cube as a light source, which gets added to the SH.
            // This is used to generate an indirect lighting SH for the scene.

            float directionStep = 2.0f / (faceSize - 1.0f);

            float dirY = 1.0f;
            for (int y = 0; y < faceSize; y++)
            {
                SphericalHarmonicL1 lineSh = new SphericalHarmonicL1();
                float dirX = -1.0f;

                for (int x = 0; x < faceSize; x++)
                {
                    //the direction to the pixel in the cube
                    Vector3 direction = new Vector3(dirX, dirY, 1);
                    Vector3.TransformNormal(ref direction, ref faceTransform, out direction);

                    //length of the direction vector
                    float length = direction.Length();
                    //approximate area of the pixel (pixels close to the cube edges appear smaller when projected)
                    float weight = 1.0f / length;

                    //normalise:
                    direction.X *= weight;
                    direction.Y *= weight;
                    direction.Z *= weight;

                    Vector3 rgbFloat;

                    throw new Exception("Opravit");
                    /*
                    if (isRgbm)
                    {
                        Color rgbm = colorDataRgb[pixelIndex++];
                        rgbFloat = RgbmHelper.RgbmGammaToFloatLinear(rgbm, rgbmMaxRange);
                    }
                    else
                    {
                        Color rgb = colorDataRgb[pixelIndex++];
                        rgbFloat = new Vector3(GammaLinearSpaceHelper.GammaToLinear(rgb).X, GammaLinearSpaceHelper.GammaToLinear(rgb).Y, GammaLinearSpaceHelper.GammaToLinear(rgb).Z);
                    }*/

                    //Add it to the SH
                    lineSh.AddLight(rgbFloat, direction, weight);

                    dirX += directionStep;
                }

                //average the SH
                if (lineSh.Weighting > 0)
                    lineSh *= 1 / lineSh.Weighting;

                // Add the line to the full SH
                // (SH is generated line by line to ease problems with floating point accuracy loss)
                sh += lineSh;

                dirY -= directionStep;
            }

            if (sh.Weighting > 0)
                sh *= 1 / sh.Weighting;

            return sh;
        }

        /// <summary>
        /// Add two spherical harmonics together.
        /// </summary>
        public static SphericalHarmonicL1 Add(SphericalHarmonicL1 x, SphericalHarmonicL1 y)
        {
            return new SphericalHarmonicL1
            {
                Weighting = x.Weighting + y.Weighting,
                C0 = { X = x.C0.X + y.C0.X, Y = x.C0.Y + y.C0.Y, Z = x.C0.Z + y.C0.Z },
                C1 = { X = x.C1.X + y.C1.X, Y = x.C1.Y + y.C1.Y, Z = x.C1.Z + y.C1.Z },
                C2 = { X = x.C2.X + y.C2.X, Y = x.C2.Y + y.C2.Y, Z = x.C2.Z + y.C2.Z },
                C3 = { X = x.C3.X + y.C3.X, Y = x.C3.Y + y.C3.Y, Z = x.C3.Z + y.C3.Z }
            };
        }

        /// <summary>
        /// Add two spherical harmonics together
        /// </summary>
        public static SphericalHarmonicL1 operator +(SphericalHarmonicL1 x, SphericalHarmonicL1 y)
        {
            return new SphericalHarmonicL1
            {
                Weighting = x.Weighting + y.Weighting,
                C0 = { X = x.C0.X + y.C0.X, Y = x.C0.Y + y.C0.Y, Z = x.C0.Z + y.C0.Z },
                C1 = { X = x.C1.X + y.C1.X, Y = x.C1.Y + y.C1.Y, Z = x.C1.Z + y.C1.Z },
                C2 = { X = x.C2.X + y.C2.X, Y = x.C2.Y + y.C2.Y, Z = x.C2.Z + y.C2.Z },
                C3 = { X = x.C3.X + y.C3.X, Y = x.C3.Y + y.C3.Y, Z = x.C3.Z + y.C3.Z }
            };
        }

        /// <summary>
        /// Multiply a spherical harmonic by a constant scale factor
        /// </summary>
        public static SphericalHarmonicL1 Multiply(SphericalHarmonicL1 x, float scale)
        {
            return new SphericalHarmonicL1
            {
                Weighting = x.Weighting * scale,
                C0 = { X = x.C0.X * scale, Y = x.C0.Y * scale, Z = x.C0.Z * scale },
                C1 = { X = x.C1.X * scale, Y = x.C1.Y * scale, Z = x.C1.Z * scale },
                C2 = { X = x.C2.X * scale, Y = x.C2.Y * scale, Z = x.C2.Z * scale },
                C3 = { X = x.C3.X * scale, Y = x.C3.Y * scale, Z = x.C3.Z * scale }
            };
        } // Multiply

        /// <summary>
        /// Multiply a spherical harmonic by a constant scale factor.
        /// </summary>
        public static SphericalHarmonicL1 operator *(SphericalHarmonicL1 x, float scale)
        {
            return new SphericalHarmonicL1
            {
                Weighting = x.Weighting * scale,
                C0 = { X = x.C0.X * scale, Y = x.C0.Y * scale, Z = x.C0.Z * scale },
                C1 = { X = x.C1.X * scale, Y = x.C1.Y * scale, Z = x.C1.Z * scale },
                C2 = { X = x.C2.X * scale, Y = x.C2.Y * scale, Z = x.C2.Z * scale },
                C3 = { X = x.C3.X * scale, Y = x.C3.Y * scale, Z = x.C3.Z * scale }
            };
        } // Multiply

        /// <summary>
        /// Linear interpolate (Lerp) between two spherical harmonics based on a interpolation factor.
        /// </summary>
        /// <param name="factor">Determines the interpolation point. When factor is 1.0, the output will be x, when factor is 0.0, the output will be y</param>
        public static SphericalHarmonicL1 Lerp(SphericalHarmonicL1 x, SphericalHarmonicL1 y, float factor)
        {
            float xs = factor;
            float ys = 1.0f - factor;
            return new SphericalHarmonicL1
            {
                Weighting = x.Weighting * xs + y.Weighting * ys,
                C0 = { X = xs * x.C0.X + ys * y.C0.X, Y = xs * x.C0.Y + ys * y.C0.Y, Z = xs * x.C0.Z + ys * y.C0.Z },
                C1 = { X = xs * x.C1.X + ys * y.C1.X, Y = xs * x.C1.Y + ys * y.C1.Y, Z = xs * x.C1.Z + ys * y.C1.Z },
                C2 = { X = xs * x.C2.X + ys * y.C2.X, Y = xs * x.C2.Y + ys * y.C2.Y, Z = xs * x.C2.Z + ys * y.C2.Z },
                C3 = { X = xs * x.C3.X + ys * y.C3.X, Y = xs * x.C3.Y + ys * y.C3.Y, Z = xs * x.C3.Z + ys * y.C3.Z },
            };
        } // Lerp
    }
}

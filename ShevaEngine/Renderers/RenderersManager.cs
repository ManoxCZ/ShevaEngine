﻿using ShevaEngine.Renderers.Deferred;
using System.Collections.Generic;

namespace ShevaEngine.Renderers
{
    /// <summary>
    /// Renderers manager.
    /// </summary>
    public class RenderersManager
    {
        private readonly ShevaGame _game;
        private readonly SortedDictionary<string, Renderer> _renderers;
        public Renderer this[string type] => _renderers[type];

        /// <summary>
        /// Constructor.
        /// </summary>
        public RenderersManager(ShevaGame game)
        {
            _game = game;

            _renderers = new SortedDictionary<string, Renderer>();
            
            CreateRenderers();
        }

        /// <summary>
        /// Create renderers.
        /// </summary>
        private void CreateRenderers()
        {
            _renderers.Add("Deferred", new DeferredRenderer(_game));
        }
    }
}

﻿using System.Collections.Generic;

namespace ShevaEngine.Renderers.RenderingPipelines
{
    /// <summary>
    /// Rendering pipeline switch node.
    /// </summary>
    internal class RenderingPipelineSwitchNode<T> : RenderingPipelineNode
    {
        public SortedDictionary<T, RenderingPipelineNode> Children { get; set; }
        

        /// <summary>
        /// Constructor.
        /// </summary>
        public RenderingPipelineSwitchNode()
        {
            Children = new SortedDictionary<T, RenderingPipelineNode>();
        }

        /// <summary>
        /// Clear method.
        /// </summary>
        public override void Clear()
        {
            foreach (KeyValuePair<T, RenderingPipelineNode> child in Children)
            {
                child.Value.Enabled = false;
            }
        }

        /// <summary>
        /// Get child.
        /// </summary>
        public virtual TU GetChild<TU>(T index) where TU : RenderingPipelineNode, new()
        {
            if (!Children.ContainsKey(index))
            {
                Children.Add(index, new TU());
            }

            return Children[index] as TU;
        }
    }
}

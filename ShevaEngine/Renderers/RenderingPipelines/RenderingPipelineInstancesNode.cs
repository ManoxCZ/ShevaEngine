﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace ShevaEngine.Renderers.RenderingPipelines
{
    /// <summary>
    /// Rendering pipeline instances node.
    /// </summary>
    internal class RenderingPipelineInstancesNode : RenderingPipelineNode
    {
        /// <summary>Matrices.</summary>
        public List<Matrix> Matrices { get; set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        public RenderingPipelineInstancesNode()
        {
            Matrices = new List<Matrix>();
        }

        /// <summary>
        /// Clear method.
        /// </summary>
        public override void Clear()
        {
            Matrices.Clear();
        }
    }
}

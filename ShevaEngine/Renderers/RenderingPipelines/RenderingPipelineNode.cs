﻿namespace ShevaEngine.Renderers.RenderingPipelines
{
    /// <summary>
    /// Rendering pipeline node.
    /// </summary>
    internal abstract class RenderingPipelineNode
    {
        public bool Enabled { get; set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        protected RenderingPipelineNode()
        {
            Enabled = true;
        }

        /// <summary>
        /// Clear node.
        /// </summary>
        public abstract void Clear();        
    }
}

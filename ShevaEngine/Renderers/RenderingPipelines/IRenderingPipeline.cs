﻿using Microsoft.Xna.Framework;
using ShevaEngine.Graphics.Materials;
using ShevaEngine.Graphics.Meshes;
using System;

namespace ShevaEngine.Renderers.RenderingPipelines
{
    /// <summary>
    /// Interface for rendering pipeline.
    /// </summary>
    public interface IRenderingPipeline
    {
        /// <summary>
        /// Clear.
        /// </summary>
        void Clear();

        /// <summary>
        /// Add mesh.
        /// </summary>
        void AddMesh(UInt16 layer, Mesh mesh, GraphicsMaterial material, Matrix worldMatrix);
    }
}

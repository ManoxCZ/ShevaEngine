﻿//using Microsoft.Xna.Framework;
//using ShevaEngine.Graphics.DrawCalls;
//using System;
//using System.Collections.Generic;

//namespace ShevaEngine.Renderers.RenderingPipelines
//{
//    /// <summary>
//    /// Rendering pipeline.
//    /// </summary>
//    public class RenderingPipeline
//    {
//        private readonly RenderingPipelineSwitchNode<UInt16> _rootNode;


//        /// <summary>
//        /// Constructor.
//        /// </summary>
//        public RenderingPipeline()
//        {
//            _rootNode = new RenderingPipelineSwitchNode<UInt16>();
//        }

//        /// <summary>
//        /// Add draw call.
//        /// </summary>
//        public void AddDrawCall(DrawCallKey drawCallKey, Matrix worldMatrix)
//        {
//            AddDrawCallLayer(drawCallKey, worldMatrix, _rootNode);
//        }

//        /// <summary>
//        /// Add drawcall layer.
//        /// </summary>
//        private void AddDrawCallLayer(DrawCallKey drawCallKey, Matrix worldMatrix, RenderingPipelineSwitchNode<UInt16> node)
//        {
//            RenderingPipelineSwitchNode<UInt16> childNode = node.GetChild<RenderingPipelineSwitchNode<UInt16>>(drawCallKey.LayerId);

//            AddDrawCallMaterial(drawCallKey, worldMatrix, childNode);
//        }

//        /// <summary>
//        /// Add drawcall material.
//        /// </summary>
//        private void AddDrawCallMaterial(DrawCallKey drawCallKey, Matrix worldMatrix, RenderingPipelineSwitchNode<UInt16> node)
//        {
//            RenderingPipelineSwitchNode<UInt16> childNode = node.GetChild<RenderingPipelineSwitchNode<UInt16>>(drawCallKey.MaterialId);

//            AddDrawCallMaterialParameters(drawCallKey, worldMatrix, childNode);
//        }

//        /// <summary>
//        /// Add drawcall material parameters.
//        /// </summary>
//        private void AddDrawCallMaterialParameters(DrawCallKey drawCallKey, Matrix worldMatrix, RenderingPipelineSwitchNode<UInt16> node)
//        {
//            RenderingPipelineSwitchNode<UInt16> childNode = node.GetChild<RenderingPipelineSwitchNode<UInt16>>(drawCallKey.MaterialParamsId);

//            AddDrawCallInstance(drawCallKey, worldMatrix, childNode);
//        }

//        /// <summary>
//        /// Add drawcall instance.
//        /// </summary>
//        private void AddDrawCallInstance(DrawCallKey drawCallKey, Matrix worldMatrix, RenderingPipelineSwitchNode<UInt16> node)
//        {
//            RenderingPipelineInstancesNode childNode = node.GetChild<RenderingPipelineInstancesNode>(drawCallKey.DrawCallId);

//            childNode.Matrices.Add(worldMatrix);
//        }

//        /// <summary>
//        /// Render.
//        /// </summary>
//        public void Render(RenderingContext context)
//        {
//            foreach (KeyValuePair<UInt16, RenderingPipelineNode> layerNode in _rootNode.Children)
//            {
//                RenderLayer(context, layerNode.Value as RenderingPipelineSwitchNode<UInt16>);    
//            }
//        }

//        /// <summary>
//        /// Render layer.
//        /// </summary>
//        private void RenderLayer(RenderingContext context, RenderingPipelineSwitchNode<UInt16> layerNode)
//        {
//            foreach (KeyValuePair<UInt16, RenderingPipelineNode> materialNode in layerNode.Children)
//            {
//                RenderMaterial(context, materialNode.Key, materialNode.Value as RenderingPipelineSwitchNode<UInt16>);
//            }
//        }

//        /// <summary>
//        /// Render material.
//        /// </summary>
//        private void RenderMaterial(RenderingContext context, UInt16 materialId, RenderingPipelineSwitchNode<UInt16> materialNode)
//        {
//            context.SetMaterial(materialId);

//            foreach (KeyValuePair<UInt16, RenderingPipelineNode> materialParamsNode in materialNode.Children)
//            {
//                RenderMaterialParams(context, materialParamsNode.Value as RenderingPipelineSwitchNode<UInt16>);
//            }
//        }

//        /// <summary>
//        /// Render material.
//        /// </summary>
//        private void RenderMaterialParams(RenderingContext context, RenderingPipelineSwitchNode<UInt16> materialParamsNode)
//        {
//            foreach (KeyValuePair<UInt16, RenderingPipelineNode> instancesNode in materialParamsNode.Children)
//            {
//                RenderInstances(context, instancesNode.Key, instancesNode.Value as RenderingPipelineInstancesNode);
//            }
//        }

//        /// <summary>
//        /// Render material.
//        /// </summary>
//        private void RenderInstances(RenderingContext context, UInt16 meshId, RenderingPipelineInstancesNode instancesNode)
//        {
//            context.DrawMeshInstances(meshId, instancesNode.Matrices);
//        }
//    }
//}

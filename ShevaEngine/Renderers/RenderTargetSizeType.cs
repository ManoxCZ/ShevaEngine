﻿namespace ShevaEngine.Renderers
{
    /// <summary>
    /// Posible size types for creating a RenderTarget texture.
    /// </summary>
    public enum RenderTargetSizeType
    {
        /// <summary>Uses the full screen size.</summary>
        FullScreen,
        /// <summary>Uses half the full screen size, e.g. 800x600 becomes 400x300</summary>
        HalfScreen,
        /// <summary>Uses a quarter of the full screen size, e.g. 800x600 becomes 200x150</summary>
        QuarterScreen,
        /// <summary>256 x 256 pixels. Good for shadows.</summary>
        Square256X256,
        /// <summary>512 x 512 pixels. Good for shadows.</summary>
        Square512X512,
        /// <summary>1024 x 1024 pixels. Good for shadows.</summary>
        Square1024X1024,
    }
}

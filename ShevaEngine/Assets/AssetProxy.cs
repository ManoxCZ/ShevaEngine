﻿using System;

namespace ShevaEngine.Assets
{
    /// <summary>
    /// Asset proxy.
    /// </summary>    
    internal sealed class AssetProxy : Asset
    {
        public AssetProxy(Guid id)
            : base()
        {
            Id = id;
        }
    }
}

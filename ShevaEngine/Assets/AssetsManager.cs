﻿using ShevaEngine.Serialization;
using System;
using System.Collections.Generic;
using System.IO;

namespace ShevaEngine.Assets
{
    /// <summary>
    /// Assets manager.
    /// </summary>
    public class AssetsManager
    {
        private readonly ShevaGame _game;
        private readonly SortedDictionary<string, Guid> _pathToGuid;
        private readonly SortedDictionary<Guid, Asset> _assets;


        /// <summary>
        /// Constructor.
        /// </summary>
        public AssetsManager(ShevaGame game)
        {
            _game = game;

            _pathToGuid = new SortedDictionary<string, Guid>();
            _assets = new SortedDictionary<Guid, Asset>();

            if (Directory.Exists("Assets"))
                foreach (string filename in Directory.EnumerateFiles("Assets", "*", SearchOption.AllDirectories))
                    _pathToGuid.Add(filename, Guid.Empty);
        }

        /// <summary>
        /// Get asset.
        /// </summary>
        public T Get<T>(string path) where T : Asset, new()
        {
            lock (_assets)
            {
                lock (_pathToGuid)
                {
                    if (_pathToGuid.ContainsKey(path))
                    {
                        if (_pathToGuid[path] == Guid.Empty)
                        {
                            T newAsset = ShevaSerializer.Deserialize<T>(_game, path);

                            if (newAsset == null)
                                return null;

                            _pathToGuid[path] = newAsset.Id;
                            _assets[newAsset.Id] = newAsset;

                            return newAsset;
                        }

                        return _assets[_pathToGuid[path]] as T;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Get asset.
        /// </summary>
        public T Get<T>(Guid id) where T : Asset
        {
            lock (_assets)
            {
                if (!_assets.ContainsKey(id))
                    return null;

                return _assets[id] as T;
            }
        }

        /// <summary>
        /// Add asset.
        /// </summary>
        public void Add(Asset asset)
        {
            lock (_assets)
            {
                if (_assets.ContainsKey(asset.Id))
                    return;

                _assets.Add(asset.Id, asset);
            }
        }

        /// <summary>
        /// Contains asset.
        /// </summary>
        public bool Contains(Asset asset)
        {
            lock (_assets)
            {
                return Contains(asset.Id);
            }
        }
        /// <summary>
        /// Contains asset.
        /// </summary>
        public bool Contains(Guid id)
        {
            lock (_assets)
            {
                return _assets.ContainsKey(id);
            }
        }
    }
}

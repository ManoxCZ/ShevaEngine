﻿using System;
using System.Runtime.Serialization;

namespace ShevaEngine.Assets
{
    /// <summary>
    /// Asset class.
    /// </summary>   
    [DataContract] 
    public class Asset
    {
        [DataMember]
        public Guid Id { get; set; }


        /// <summary>
        /// Constructor.
        /// </summary>
        public Asset()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Initialize.
        /// </summary>
        public virtual void Initialize(ShevaGame game)
        {
            
        }
    }
}

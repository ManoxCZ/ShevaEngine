﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Config;

namespace ShevaEngine.Logging
{
    /// <summary>
    /// Log manager.
    /// </summary>
    public class LogManager
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public LogManager()
        {
            XmlConfigurator.Configure(new System.IO.FileInfo("log.config"));
        }
    }
}

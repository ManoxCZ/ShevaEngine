﻿using Microsoft.Xna.Framework;

namespace ShevaEngine.Input
{
    /// <summary>
    /// Input manager.
    /// </summary>
    public class InputManager
    {
        public const int GamepadsCount = 4;

        public ShevaKeyboard Keyboard { get; }
        public ShevaMouse Mouse { get; }
        public ShevaGamepad[] Gamepads { get; }

        /// <summary>
        /// Constructor.
        /// </summary>        
        public InputManager()
        {
            Keyboard = new ShevaKeyboard();
            Mouse = new ShevaMouse();
            Gamepads = new ShevaGamepad[GamepadsCount];

            for (int i = 0; i < GamepadsCount; i++)
                Gamepads[i] = new ShevaGamepad((PlayerIndex)i);
        }

        /// <summary>
        /// Update().
        /// </summary>
        public void Update(GameTime gameTime)
        {
            Keyboard.Update(gameTime);
            Mouse.Update(gameTime);
            
            for (int i = 3; i >= 0; i--)
                Gamepads[i].Update(gameTime);
        }
    }
}

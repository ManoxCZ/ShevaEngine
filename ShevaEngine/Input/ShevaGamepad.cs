﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ShevaEngine.Input
{
    /// <summary>
    /// Gamepad class.
    /// </summary>
    public class ShevaGamepad 
    {
        private readonly PlayerIndex _playerIndex;
        private GamePadState _previousState;

        public event OnButtonDelegate OnButtonClick;
        public event OnButtonDelegate OnButtonDown;
        public event OnButtonDelegate OnButtonUp;

        public bool IsConnected => _previousState.IsConnected;
        public Vector2 LeftThumbStick => _previousState.ThumbSticks.Left;
        public Vector2 RightThumbStick => _previousState.ThumbSticks.Right;


        /// <summary>
        /// Constructor.
        /// </summary>
        public ShevaGamepad(PlayerIndex index)
        {
            _playerIndex = index;
            _previousState = GamePad.GetState(_playerIndex);
        }

        
        /// <summary>
        /// Method update.
        /// </summary>        
        public void Update(GameTime time)
        {
            GamePadState state = GamePad.GetState(_playerIndex);

            UpdateButton(state, Buttons.DPadUp);
            UpdateButton(state, Buttons.DPadDown);
            UpdateButton(state, Buttons.DPadLeft);
            UpdateButton(state, Buttons.DPadRight);
            UpdateButton(state, Buttons.Start);
            UpdateButton(state, Buttons.Back);
            UpdateButton(state, Buttons.LeftStick);
            UpdateButton(state, Buttons.RightStick);
            UpdateButton(state, Buttons.LeftShoulder);
            UpdateButton(state, Buttons.RightShoulder);
            UpdateButton(state, Buttons.BigButton);
            UpdateButton(state, Buttons.A);
            UpdateButton(state, Buttons.B);
            UpdateButton(state, Buttons.X);
            UpdateButton(state, Buttons.Y);
            UpdateButton(state, Buttons.LeftThumbstickLeft);
            UpdateButton(state, Buttons.RightTrigger);
            UpdateButton(state, Buttons.LeftTrigger);
            UpdateButton(state, Buttons.RightThumbstickUp);
            UpdateButton(state, Buttons.RightThumbstickDown);
            UpdateButton(state, Buttons.RightThumbstickRight);
            UpdateButton(state, Buttons.RightThumbstickLeft);
            UpdateButton(state, Buttons.LeftThumbstickUp);
            UpdateButton(state, Buttons.LeftThumbstickDown);
            UpdateButton(state, Buttons.LeftThumbstickRight);

            _previousState = state;
        }

        /// <summary>
        /// Metoda updates button.
        /// </summary>
        private void UpdateButton(GamePadState state, Buttons button)
        {
            if (_previousState.IsButtonDown(button) && state.IsButtonUp(button))
            {
                OnButtonUp?.Invoke(this, button);
            }
            else
                if (_previousState.IsButtonUp(button) && state.IsButtonDown(button))
                {
                    OnButtonClick?.Invoke(this, button);

                    OnButtonDown?.Invoke(this, button);
                }            
        }

        /// <summary>
        /// Method checks button.
        /// </summary>
        public void CheckButton(Buttons button, ButtonState previousState, ButtonState newState)
        {

        }
    }

    public delegate void OnButtonDelegate(object sender, Buttons button);
}

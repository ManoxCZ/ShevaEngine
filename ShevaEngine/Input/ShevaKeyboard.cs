using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace ShevaEngine.Input
{
    /// <summary>
    /// Keyboard class.
    /// </summary>
    public class ShevaKeyboard
    {
        private KeyboardState _previousState;

        public event OnKeyEventDelegate OnKeyDown;
        public event OnKeyEventDelegate OnKeyUp;
        public event OnKeyEventDelegate OnKeyPressed;
        public event OnCharEventDelegate OnCharPressed;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public ShevaKeyboard()
        {
            _previousState = Keyboard.GetState();
        }

        /// <summary>
        /// Method Update().
        /// </summary>
        public void Update(GameTime time)
        {
            KeyboardState newState = Keyboard.GetState();            

            HashSet<Keys> previousPressedKeys = new HashSet<Keys>(_previousState.GetPressedKeys());
            HashSet<Keys> newPressedKeys = new HashSet<Keys>(newState.GetPressedKeys());
            
            foreach (Keys key in newPressedKeys)
            {
                if (!previousPressedKeys.Contains(key))                
                {
                    OnKeyDown?.Invoke(this, key);

                    previousPressedKeys.Remove(key);
                }
            }

            foreach (Keys key in previousPressedKeys)
            {
                if (!newPressedKeys.Contains(key))
                {
                    OnKeyUp?.Invoke(this, key);

                    OnKeyPressed?.Invoke(this, key);

                    if ((Keys.A <= key && key <= Keys.Z) ||
                        (Keys.NumPad0 <= key && key <= Keys.NumPad9) ||
                        (Keys.D0 <= key && key <= Keys.D9))
                    {
                        char character = (char)key;

                        if (!newState.IsKeyDown(Keys.LeftShift))
                            character = Char.ToLower(character);

                        OnCharPressed?.Invoke(this, character);
                    }
                }
            }

            _previousState = newState;                       
        }        

        /// <summary>
        /// Method IsKeyDown().
        /// </summary>
        public bool IsKeyDown(Keys key)
        {
            return _previousState.IsKeyDown(key);
        }
    }

    public delegate void OnKeyEventDelegate(ShevaKeyboard keyboard, Keys e);

    public delegate void OnCharEventDelegate(ShevaKeyboard keyboard, char value);
}

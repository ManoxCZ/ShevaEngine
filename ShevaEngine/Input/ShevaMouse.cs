﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ShevaEngine.Input
{
    /// <summary>
    /// Sheva mouse.
    /// </summary>
    public class ShevaMouse 
    {
        private MouseState _previousState;
        
        public event OnMouseButtonDelegate OnMouseButtonClick;
        public event OnMouseButtonDelegate OnMouseButtonDown;
        public event OnMouseButtonDelegate OnMouseButtonUp;
        public event OnMouseMoveDelegate OnMouseMove;
        public event OnMouseButtonDelegate OnMouseButtonHold;
        private readonly GameTime[] _downButtonTimes;
        public float ButtonDelay = 1;

        public ButtonState LeftButton => _previousState.LeftButton;
        public ButtonState RightButton => _previousState.RightButton;
        public ButtonState MiddleButton => _previousState.MiddleButton;
        public Point Position => _previousState.Position;
        public int WheelDelta => _previousState.ScrollWheelValue;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public ShevaMouse()
        {
            _previousState = Mouse.GetState();
            _downButtonTimes = new[] { new GameTime(), new GameTime(), new GameTime() };
        }

        /// <summary>
        /// Metoda update.
        /// </summary>        
        public void Update(GameTime time)
        {
            MouseState actualState = Mouse.GetState();

            if (_previousState.X != actualState.X || _previousState.Y != actualState.Y)
                OnMouseMove?.Invoke(this, new Vector2(actualState.X, actualState.Y));

            if (actualState.LeftButton != _previousState.LeftButton)
            {
                if (actualState.LeftButton == ButtonState.Pressed)
                {
                    OnMouseButtonDown?.Invoke(this, MouseButton.Left);

                    _downButtonTimes[0] = time;
                }
                else
                {
                    OnMouseButtonUp?.Invoke(this, MouseButton.Left);

                    OnMouseButtonClick?.Invoke(this, MouseButton.Left);
                }
            }
            else
                if (actualState.LeftButton == ButtonState.Pressed &&
                    time.ElapsedGameTime.Seconds - _downButtonTimes[0].ElapsedGameTime.Seconds > ButtonDelay)
                    OnMouseButtonHold?.Invoke(this, MouseButton.Left);                

            if (actualState.RightButton != _previousState.RightButton)
            {
                if (actualState.RightButton == ButtonState.Pressed)
                    OnMouseButtonDown?.Invoke(this, MouseButton.Right);
                else
                {
                    OnMouseButtonUp?.Invoke(this, MouseButton.Right);

                    OnMouseButtonClick?.Invoke(this, MouseButton.Right);
                }
            }
            else
                if (actualState.RightButton == ButtonState.Pressed)
                    OnMouseButtonHold?.Invoke(this, MouseButton.Right);
                

            if (actualState.MiddleButton != _previousState.MiddleButton)
            {
                if (actualState.MiddleButton == ButtonState.Pressed)
                    OnMouseButtonDown?.Invoke(this, MouseButton.Middle);
                else
                {
                    OnMouseButtonUp?.Invoke(this, MouseButton.Middle);

                    OnMouseButtonClick?.Invoke(this, MouseButton.Middle);
                }
            }
            else
                if (actualState.MiddleButton == ButtonState.Pressed)
                    OnMouseButtonHold?.Invoke(this, MouseButton.Middle);
                
            _previousState = actualState;
        }        
    }

    public delegate void OnMouseButtonDelegate(object sender, MouseButton button);

    public delegate void OnMouseMoveDelegate(object sender, Vector2 position);

    public enum MouseButton : byte
    {
        Left = 0, 
        Right = 1,
        Middle = 2
    }    
}

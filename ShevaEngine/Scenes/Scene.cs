﻿using Microsoft.Xna.Framework;
using ShevaEngine.Assets;
using ShevaEngine.Entities;
using ShevaEngine.Entities.Components;
using ShevaEngine.Renderers.RenderingPipelines;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ShevaEngine.Scenes
{
    /// <summary>
    /// Scene tree.
    /// </summary>
    public class Scene : Asset
    {
        private ShevaGame _game;
        [DataMember]
        private readonly List<Entity> _entities;
        private readonly List<Entity> _toAddEntities;        
        private readonly List<Entity> _toRemoveEntities;        
        private readonly List<IUpdatableComponent> _updatableComponents;
        public List<IOnGUIComponent> OnGuiComponents { get; }
        public List<CameraComponent> CameraComponents { get; }
        

        /// <summary>
        /// Constructor.
        /// </summary>
        public Scene()
        {
            _entities = new List<Entity>();            
            _toAddEntities = new List<Entity>();
            _toRemoveEntities = new List<Entity>();
            _updatableComponents = new List<IUpdatableComponent>();
            OnGuiComponents = new List<IOnGUIComponent>();
            CameraComponents = new List<CameraComponent>();    
        }
        
        /// <summary>
        /// Start.
        /// </summary>
        public void Start(ShevaGame game)
        {
            _game = game;
            
            lock (_entities)
                foreach (Entity entity in _entities)
                    RegisterEntity(entity);                
        }

        /// <summary>
        /// Update.
        /// </summary>
        public void Update(GameTime gameTime)
        {
            lock (_updatableComponents)
                foreach (IUpdatableComponent updatableComponent in _updatableComponents)
                {
                    ActivableComponent activableComponent = updatableComponent as ActivableComponent;

                    if (activableComponent == null || activableComponent.IsActive)
                        updatableComponent.Update(gameTime);
                }

            UpdateEntities();
        }

        /// <summary>
        /// Update entities.
        /// </summary>
        private void UpdateEntities()
        {
            lock (_toAddEntities)
            {
                foreach (Entity entity in _toAddEntities)
                    AddEntityImmediate(entity);

                _toAddEntities.Clear();
            }

            lock (_toRemoveEntities)
            {
                foreach (Entity entity in _toRemoveEntities)
                    RemoveEntityImmediate(entity);

                _toRemoveEntities.Clear();
            }
        }        

        /// <summary>
        /// Get entities of type.
        /// </summary>
        public IEnumerable<T> GetComponentsOfType<T>() where T : EntityComponent
        {
            lock (_entities)
                foreach (Entity entity in _entities)
                {
                    T component = entity.GetComponent<T>();

                    if (component != null)
                        yield return component;
                }
        }

        /// <summary>
        /// Add entity.
        /// </summary>        
        public void AddEntity(Entity entity)
        {
            lock (_toAddEntities)
                _toAddEntities.Add(entity);
        }

        /// <summary>
        /// Remove entity.
        /// </summary>        
        public void RemoveEntity(Entity entity)
        {
            lock (_toRemoveEntities)
                _toRemoveEntities.Add(entity);
        }

        /// <summary>
        /// Add entity immediate.
        /// </summary>        
        public void AddEntityImmediate(Entity entity)
        {
            lock (_entities)
                _entities.Add(entity);

            //AddInAssets(entity);

            RegisterEntity(entity);
        }
        
        /// <summary>
        /// Remove entity.
        /// </summary>        
        public void RemoveEntityImmediate(Entity entity)
        {
            lock (_entities)
                _entities.Remove(entity);

            UnregisterEntity(entity);
        }

        /// <summary>
        /// Register entity.
        /// </summary>
        private void RegisterEntity(Entity entity)
        {
            entity.Start(_game);

            lock (_updatableComponents)
                _updatableComponents.AddRange(entity.GetComponents<IUpdatableComponent>());

            lock (CameraComponents)
                CameraComponents.AddRange(entity.GetComponents<CameraComponent>());

            lock (OnGuiComponents)
                OnGuiComponents.AddRange(entity.GetComponents<IOnGUIComponent>());            
        }

        /// <summary>
        /// Unregister entity.
        /// </summary>
        private void UnregisterEntity(Entity entity)
        {
            lock (_updatableComponents)
                foreach (IUpdatableComponent updatableComponent in entity.GetComponents<IUpdatableComponent>())
                    if (_updatableComponents.Contains(updatableComponent))
                        _updatableComponents.Remove(updatableComponent);

            lock (CameraComponents)
                foreach (CameraComponent cameraComponent in entity.GetComponents<CameraComponent>())
                    if (CameraComponents.Contains(cameraComponent))
                        CameraComponents.Remove(cameraComponent);

            lock (OnGuiComponents)
                foreach (IOnGUIComponent onGuiComponent in entity.GetComponents<IOnGUIComponent>())
                    if (OnGuiComponents.Contains(onGuiComponent))
                        OnGuiComponents.Remove(onGuiComponent);
        }

        /// <summary>
        /// Update rendering pipeline.
        /// </summary>
        public void UpdateRenderingPipeline(IRenderingPipeline pipeline, CameraComponent cameraComponent)
        {

        }

        /// <summary>
        /// Get rendering pipeline.
        /// </summary>
        public void UpdateRenderingPipeline(IRenderingPipeline pipeline, LightComponent lightComponent)
        {
            
        }
    }
}

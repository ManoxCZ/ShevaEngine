﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using ShevaEngine.Graphics.Meshes;
using System;
using System.IO;

namespace ModelConvertor
{
    public class ModelConvertor
    {
        /// <summary>
        /// Convert model.
        /// </summary>
        /// <param name="inputFilename"></param>
        public static void Convert(string inputFilename)
        {
            using (Assimp.AssimpContext context = new Assimp.AssimpContext())
            {
                Assimp.Scene scene = context.ImportFile(inputFilename);

                foreach (Assimp.Mesh mesh in scene.Meshes)
                {
                    string outputFilename = Path.ChangeExtension(inputFilename, "mesh");

                    if (!string.IsNullOrEmpty(outputFilename))
                    {
                        using (JsonWriter writer = new JsonTextWriter(new StreamWriter(outputFilename)))
                        {
                            JsonSerializer serializer = new JsonSerializer();

                            serializer.Serialize(writer, ConvertMesh(mesh));
                        }
                    }
                }
            }
        }


        private static Mesh ConvertMesh(Assimp.Mesh assimpMesh)
        {
            Mesh output = new Mesh
            {
                Id = Guid.NewGuid(),
                Vertices = new Vector3[assimpMesh.Vertices.Count]
            };


            for (int i = 0; i < assimpMesh.Vertices.Count; i++)
            {
                output.Vertices[i] = new Vector3(assimpMesh.Vertices[i].X, assimpMesh.Vertices[i].Y, assimpMesh.Vertices[i].Z);
            }

            output.Normals = new Vector3[assimpMesh.Normals.Count];

            for (int i = 0; i < assimpMesh.Normals.Count; i++)
            {
                output.Normals[i] = new Vector3(assimpMesh.Normals[i].X, assimpMesh.Normals[i].Y, assimpMesh.Normals[i].Z);
            }

            output.TextureCoords1 = new Vector2[assimpMesh.TextureCoordinateChannels[0].Count];

            for (int i = 0; i < assimpMesh.TextureCoordinateChannels[0].Count; i++)
            {
                output.TextureCoords1[i] = new Vector2(assimpMesh.TextureCoordinateChannels[0][i].X, assimpMesh.TextureCoordinateChannels[0][i].Y);
            }            

            return output;
        }        
    }
}

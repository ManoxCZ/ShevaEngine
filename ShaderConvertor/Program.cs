﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShaderConvertor
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
                ShaderConvertor.Convert(args[0]);
        }
    }
}

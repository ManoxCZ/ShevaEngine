﻿using Newtonsoft.Json;
using ShevaEngine.Graphics.Shaders;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace ShaderConvertor
{
    public class ShaderConvertor
    {
        /// <summary>
        /// Convert.
        /// </summary>        
        public static void Convert(string arg)
        {
            if (File.Exists(arg))
                ConvertFile(arg);
            else if (Directory.Exists(arg))
                ConvertDirectory(arg);
        }

        /// <summary>
        /// Convert.
        /// </summary>        
        public static void ConvertFile(string inputFilename, string outputFilename = null)
        {
            Process process = new Process();
            process.StartInfo.Arguments = inputFilename + " " + Path.ChangeExtension(inputFilename, "bytecode") +
                                          " /Profile:DirectX_11";
            process.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
            process.StartInfo.FileName = @"C:\Program Files (x86)\MSBuild\MonoGame\v3.0\Tools\2mgfx.exe";

            process.Start();

            process.WaitForExit();

            if (!File.Exists(Path.ChangeExtension(inputFilename, "bytecode")))
                return;

            Shader newShader = new Shader();
            
            newShader.ByteCode = File.ReadAllBytes(Path.ChangeExtension(inputFilename, "bytecode"));

            JsonSerializerSettings settings = new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.Auto};

            string output = outputFilename ?? Path.ChangeExtension(inputFilename, "shader");

            using (StreamWriter writer = new StreamWriter(output, false, Encoding.UTF8))
            {
                writer.Write(JsonConvert.SerializeObject(newShader, settings));
            }
        }

        /// <summary>
        /// Convert.
        /// </summary>        
        public static void ConvertDirectory(string inputDirectory)
        {
            foreach (string file in Directory.GetFiles(inputDirectory,"*", SearchOption.AllDirectories))
            {
                if (Path.GetExtension(file) ==  ".fx" && Path.GetFileNameWithoutExtension(file) != "helper")
                {
                    ConvertFile(file);
                }
            }
        }
    }
}

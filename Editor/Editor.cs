﻿using ShevaEngine;

namespace Editor
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Editor : ShevaGame
    {
        
        /// <summary>
        /// Constructor.
        /// </summary>
        public Editor()
        {
            
        }              
    }
}

float4x4 World;
float4x4 View;
float4x4 Proj;

texture Diffuse;

sampler2D DiffuseSampler = sampler_state
{
	Texture = <Diffuse>;
	MipFilter = NONE;
	MagFilter = POINT;
	MinFilter = POINT;
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float2 TexCoord : TEXCOORD0;
};


VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Proj);    

	output.TexCoord = input.TexCoord;

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	float4 outputColor = tex2D(DiffuseSampler, input.TexCoord);

	clip(outputColor.a - 0.999f);

    return outputColor;
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_4_0 VertexShaderFunction();
        PixelShader = compile ps_4_0 PixelShaderFunction();
    }
}

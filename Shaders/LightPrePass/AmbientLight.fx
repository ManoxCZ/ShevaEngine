#include "../Common/Structs.fxh"
#include "../Common/Textures.fxh"
#include "../Common/Normals.fxh"
#include "../Common/Specular.fxh"
#include "../Common/GammaLinearSpace.fxh"
#include "../Common/SphericalHarmonics.fxh"

float4x4 viewI;
float2 HalfPixel;
float intensity = 0.1f;
float ambientOcclusionStrength;

texture ambientOcclusionTexture;

sampler2D ambientOcclusionSample = sampler_state
{
    Texture = <ambientOcclusionTexture>;
    MinFilter = Point;
    MagFilter = Point;
	MipFilter = none;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

texture lightMapTexture;

sampler2D lightMapSample = sampler_state
{
    Texture = <lightMapTexture>;
    MinFilter = Point;
    MagFilter = Point;
	MipFilter = none;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

struct VS_OUT
{
	float4 position		: POSITION;
	float2 uv			: TEXCOORD0;
};

VS_OUT vs_main(in float4 position : POSITION, in float2 uv : TEXCOORD)
{
	VS_OUT output = (VS_OUT)0;
	
	output.position = position;
	output.position.xy += HalfPixel; // http://drilian.com/2008/11/25/understanding-half-pixel-and-half-texel-offsets/
	output.uv = uv; 
	
	return output;
}

// This shader works in view space.
float4 ps_mainSH(in float2 uv : TEXCOORD0) : COLOR0
{	
	float3 N = SampleNormal(uv, NormalSamplerHV2);

	// Normal (view space) to world space
	N = normalize(mul(N, viewI));
	
	return float4(SampleSH(N) * intensity, 0);
}

// This shader works in view space.
float4 ps_mainSHSSAO(in float2 uv : TEXCOORD0) : COLOR0
{	
	float3 N = SampleNormal(uv, NormalSamplerHV2);

	// Normal (view space) to world space
	N = normalize(mul(N, viewI));	

	float ssao = tex2D(ambientOcclusionSample, uv).r;
		
	return float4(SampleSH(N) * intensity * pow(ssao, ambientOcclusionStrength), 0);
}

technique AmbientLightSH
{
	pass p0
	{
		VertexShader = compile vs_4_0 vs_main();
		PixelShader  = compile ps_4_0 ps_mainSH();
	}
}

technique AmbientLightSHSSAO
{
	pass p0
	{
		VertexShader = compile vs_4_0 vs_main();
		PixelShader  = compile ps_4_0 ps_mainSHSSAO();
	}
}
#include "../Common/GammaLinearSpace.fxh"
#include "../Common/Normals.fxh"
#include "../Common/Specular.fxh"

static const float BIAS = 0.08f;

float4x4 World;
float4x4 View;
float4x4 Projection;
float4x4 ViewI;
float4x4 lightView;
float4x4 Camera;

float2 halfPixel;
float3 lightPosition;
float3 lightColor = float3(1,1,1);
float lightRadius = 10;
float lightIntensity = 1;
bool insideBoundingLightObject;
float farPlane = 100.0f;
float nearPlane = 0.01f;

texture normalTexture : RENDERCOLORTARGET;

sampler2D normalSampler = sampler_state
{
	Texture = <normalTexture>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = NONE;
};

texture depthTexture : RENDERCOLORTARGET;

sampler2D depthSampler = sampler_state
{
	Texture = <depthTexture>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = NONE;
};

texture shadowMapTop;

sampler2D shadowMapTopSampler = sampler_state
{
	Texture = <shadowMapTop>;
    MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = POINT;
	AddressU  = Clamp;
	AddressV  = Clamp;
};

texture shadowMapBottom;

sampler2D shadowMapBottomSampler = sampler_state
{
	Texture = <shadowMapBottom>;
    MinFilter = POINT;
	MagFilter = POINT;
	MipFilter = POINT;
	AddressU  = Clamp;
	AddressV  = Clamp;
};

texture motionVectorSpecularPowerTexture : RENDERCOLORTARGET;

sampler2D motionVectorSpecularPowerSampler = sampler_state
{
	Texture = <motionVectorSpecularPowerTexture>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = NONE;
};

struct VS_OUT
{
	float4 position			: POSITION;
	float4 screenPosition 	: TEXCOORD0;
	float4 viewPosition     : TEXCOORD1;
};

VS_OUT vs_main(in float4 position : POSITION)
{
	VS_OUT output = (VS_OUT)0;
	
	output.position = mul(position, World);	
	output.position = mul(output.position, View);	
	output.position = mul(output.position, Projection);	

	output.screenPosition = output.position;
	
	output.viewPosition = mul(position, World);
	output.viewPosition = mul(output.viewPosition, View);

	return output;
}

float ComputeParaboloidShadow(float3 worldPosition)
{	
	//float4 pos = mul(mul(float4(worldPosition,1), Camera), lightView);
	float4 pos = mul(mul(float4(worldPosition,1), Camera), lightView);
        
    float L = length(pos);
    pos /= L;

	float ourdepth = (L - nearPlane) / (lightRadius - nearPlane);	

	float shadow = 1;
	float depthFromMap;	
	
	if(pos.z > 0.0f)
    {
        float2 vTexFront;
        vTexFront.x =  (pos.x /  (1.0f + pos.z)) * 0.5f + 0.5f; 
        vTexFront.y =  1.0f - ((pos.y /  (1.0f + pos.z)) * 0.5f + 0.5f); 		

		depthFromMap = tex2D(shadowMapTopSampler, vTexFront.xy).r;		
    }
    else
    {
        float2 vTexBack;
        vTexBack.x =  (pos.x /  (1.0f - pos.z)) * 0.5f + 0.5f; 
        vTexBack.y =  1.0f - ((pos.y /  (1.0f - pos.z)) * 0.5f + 0.5f); 
	
		depthFromMap = tex2D(shadowMapBottomSampler, vTexBack.xy).r;		
    }

	//return depthFromMap;

	if((depthFromMap + BIAS) < ourdepth)		
		shadow = 0.0f;
				
	return shadow;
}

float4 ps_NoneSMmain(VS_OUT input) : COLOR0
{	 	
    input.screenPosition.xy /= input.screenPosition.w;	

    float2 uv = 0.5f * (float2(input.screenPosition.x, -input.screenPosition.y) + 1) + halfPixel;

	float depth = tex2D(depthSampler, uv).r;
	
	if (depth == 1)
	{
		// discard' doesn't actually exit the shader (the shader will continue to execute). 
		// It merely instructs the output merger stage not to output the result of the pixel (which must still be returned by the shader).
		// The pair discard return rocks!!! And you should be use it. However, the xbox 360 doesn't support it, I think.
		discard;
		return float4(0, 0, 0, 0);
	}
	
	// Optimization. We can't implement stencil optimizations, but at least this will allow us to avoid the normal map fetch and some other calculations.
	if (insideBoundingLightObject)
	{
		if (depth > -input.viewPosition.z / farPlane)
		{
			// discard' doesn't actually exit the shader (the shader will continue to execute). 
			// It merely instructs the output merger stage not to output the result of the pixel (which must still be returned by the shader).
			// The pair discard return rocks!!! And you should be use it. However, the xbox 360 doesn't support it, I think.
			discard;
			return float4(0, 0, 0, 0);
		}
	}
	else
	{
		if (depth < -input.viewPosition.z / farPlane)
		{
			// discard' doesn't actually exit the shader (the shader will continue to execute). 
			// It merely instructs the output merger stage not to output the result of the pixel (which must still be returned by the shader).
			// The pair discard return rocks!!! And you should be use it. However, the xbox 360 doesn't support it, I think.
			discard;
			return float4(0, 0, 0, 0);
		}
	}			
	
	// This is a ray constructed using the camera frustum.
    // Because it will be interpolated for the current pixel we can use
    // this to reconstruct the position of the surface we want to light.
    float3 frustumRayVS = input.viewPosition.xyz * (farPlane / -input.viewPosition.z);

	// Reconstruct the view space position of the surface to light.
    float3 positionVS = depth * frustumRayVS;

	// Surface-to-light vector (in view space)
    float3 L = lightPosition - positionVS;			

	float3 N = SampleNormal(uv, normalSampler);

    // Compute diffuse light
    float NL = max(dot(N, normalize(L)), 0);	

	if (NL == 0)
	{
		// discard' doesn't actually exit the shader (the shader will continue to execute). 
		// It merely instructs the output merger stage not to output the result of the pixel (which must still be returned by the shader).
		// The pair discard return rocks!!! And you should be use it. However, the xbox 360 doesn't support it, I think.
		discard; 
		return float4(0, 0, 0, 0);
	}	

	// Compute attenuation
	// Basic attenuation
	//float attenuation = saturate(1.0f - dot(L, L) / pow(lightRadius, 2)); // length(L)
	// Natural light attenuation (http://fools.slindev.com/viewtopic.php?f=11&t=21&view=unread#unread)
	float attenuationFactor = 1;
	float attenuation = dot(L, L) / pow(lightRadius, 2);
	attenuation = 1 / (attenuation * attenuationFactor + 1);
	// Second we move down the function therewith it reaches zero at abscissa 1:
	attenuationFactor = 1/ (attenuationFactor + 1); //att_s contains now the value we have to subtract
	attenuation = attenuation - attenuationFactor;
	// Finally we expand the equation along the y-axis so that it starts with a function value of 1 again.
	attenuation /= 1 - attenuationFactor;	

	// In "Experimental Validation of Analytical BRDF Models" (Siggraph2004) the autors arrive to the conclusion that half vector lobe is better than mirror lobe.
	float3 V = normalize(-positionVS);
	float3 H  = normalize(V + L);
	// Compute specular light
    float specular = pow(saturate(dot(N, H)), DecompressSpecularPower(tex2D(motionVectorSpecularPowerSampler, uv).b));

    /*// Reflexion vector
    float3 R = normalize(reflect(-L, N));
    // Camera-to-surface vector
    float3 V = normalize(-positionVS);
	// Compute specular light
    float specular = pow(saturate(dot(R, V)), specularPower);*/			

	// Fill the light buffer:
	// R: Color.r * N.L // The color need to be in linear space and right now it's in gamma.
	// G: Color.g * N.L
	// B: Color.b * N.L
	// A: Specular Term * N.L (Look in Shader X7 to know why N * L is necesary in this last channel)
	// Also in Shader X7 talk about a new channel so that the material shininess could be controled better.
	// http://diaryofagraphicsprogrammer.blogspot.com/2008/03/light-pre-pass-renderer.html	    
	return float4(lightColor, specular) * attenuation * lightIntensity * NL;			
    return float4(GammaToLinear(lightColor), specular) * attenuation * lightIntensity * NL;			
}

float4 ps_DualParaboloidSMmain(VS_OUT input) : COLOR0
{	 	
    input.screenPosition.xy /= input.screenPosition.w;	

    float2 uv = 0.5f * (float2(input.screenPosition.x, -input.screenPosition.y) + 1) + halfPixel;

	float depth = tex2D(depthSampler, uv).r;
	
	if (depth == 1)
	{
		// discard' doesn't actually exit the shader (the shader will continue to execute). 
		// It merely instructs the output merger stage not to output the result of the pixel (which must still be returned by the shader).
		// The pair discard return rocks!!! And you should be use it. However, the xbox 360 doesn't support it, I think.
		discard;
		return float4(0, 0, 0, 0);
	}
	
	// Optimization. We can't implement stencil optimizations, but at least this will allow us to avoid the normal map fetch and some other calculations.
	if (insideBoundingLightObject)
	{
		if (depth > -input.viewPosition.z / farPlane)
		{
			// discard' doesn't actually exit the shader (the shader will continue to execute). 
			// It merely instructs the output merger stage not to output the result of the pixel (which must still be returned by the shader).
			// The pair discard return rocks!!! And you should be use it. However, the xbox 360 doesn't support it, I think.
			discard;
			return float4(0, 0, 0, 0);
		}
	}
	else
	{
		if (depth < -input.viewPosition.z / farPlane)
		{
			// discard' doesn't actually exit the shader (the shader will continue to execute). 
			// It merely instructs the output merger stage not to output the result of the pixel (which must still be returned by the shader).
			// The pair discard return rocks!!! And you should be use it. However, the xbox 360 doesn't support it, I think.
			discard;
			return float4(0, 0, 0, 0);
		}
	}			
	
	// This is a ray constructed using the camera frustum.
    // Because it will be interpolated for the current pixel we can use
    // this to reconstruct the position of the surface we want to light.
    float3 frustumRayVS = input.viewPosition.xyz * (farPlane / -input.viewPosition.z);

	// Reconstruct the view space position of the surface to light.
    float3 positionVS = depth * frustumRayVS;

	float shadow = ComputeParaboloidShadow(positionVS);

    // Surface-to-light vector (in view space)
    float3 L = lightPosition - positionVS;			

	float3 N = SampleNormal(uv, normalSampler);

    // Compute diffuse light
    float NL = max(dot(N, normalize(L)), 0);	

	if (NL == 0)
	{
		// discard' doesn't actually exit the shader (the shader will continue to execute). 
		// It merely instructs the output merger stage not to output the result of the pixel (which must still be returned by the shader).
		// The pair discard return rocks!!! And you should be use it. However, the xbox 360 doesn't support it, I think.
		discard; 
		return float4(0, 0, 0, 0);
	}	

	// Compute attenuation
	// Basic attenuation
	//float attenuation = saturate(1.0f - dot(L, L) / pow(lightRadius, 2)); // length(L)
	// Natural light attenuation (http://fools.slindev.com/viewtopic.php?f=11&t=21&view=unread#unread)
	float attenuationFactor = 1;
	float attenuation = dot(L, L) / pow(lightRadius, 2);
	attenuation = 1 / (attenuation * attenuationFactor + 1);
	// Second we move down the function therewith it reaches zero at abscissa 1:
	attenuationFactor = 1/ (attenuationFactor + 1); //att_s contains now the value we have to subtract
	attenuation = attenuation - attenuationFactor;
	// Finally we expand the equation along the y-axis so that it starts with a function value of 1 again.
	attenuation /= 1 - attenuationFactor;	

	// In "Experimental Validation of Analytical BRDF Models" (Siggraph2004) the autors arrive to the conclusion that half vector lobe is better than mirror lobe.
	float3 V = normalize(-positionVS);
	float3 H  = normalize(V + L);
	// Compute specular light
    float specular = pow(saturate(dot(N, H)), DecompressSpecularPower(tex2D(motionVectorSpecularPowerSampler, uv).b));

    /*// Reflexion vector
    float3 R = normalize(reflect(-L, N));
    // Camera-to-surface vector
    float3 V = normalize(-positionVS);
	// Compute specular light
    float specular = pow(saturate(dot(R, V)), specularPower);*/			

	// Fill the light buffer:
	// R: Color.r * N.L // The color need to be in linear space and right now it's in gamma.
	// G: Color.g * N.L
	// B: Color.b * N.L
	// A: Specular Term * N.L (Look in Shader X7 to know why N * L is necesary in this last channel)
	// Also in Shader X7 talk about a new channel so that the material shininess could be controled better.
	// http://diaryofagraphicsprogrammer.blogspot.com/2008/03/light-pre-pass-renderer.html	    
	return float4(lightColor, specular) * attenuation * lightIntensity * NL * shadow;			
    return float4(GammaToLinear(lightColor), specular) * attenuation * lightIntensity * NL * shadow;			
}

technique NoneSM
{
	pass p0
	{
		VertexShader = compile vs_4_0 vs_main();
		PixelShader  = compile ps_5_0 ps_NoneSMmain();
	}
} 

technique DualParaboloidSM
{
	pass p0
	{
		VertexShader = compile vs_4_0 vs_main();
		PixelShader  = compile ps_5_0 ps_DualParaboloidSMmain();
	}
} 

#include "../Common/Structs.fxh"
#include "../Common/Textures.fxh"
#include "../Common/Normals.fxh"
#include "../Common/Specular.fxh"


float FarPlane = 100.0f;
float SpecularPower;
bool SpecularTextured;
bool DiffuseTextured;

texture normalRenderTarget : RENDERCOLORTARGET;

sampler2D normalSampler = sampler_state
{
	Texture = <normalRenderTarget>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = NONE;
};

texture depthRenderTarget : RENDERCOLORTARGET;

sampler2D depthSampler = sampler_state
{
	Texture = <depthRenderTarget>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = NONE;
};

texture motionVectorSpecularPowerRenderTarget : RENDERCOLORTARGET;

sampler2D motionVectorSpecularPowerSampler = sampler_state
{
	Texture = <motionVectorSpecularPowerRenderTarget>;
    ADDRESSU = CLAMP;
	ADDRESSV = CLAMP;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = NONE;
};

struct PixelShader_OUTPUT
{
    float4 depth  : COLOR0;
    float4 normal : COLOR1;
	float4 motionVectorSpecularPower : COLOR2;
};

PositionNormalDepthVSOutput GBufferWithoutTextureVS(PositionNormalVSInput input)
{
	PositionNormalDepthVSOutput output;
   
    Matrix worldView = mul(World, View);

	output.position = mul(mul(input.position, worldView), Projection);
	output.depth    = -mul(input.position, worldView).z / FarPlane;
	// The normals are in view space.
	output.normal   = mul(input.normal, WorldViewIT);

	return output;
}


PositionNormalDepthVSOutput GBufferWithoutTextureVSHWInstancing(PositionNormalVSInput input, 
	float4x4 instanceTransform : BLENDWEIGHT, float4x4 instanceWorldVievITTransform : COLOR)
{
	PositionNormalDepthVSOutput output;
   
	float4x4 newWorld = transpose(instanceTransform);
	float4x4 newWorldViewIT = transpose(instanceWorldVievITTransform);

	float4 position = mul(input.position, newWorld);
	position = mul(position, View);
	output.position = mul(position, Projection);
	output.depth    = -mul(mul(input.position, newWorld), View).z / FarPlane;	
	// The normals are in view space.
	output.normal   = mul(input.normal, newWorldViewIT);

	return output;
}


PositionNormalDepthVSOutput GBufferWithoutTextureVSAnimated(AnimatedPositionNormalVSInput input)
{
	PositionNormalDepthVSOutput output;
   
	float4x3 skinning = 0;

    [unroll]
    for (int i = 0; i < 4; i++)
    {
        skinning += AnimationTransforms[input.indices[i]] * input.weights[i];
    }

    input.position.xyz = mul(input.position, skinning);    
	input.normal.xyz = mul(input.normal, skinning);    

	Matrix worldView = mul(World, View);

	output.position = mul(mul(input.position, worldView), Projection);
	output.depth    = -mul(input.position, worldView).z / FarPlane;
	// The normals are in view space.
	output.normal   = mul(input.normal, WorldViewIT);

	return output;
}

PositionNormalDepthTexCoordVSOutput GBufferWithDiffuseTextureVS(PositionNormalTexCoordVSInput input)
{
	PositionNormalDepthTexCoordVSOutput output;
   
    Matrix worldView = mul(World, View);

	output.position = mul(mul(input.position, worldView), Projection);
	output.depth    = -mul(input.position, worldView).z / FarPlane;
	// The normals are in view space.
	output.normal   = mul(input.normal, WorldViewIT);
	output.uv = input.uv;

	return output;
}


PositionNormalDepthTexCoordVSOutput GBufferWithDiffuseTextureVSHWInstancing(PositionNormalTexCoordVSInput input, 
	float4x4 instanceTransform : BLENDWEIGHT, float4x4 instanceWorldVievITTransform : COLOR)
{
	PositionNormalDepthTexCoordVSOutput output;
   
	float4x4 newWorld = transpose(instanceTransform);
	float4x4 newWorldViewIT = transpose(instanceWorldVievITTransform);

	float4 position = mul(input.position, newWorld);
	position = mul(position, View);
	output.position = mul(position, Projection);
	output.depth    = -mul(mul(input.position, newWorld), View).z / FarPlane;	
	// The normals are in view space.
	output.normal   = mul(input.normal, newWorldViewIT);
	output.uv = input.uv;

	return output;
}


PositionNormalDepthTexCoordVSOutput GBufferWithDiffuseTextureVSAnimated(AnimatedPositionNormalTexCoordVSInput input)
{
	PositionNormalDepthTexCoordVSOutput output;
   
	float4x3 skinning = 0;

    [unroll]
    for (int i = 0; i < 4; i++)
    {
        skinning += AnimationTransforms[input.indices[i]] * input.weights[i];
    }

    input.position.xyz = mul(input.position, skinning);    
	input.normal.xyz = mul(input.normal, skinning);    

	Matrix worldView = mul(World, View);

	output.position = mul(mul(input.position, worldView), Projection);
	output.depth    = -mul(input.position, worldView).z / FarPlane;
	// The normals are in view space.
	output.normal   = mul(input.normal, WorldViewIT);
	output.uv = input.uv;

	return output;
}

PositionDepthTangentVSOutput GBufferWithNormalMapVS(PositionTangentTexCoordVSInput input)
{
	PositionDepthTangentVSOutput output;
   
	Matrix worldView = mul(World, View);

	output.position = mul(mul(input.position, worldView), Projection);
	output.depth    = -mul(input.position, worldView).z / FarPlane;     

	// Generate the tanget space to view space matrix
	output.tangentToView[0] = mul(input.tangent,  WorldViewIT);
	output.tangentToView[1] = mul(input.binormal, WorldViewIT); // binormal = cross(input.tangent, input.normal)
	output.tangentToView[2] = mul(input.normal,   WorldViewIT);

	output.uv = input.uv;

	return output;
}

PositionDepthTangentVSOutput GBufferWithNormalMapVSHwInstancing(PositionTangentTexCoordVSInput input, 
	float4x4 instanceTransform : BLENDWEIGHT, float4x4 instanceWorldVievITTransform : COLOR)
{
	PositionDepthTangentVSOutput output;
   
    float4x4 newWorld = transpose(instanceTransform);
	float4x4 newWorldViewIT = transpose(instanceWorldVievITTransform);

	Matrix worldView = mul(newWorld, View);

	output.position = mul(mul(input.position, worldView), Projection);
	output.depth    = -mul(input.position, worldView).z / FarPlane;     

	// Generate the tanget space to view space matrix
	output.tangentToView[0] = mul(input.tangent,  newWorldViewIT);
	output.tangentToView[1] = mul(input.binormal, newWorldViewIT); // binormal = cross(input.tangent, input.normal)
	output.tangentToView[2] = mul(input.normal,   newWorldViewIT);

	output.uv = input.uv;

	return output;
}

PositionDepthTangentVSOutput GBufferWithNormalMapVSAnimated(AnimatedPositionNormalTangentTexCoordVSInput input)
{
	PositionDepthTangentVSOutput output;
   
    float4x3 skinning = 0;

    [unroll]
    for (int i = 0; i < 4; i++)
    {
        skinning += AnimationTransforms[input.indices[i]] * input.weights[i];
    }

    input.position.xyz = mul(input.position, skinning);    
	input.normal.xyz = mul(input.normal, skinning);    

	Matrix worldView = mul(World, View);

	output.position = mul(mul(input.position, worldView), Projection);	
	output.depth    = -mul(input.position, worldView).z / FarPlane;     

	// Generate the tanget space to view space matrix
	output.tangentToView[0] = mul(input.tangent,  WorldViewIT);
	output.tangentToView[1] = mul(input.binormal, WorldViewIT); // binormal = cross(input.tangent, input.normal)
	output.tangentToView[2] = mul(input.normal,   WorldViewIT);

	output.uv = input.uv;

	return output;
}

// PIXEL SHADERS

PixelShader_OUTPUT GBufferWithoutTexturePS(PositionNormalDepthVSOutput input)
{
	PixelShader_OUTPUT output = (PixelShader_OUTPUT)0;
 
	output.depth = float4(input.depth, 0, 1, 1);

	input.normal = normalize(input.normal);
		
	// Spherical Coordinates
	float f = input.normal.z * 2 + 1;
	float g = dot(input.normal, input.normal);
	float p = sqrt(g + f);
	output.normal  = float4(input.normal.xy / p * 0.5 + 0.5, 1, 1);
	output.normal = float4(normalize(input.normal.xy) * sqrt(input.normal.z * 0.5 + 0.5), 1, 1); // Spheremap Transform: Crytek method. 
	output.normal = 0.5f * (float4(input.normal.xyz, 1) + 1.0f); // Change to the [0, 1] range to avoid negative values.

	output.motionVectorSpecularPower.b = CompressSpecularPower(SpecularPower);

	return output;
}

PixelShader_OUTPUT GBufferWithDiffuseTexturePS(PositionNormalDepthTexCoordVSOutput input)
{
	PixelShader_OUTPUT output = (PixelShader_OUTPUT)0;
 
	clip (tex2D(DiffuseSampler, input.uv).a - 0.999);

	output.depth = float4(input.depth, 0, 1, 1);

	input.normal = normalize(input.normal);
		
	// Spherical Coordinates
	float f = input.normal.z * 2 + 1;
	float g = dot(input.normal, input.normal);
	float p = sqrt(g + f);
	output.normal  = float4(input.normal.xy / p * 0.5 + 0.5, 1, 1);
	output.normal = float4(normalize(input.normal.xy) * sqrt(input.normal.z * 0.5 + 0.5), 1, 1); // Spheremap Transform: Crytek method. 
	output.normal = 0.5f * (float4(input.normal.xyz, 1) + 1.0f); // Change to the [0, 1] range to avoid negative values.

	output.motionVectorSpecularPower.b = CompressSpecularPower(SpecularPower);

	return output;
}

PixelShader_OUTPUT GBufferWithNormalMapPS(PositionDepthTangentVSOutput input)
{
	PixelShader_OUTPUT output = (PixelShader_OUTPUT)0;
 
	clip (tex2D(DiffuseSampler, input.uv).a - 0.999);

	output.depth = float4(input.depth, 0, 1, 1);
 
 	output.normal.xyz = 2.0 * tex2D(NormalSampler, input.uv).rgb - 1;
	output.normal.xyz =  normalize(mul(output.normal.xyz, input.tangentToView));	

	// Spherical Coordinates
	float f = output.normal.z * 2 + 1;
	float g = dot(output.normal.xyz, output.normal.xyz);
	float p = sqrt(g + f);
	output.normal  = float4(output.normal.xy / p * 0.5 + 0.5, 1, 1);
	
	if (SpecularTextured)
		output.motionVectorSpecularPower.b = CompressSpecularPower(tex2D(SpecularSampler, input.uv).a);	
	else
		output.motionVectorSpecularPower.b = CompressSpecularPower(SpecularPower);		

	return output;
} 



technique GBufferWithoutTexture
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithoutTextureVS();
        PixelShader  = compile ps_4_0 GBufferWithoutTexturePS();
    }
} 

technique GBufferWithoutTextureHwInstancing
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithoutTextureVSHWInstancing();
        PixelShader  = compile ps_4_0 GBufferWithoutTexturePS();
    }
} 

technique GBufferWithoutTextureAnimated
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithoutTextureVSAnimated();
        PixelShader  = compile ps_4_0 GBufferWithoutTexturePS();
    }
} 

technique GBufferWithDiffuseMap
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithDiffuseTextureVS();
        PixelShader  = compile ps_4_0 GBufferWithDiffuseTexturePS();
    }
} 

technique GBufferWithDiffuseMapHwInstancing
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithDiffuseTextureVSHWInstancing();
        PixelShader  = compile ps_4_0 GBufferWithDiffuseTexturePS();
    }
} 

technique GBufferWithDiffuseMapAnimated
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithDiffuseTextureVSAnimated();
        PixelShader  = compile ps_4_0 GBufferWithDiffuseTexturePS();
    }
} 

technique GBufferWithNormalMap
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithNormalMapVS();
        PixelShader  = compile ps_4_0 GBufferWithNormalMapPS();
    }
}

technique GBufferWithNormalMapHwInstancing
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithNormalMapVSHwInstancing();
        PixelShader  = compile ps_4_0 GBufferWithNormalMapPS();
    }
}

technique GBufferWithNormalMapAnimated
{
    pass p0
    {
        VertexShader = compile vs_4_0 GBufferWithNormalMapVSAnimated();
        PixelShader  = compile ps_4_0 GBufferWithNormalMapPS();
    }
}
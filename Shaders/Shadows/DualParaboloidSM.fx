#include "../Common/Structs.fxh"
#include "../Common/Textures.fxh"

float ShadowMapDirection = 1.0f;
float ShadowMapFarPlane = 25.0f;
float ShadowMapNearPlane = .001f;


PositionZDepthVSOutput VertexShader_ParaboloidShadowMap(PositionVSInput input)
{    
	PositionZDepthVSOutput output;	
	
	output.Position = mul(input.Position, mul(World, View));
	//output.Position = output.Position / output.Position.w;
	
	output.Position.z = output.Position.z * ShadowMapDirection;
	
	float L = length( output.Position.xyz );
	output.Position = output.Position / L;
	
	output.Z = output.Position.z;
	output.Position.z = output.Position.z + 1;

	output.Position.xy = output.Position.xy / output.Position.z;	

	output.Position.z = (L - ShadowMapNearPlane) / (ShadowMapFarPlane - ShadowMapNearPlane);
	
	output.Position.w = 1;
	
	output.Depth = output.Position.zw;	

	return output;
}

PositionZDepthVSOutput VertexShader_ParaboloidShadowMapHwInstancing(PositionVSInput input, float4x4 instanceTransform : BLENDWEIGHT)
{    
	PositionZDepthVSOutput output;	
	
	float4x4 newWorld = transpose(instanceTransform);

	output.Position = mul(input.Position, mul(newWorld, View));
	//output.Position = output.Position / output.Position.w;
	
	output.Position.z = output.Position.z * ShadowMapDirection;
	
	float L = length( output.Position.xyz );
	output.Position = output.Position / L;
	
	output.Z = output.Position.z;
	output.Position.z = output.Position.z + 1;

	output.Position.xy = output.Position.xy / output.Position.z;	

	output.Position.z = (L - ShadowMapNearPlane) / (ShadowMapFarPlane - ShadowMapNearPlane);
	output.Position.w = 1;
	
	output.Depth = output.Position.zw;
	
	return output;
}

PositionZDepthVSOutput VertexShader_ParaboloidShadowMapWithAnimations(AnimatedPositionVSInput input)
{    
	PositionZDepthVSOutput output;	
	
	float4x3 skinning = 0;

    [unroll]
    for (int i = 0; i < 4; i++)
    {
        skinning += AnimationTransforms[input.indices[i]] * input.weights[i];
    }

    input.Position.xyz = mul(input.Position, skinning);    

	output.Position = mul(input.Position, mul(World, View));
	//output.Position = output.Position / output.Position.w;
	
	output.Position.z = output.Position.z * ShadowMapDirection;
	
	float L = length( output.Position.xyz );
	output.Position = output.Position / L;
	
	output.Z = output.Position.z;
	output.Position.z = output.Position.z + 1;

	output.Position.xy = output.Position.xy / output.Position.z;	

	output.Position.z = (L - ShadowMapNearPlane) / (ShadowMapFarPlane - ShadowMapNearPlane);
	output.Position.w = 1;
	
	output.Depth = output.Position.zw;
	
	return output;
}

float4 PixelShader_ParaboloidShadowMap(PositionZDepthVSOutput input) : COLOR
{
	clip(input.Z);
	
	return float4(input.Depth.x, 0,0,1);//input.Depth.x * input.Depth.x, 0, 1);
}

technique NoAnimations
{
	pass P0
	{
		vertexShader = compile vs_4_0 VertexShader_ParaboloidShadowMap();
        pixelShader  = compile ps_4_0 PixelShader_ParaboloidShadowMap();               		
	}
}

technique NoAnimationsHwInstancing
{
	pass P0
	{
		vertexShader = compile vs_4_0 VertexShader_ParaboloidShadowMapHwInstancing();
        pixelShader  = compile ps_4_0 PixelShader_ParaboloidShadowMap();               		
	}
}

technique WithAnimations
{
	pass P0
	{
		vertexShader = compile vs_4_0 VertexShader_ParaboloidShadowMapWithAnimations();
        pixelShader  = compile ps_4_0 PixelShader_ParaboloidShadowMap();               		
	}
}
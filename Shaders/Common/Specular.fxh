/// Compress to the (0,1) range with high precision for low values. Guerilla method.
float CompressSpecularPower(float specularPower)
{
	return log2(specularPower) / 10.5;
}

float DecompressSpecularPower(float compressedSpecularPower)
{
	return pow(2, compressedSpecularPower * 10.5);
}
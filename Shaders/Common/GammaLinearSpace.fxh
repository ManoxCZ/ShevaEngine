// Converts from linear RGB space to gamma.
float3 LinearToGamma(float3 color)
{
    return sqrt(color);
}

// Converts from gamma space to linear RGB.
float3 GammaToLinear(float3 color)
{
    return color * color;
}

// Converts from gamma space to linear RGB.
float4 GammaToLinear(float4 color)
{
    return color * color;
}
texture DiffuseMap;

sampler2D DiffuseSampler = sampler_state
{
	Texture = <DiffuseMap>;
    ADDRESSU = WRAP;
	ADDRESSV = WRAP;
	MAGFILTER = ANISOTROPIC; //LINEAR;
	MINFILTER = ANISOTROPIC; //LINEAR;
	MIPFILTER = LINEAR;
};

texture NormalMap;

sampler2D NormalSampler = sampler_state
{
	Texture = <NormalMap>;
    ADDRESSU = WRAP;
	ADDRESSV = WRAP;
	MAGFILTER = ANISOTROPIC; //LINEAR;
	MINFILTER = ANISOTROPIC; //LINEAR;
	MIPFILTER = LINEAR;
};

texture SpecularMap;

sampler2D SpecularSampler = sampler_state
{
	Texture = <SpecularMap>;
    ADDRESSU = WRAP;
	ADDRESSV = WRAP;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
	MIPFILTER = LINEAR;
};

texture NormalMapHV2;

sampler2D NormalSamplerHV2 = sampler_state
{
	Texture = <NormalMapHV2>;
    ADDRESSU = WRAP;
	ADDRESSV = WRAP;
	MAGFILTER = POINT; //LINEAR;
	MINFILTER = POINT; //LINEAR;
	MIPFILTER = POINT;
};
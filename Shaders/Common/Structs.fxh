float4x4 Projection;
float4x4 View;
float4x4 World;
float4x4 WorldViewIT;

float4x3 AnimationTransforms[70];


struct PositionVSInput
{
    float4 Position : POSITION0;
};

struct PositionNormalVSInput 
{
   float4 position : POSITION;
   float3 normal   : NORMAL;   
};

struct AnimatedPositionVSInput
{
    float4 Position : POSITION0;
	half4 indices : BLENDINDICES0;
	float4 weights	: BLENDWEIGHT0;
};

struct AnimatedPositionNormalVSInput
{
   float4 position : POSITION;
   float3 normal   : NORMAL;   
   half4 indices : BLENDINDICES0;
   float4 weights	: BLENDWEIGHT0;
};

struct PositionNormalTexCoordVSInput
{
   float4 position : POSITION;
   float3 normal   : NORMAL;
   float2 uv       : TEXCOORD0;
};

struct AnimatedPositionNormalTexCoordVSInput
{
   float4 position : POSITION;
   float3 normal   : NORMAL;
   float2 uv       : TEXCOORD0;
   half4 indices : BLENDINDICES0;
   float4 weights	: BLENDWEIGHT0;
};

struct PositionTangentTexCoordVSInput
{
   float4 position : POSITION;
   float3 normal   : NORMAL;
   float3 tangent  : TANGENT;
   float3 binormal : BINORMAL;
   float2 uv       : TEXCOORD0;
};

struct AnimatedPositionNormalTangentTexCoordVSInput
{
   float4 position : POSITION;
   float3 normal   : NORMAL;
   float3 tangent  : TANGENT;
   float3 binormal : BINORMAL;
   float2 uv       : TEXCOORD0;
   half4 indices : BLENDINDICES0;
   float4 weights	: BLENDWEIGHT0;
};

struct PositionNormalDepthVSOutput 
{
   float4 position : POSITION0;
   float3 normal   : TEXCOORD0;
   float  depth    : TEXCOORD1;   
};

struct PositionNormalDepthTexCoordVSOutput
{
   float4 position         : POSITION0;
   float3 normal           : TEXCOORD0;
   float  depth            : TEXCOORD1;   
   float2 uv			   : TEXCOORD2;
};

struct PositionDepthTangentVSOutput
{
   float4 position         : POSITION0;
   float  depth            : TEXCOORD0;
   float2 uv			   : TEXCOORD1;
   float3x3 tangentToView  : TEXCOORD2;
};

struct PositionZDepthVSOutput
{
    float4 Position : POSITION0;
    float Z		    : TEXCOORD0;
    float2 Depth    : TEXCOORD1;
};
float3 SampleNormal(float2 uv, sampler2D textureSampler)
{
	float2 normalInformation = tex2D(textureSampler, uv).xy;
	float3 N;
	
	// Spheremap Transform (not working)
	/*N.z = dot(normalInformation, normalInformation) * 2 - 1; // lenght2 = dot(v, v)
	N.xy = normalize(normalInformation) * sqrt(1 - N.z * N.z);	*/	

	// Spherical Coordinates
	N.xy = -normalInformation * normalInformation + normalInformation;
	N.z = -1;
	float f = dot(N, float3(1, 1, 0.25));
	float m = sqrt(f);
	N.xy = (normalInformation * 8 - 4) * m;
	N.z = -(1 - 8 * f);
	
	// Basic form
	//N = tex2D(textureSampler, uv).xyz * 2 - 1;
	
	return N; // Already normalized
} // SampleNormal

float3 SampleNormal(float2 uv, sampler normalSampler)
{
	return SampleNormal(uv, normalSampler);
} // SampleNormal

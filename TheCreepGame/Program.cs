﻿using System;
using ShevaEngine;

namespace TheCreepGame
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new ShevaGame())
                game.Run();
        }
    }
#endif
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShaderGenerator
{
    public class Strings
    {
        public const string ProjectionMatrixConst = "float4x4 Projection;";
        public const string ViewMatrixConst = "float4x4 View;";
        public const string WorldMatrixConst = "float4x4 World;";        

        public const string Position0Element = "float4 Position : POSITION0;";
        public const string NormalElement = "float3 normal : NORMAL;";
        public const string TexCoord0Element = "float2 uv : TEXCOORD0;";
        public const string TangentElement = "float3 tangent  : TANGENT;";
        public const string BinormalElement = "float3 binormal : BINORMAL;";
        public const string IndicesElement = "half4 indices : BLENDINDICES0;";
        public const string WeightElement = "float4 weights	: BLENDWEIGHT0;";

    }
}

﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using ShevaEngine.Entities;
using ShevaEngine.Entities.Components;
using ShevaEngine.Scenes;
using System.IO;

namespace TestSceneBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            Scene testScene = new Scene();

            Entity newEntity = new Entity();
            newEntity.AddComponent<TransformComponent>();
            testScene.AddEntityImmediate(newEntity);

            Entity cameraEntity = new Entity();
            cameraEntity.AddComponent<TransformComponent>();
            CameraComponent cameraComponent = cameraEntity.AddComponent<CameraComponent>();
            cameraComponent.RenderingPath = "Deferred";
            cameraComponent.NearPlane = 0.1f;
            cameraComponent.FarPlane = 1000;
            cameraComponent.Viewport = new Rectangle(0, 0, 1000, 1000);
            cameraComponent.IsActive = true;
            cameraComponent.ClearColor = Color.CornflowerBlue;
            testScene.AddEntityImmediate(cameraEntity);

            JsonSerializerSettings settings = new JsonSerializerSettings {TypeNameHandling = TypeNameHandling.Auto};
            using (TextWriter writer = new StreamWriter("TestScene.scene"))
            {
                writer.Write(JsonConvert.SerializeObject(testScene, settings));
            }
        }
    }
}

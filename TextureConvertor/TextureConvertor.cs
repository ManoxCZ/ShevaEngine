﻿using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using System.IO;

namespace TextureConvertor
{
    public class TextureConvertor
    {
        /// <summary>
        /// Convert model.
        /// </summary>
        /// <param name="inputFilename"></param>
        public static void Convert(string inputFilename)
        {
            GraphicsDevice device = new GraphicsDevice(GraphicsAdapter.DefaultAdapter, GraphicsProfile.HiDef, new PresentationParameters());

            using (StreamReader reader = new StreamReader(inputFilename))
            {
                Texture2D texture = Texture2D.FromStream(device, reader.BaseStream);

                ShevaEngine.Graphics.Textures.Texture2D sTexture = new ShevaEngine.Graphics.Textures.Texture2D
                {
                    Width = texture.Width,
                    Height = texture.Height,
                    LevelCount = texture.LevelCount,
                    Format = texture.Format
                };
                sTexture.Data = new byte[sTexture.LevelCount][];

                for (int i = 0; i < sTexture.LevelCount; i++)
                {
                    sTexture.Data[i] = new byte[sTexture.Width * sTexture.Height * 4];
                    texture.GetData(i, null, sTexture.Data[i], 0, sTexture.Data[i].Length);
                }

                using (JsonWriter writer = new JsonTextWriter(new StreamWriter(Path.ChangeExtension(inputFilename, "tex2D"))))
                {
                    JsonSerializer serializer = new JsonSerializer();

                    serializer.Serialize(writer, sTexture);
                }
            }
        }
    }
}
